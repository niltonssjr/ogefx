<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return redirect()->route('analysis.view');
});

Auth::routes();

Route::get('/home', function () {
	return redirect()->route('analysis.view');
});

Route::get('/expirado', function () {
	return view ('errors.429');
});

Route::group(['middleware'=>'auth','namespace'=>'Admin','prefix'=>'admin'],function(){
	Route::get('/dashboard',"QuotesController@analysis")->name('dashboard');
	// Route::get('/dashboard','GeneralController@index')->name('dashboard');
	Route::get('/usuarios/listar','UsersController@index')->name('users.list');
	Route::get('/usuarios/incluir','UsersController@insert_show')->name('users.insert.show');
	Route::post('/usuarios/incluir','UsersController@insert_process')->name('users.insert.process');
	Route::get('/usuarios/atualizar/{id}','UsersController@update_show')->name('users.update.show');
	Route::post('/usuarios/atualizar','UsersController@update_process')->name('users.update.process');
	Route::get("/usuarios/remover/{id}","UsersController@delete_process")->name('users.delete.process');
	Route::get("/usuarios/desativar/{id}","UsersController@inactivate_process")->name('users.inactivate.process');
	Route::get("/usuarios/reativar/{id}","UsersController@reactivate_process")->name('users.reactivate.process');
	Route::get("/meuperfil","UsersController@profile_show")->name('myprofile.show');
	Route::post("/meuperfil","UsersController@profile_update")->name('myprofile.update');
	Route::get("/meuacesso","UsersController@password_show")->name('myaccess.show');
	Route::post("/meuacesso","UsersController@password_update")->name('myaccess.update');
	Route::get('/moedas',"CurrenciesController@index")->name('currencies');		
	Route::get('/moedas/remover/{id}',"CurrenciesController@destroy")->name('currencies.delete');
	Route::post('/moedas/adicionar',"CurrenciesController@store")->name('currencies.post');
	Route::get('/cotacoes',"QuotesController@index")->name('quotes');
	Route::get('/cotacoes/diario',"QuotesController@quotesDaily")->name('quotes.daily');
	Route::get('/cotacoes/semanal',"QuotesController@quotesWeekly")->name('quotes.weekly');
	Route::get('/cotacoes/mensal',"QuotesController@quotesMonthly")->name('quotes.monthly');
	Route::get('/cotacoes/carregar',"QuotesController@showLoad")->name('quotes.showload');
	Route::post('/cotacoes/carregar',"QuotesController@loadSingleFile")->name('quotes.loadsinglefile');
	Route::get('/cotacoes/reiniciar',"QuotesController@cleanQuotesDatabase")->name('quotes.clean');
	Route::get('/cotacoes/unzip',"QuotesController@unzip")->name('quotes.unzip');
	Route::get('/cotacoes/historico',"QuotesController@history")->name('quotes.history');
	Route::post('/cotacoes/historico',"QuotesController@history")->name('quotes.history');
	Route::get('/indicadores','QuotesController@indicators')->name('indicators.view');
	Route::post('/indicadores','QuotesController@indicators')->name('indicators.view');
	Route::get('/analise-tecnica','QuotesController@analysis')->name('analysis.view');
	Route::post('/analise-tecnica','QuotesController@analysis')->name('analysis.view');	
	Route::any('/analise-tecnica/simulador','QuotesController@simulator')->name('analysis.simulator');	
	Route::get('/portfolio','PortfolioController@index')->name('portfolio');	
	Route::post('/portfolio/create','PortfolioController@create')->name('portfolio.create');	
	Route::get('/portfolio/delete/{id}','PortfolioController@delete')->name('portfolio.delete');	
	Route::get('/portfolio/delete_all','PortfolioController@delete_all')->name('portfolio.deleteall');	
	Route::get('/configuracoes',"SystemSettingsController@index")->name('systemsettings');
	Route::post('/configuracoes',"SystemSettingsController@store")->name('systemsettings.update');
	Route::get("/testemail","GeneralController@test_email");
	Route::get('/cotacoes/forca',"QuotesController@forca_das_moedas")->name('quotes.power');
	Route::get("/cotacoes/semaforo_todas_as_moedas","QuotesController@semaforos_todas_as_cotacoes")->name("quotes.semaforos");
	Route::get("/cotacoes/semaforo_cruzamento_media/{pair}","QuotesController@semaforo_cruzamento_media");
	Route::get("/cotacoes/flow-money-csv/{moeda}/{periodo}/{qtd_periodos}/{start_date}/{end_date}","QuotesController@flow_money_csv");
});
