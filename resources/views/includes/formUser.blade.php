				{!! csrf_field(); !!}
				<input type="hidden" name="user_id" value="{{ old('user_id',$user_form->id)}}">
				<div class=''>
					<div class="form-group">
						<label for="user_name">Nome:</label>
						<input type="text" class='form-control' name="user_name" placeholder="Nome do usuário" value="{{ old('user_name',$user_form->name) }}">	
					</div>							
				</div>
				<div class=''>
					<div class="form-group">
						<label for="user_email">E-mail:</label>
						<input type="email" class='form-control' name="user_email" placeholder="Email do usuário" value="{{ old('user_email',$user_form->email) }}">	
					</div>					
				</div>
				<div class="row">
					<div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
						<div class="form-group">
							<label for="user_nivel">Nível:</label>
							<select class="as_select2 form-control" name="user_nivel">
							  <option value="admin">Administrador</option>							    
							  <option value="user" @if (old('user_nivel',$user_form->nivel) == 'user') SELECTED @endif>Usuário</option>
							</select>
						</div>					
					</div>					
					<div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
						<div class="form-group">
							<label for="user_status">Status do usuário:</label>
							<select class="as_select2 form-control" name="user_status">
							  <option value="ativo">Ativo</option>							    
							  <option value="inativo"  @if (old('user_status',$user_form->status) == 'inativo') SELECTED @endif>Inativo</option>
							</select>
						</div>					
					</div>										
				</div>				
				<div class="row">
					<div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
						<div class="form-group">
							<label for="user_plano_nome">Plano:</label>
							<select name="user_plano_nome" id='user_plano_nome' class="form-control">
				             	<option value="sel">Selecione</option>
				             	<option value="1" @if (old('user_plano_nome',$user_form->plano_nome)=='Trial') selected @endif>Trial</option>
				             	<option value="1" @if (old('user_plano_nome',$user_form->plano_nome)=='1') selected @endif>1 mês</option>
				             	<option value="3" @if (old('user_plano_nome',$user_form->plano_nome)=='3') selected @endif>3 meses</option>
				             	<option value="6" @if (old('user_plano_nome',$user_form->plano_nome)=='6') selected @endif>6 meses</option>
				             	<option value="9" @if (old('user_plano_nome',$user_form->plano_nome)=='9') selected @endif>9 meses</option>
				             	<option value="12" @if (old('user_plano_nome',$user_form->plano_nome)=='12') selected @endif>12 meses</option>

				         	</select>
						</div>					
					</div>					
					<div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
						<div class="form-group">
							<label for="user_plano_validade">Validade do plano:</label>
							<input type="text" class='form-control  as_mask_date as_datepicker' name="user_plano_validade" id="user_plano_validade" value="{{ old('user_plano_validade',$user_form->plano_validade) }}">	
						</div>					
					</div>										
				</div>