<div>
	<table class='table table-striped table-bordered' id="standard_table">
		<thead>
			<tr>
			@foreach($titles as $title)		
				<th>{{$title}}</th>
			@endforeach
			</tr>
		</thead>	
		<tbody>
			@foreach($lines as $line)
			<tr>
				@foreach($line as $column)
					<td>
						{!! $column !!}
					</td>
				@endforeach
			</tr>
			@endforeach
		</tbody>
	</table>
</div>