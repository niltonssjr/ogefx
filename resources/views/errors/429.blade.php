@extends('adminlte::page')

@section('title', 'OGEFX - Painel principal')

@section('content')
    <div class="dashboard_logo">
    	<img src="{{ url('/img/logo_colorido_500.png')}}">
    	<h3>Seu plano de assinatura expirou! Entre em contato conosco.</h3>
    </div>
@stop

@section('css')
<link rel="stylesheet" href="{{ url('/css/ogefx.css')}}">
@stop