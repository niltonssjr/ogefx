<!DOCTYPE html>
<html>
<head>
	<title>OGEFX - Página não encontrada</title>
</head>
<style type="text/css">
	.center{text-align:center;margin-top:250px;}
</style>
<body>
<div class="center">
	<img src="{{ url('/img/logo_colorido_500.png')}}">	
	<h1>Desculpe, a página que você está buscando não existe,<br>ou não está disponível no momento.</h1>
</div>

</body>
</html>