@extends('adminlte::page')

@section('title', 'Usuários')

@php
$lines = [];
$current_user = auth()->user()->id;

foreach($users as $user){
	
	$line = $user;
	
	$url_update = route('users.update.show',['id'=>$user['id']]);	
	$url_delete = "javascript: removal_confirmation(" . $user['id'] . ")";

	$url_inactivate = route('users.inactivate.process',['id'=>$user['id']]);
	$url_reactivate = route('users.reactivate.process',['id'=>$user['id']]);

	$action_update = "<a href='{$url_update}'><span data-toggle='tooltip' data-placement='bottom' title='Atualizar dados do usuário' class='glyphicon glyphicon-pencil button_tag orange_tag'></span></a>";

	$action_delete = "<a href='{$url_delete}'><span data-toggle='tooltip' data-placement='bottom' title='Remover usuário' class='glyphicon glyphicon-remove button_tag red_tag'></span></a>";

	$action_inactivate = "<a href='{$url_inactivate}'><span data-toggle='tooltip' data-placement='bottom' title='Desativar usuário' class='glyphicon glyphicon-ban-circle button_tag gray_tag'></span></a>";

	$action_reactivate = "<a href='{$url_reactivate}'><span data-toggle='tooltip' data-placement='bottom' title='Reativar usuário' class='glyphicon glyphicon-ok button_tag green_tag'></span></a>";
	
	if ($user['status']=='Ativo'){
		$action_inactivate_reactivate = $action_inactivate;
	} else {
		$action_inactivate_reactivate = $action_reactivate;
	}

	$actions = "";
	
	if ($user['id']!=$current_user)
		$actions = $action_update.$action_delete.$action_inactivate_reactivate;
	
	$line[] = $actions;
	

	$lines[] = $line;

}

@endphp

@section('content')    
<span class='content-box-title'>Usuários - {{ env('APP_NAME') }}</span>
<div class='content-box'>	
	@include('includes.alerts')
	@include('includes.tabela')
</div>
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.css"/>
<link rel="stylesheet" href='{{ url("/") }}/css/ogefx.css'>
@stop

@section('js')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
<script type="text/javascript" src="{{ url('/')}}/js/sweetalert2.all.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
		$('#standard_table').DataTable({
			"responsive" :true,
			"language": {
				"lengthMenu": "Mostrando  _MENU_  sub-contas por página",
				"emptyTable": "Nenhuma sub-conta encontrada",
				"search":         "Pesquisar: ",
				"zeroRecords": "Nenhuma sub-conta encontrada",
				"info": "Mostrando página  _PAGE_  de  _PAGES_",
				"infoEmpty": "Nenhuma sub-conta encontrada",
				"infoFiltered": "(filtrado de _MAX_ sub-contas)",
				"paginate": {
					"first":      "Primeira",
					"last":       "Última",
					"next":       "Próxima",
					"previous":   "Anterior"
				},
				"aria": {
					"sortAscending":  ": ative para a ordenação crescente",
					"sortDescending": ": ative para a ordenação decrescente"
				}					    
			}    			
		});
	} );

	function removal_confirmation(id){
		swal({
			title: 'Confirma a exclusão do cliente?',
			text: "Esta ação não pode ser revertida!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim, remova o cliente!'
		}).then((result) => {
			if (result.value) {
				target_route = "{{ route('users.delete.process',['id'=>'##@@##'])}}";					
				target_url = target_route.replace('##@@##',id);				
				window.location.replace(target_url);				
			}
		})
	}

</script>
@stop
