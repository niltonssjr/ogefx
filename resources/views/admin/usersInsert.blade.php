@extends('adminlte::page')

@section('title', 'Cadastrar usuário')

@section('content')
<div class="row">
	<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
		<span class='content-box-title'>Novo usuário - {{ env('APP_NAME') }}</span>
		<div class='content-box'>	
		@include('includes.alerts')	
			<form action="{{ route('users.insert.process')}}" method='post'>
				@include('includes.formUser')	
				<div>
					<button class="btn btn-primary">Cadastrar</button>
				</div>			
			</form>
		</div>		
	</div>	
</div>

@stop

@section('css')
<link href="{{ url('/')}}/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link rel="stylesheet" href='{{ url("/") }}/css/ogefx.css'>
@stop

@section('js')
<script src='{{ url("/") }}/js/jquery.mask.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.pt-BR.min.js'></script>
<script src='{{ url("/") }}/js/auge_masks.js'></script>
<script src='{{ url("/") }}/js/sweetalert2.all.js'></script>
<script src='{{ url("/") }}/js/add_date.js'></script>
<script>
	$(document).ready(function(){
		//datepicker for due_date
		$('.as_select2').select2();

		$(".as_datepicker").datepicker({
			format: "dd/mm/yyyy",
			todayBtn: true,
			autoclose: true,
			language: "pt-BR",
			startDate: "1d"
		});

		$("#user_plano_nome").change(function(){		
			var meses = $("#user_plano_nome option:selected").val();
			if (meses=='sel' || meses=="Trial") return;
			var hoje = new Date();
			hoje = hoje.addMonths(Number(meses));
			$(".as_datepicker").datepicker('update',hoje);
		});

	});
</script>
@stop



