@extends('adminlte::page')

@section('title', 'Portfolio')

@section('content')    
<span class='content-box-title'>Últimas 20 análises salvas no portfolio</span>

<div class='content-box'>	
	<div class='row'>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 button-box">
			<button class='btn btn-danger' onclick='limpar_portfolio()'><i class='fa fa-trash'></i> Excluir tudo</button>
		</div>
	</div>	
	@include('includes.tabela')
</div>
@stop

@section('css')
<link href="{{ url('/')}}/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.css"/>
<link rel="stylesheet" href='{{ url("/") }}/css/ogefx.css?v=20190619a'>
@stop

@section('js')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/sorting/datetime-moment.js"></script>	
<script src='{{ url("/") }}/js/jquery.mask.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.pt-BR.min.js'></script>
<script src='{{ url("/") }}/js/auge_masks.js'></script>
<script type="text/javascript" src="{{ url('/')}}/js/sweetalert2.all.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$.fn.dataTable.moment( "DD/MM/YYYY");
		jQuery.extend( jQuery.fn.dataTableExt.oSort, {
			"currency-pre": function ( a ) {
		        // a = (a==="-") ? 0 : a.replace( /[^\d\-\.]/g, "" );
		        a = a.replace("R$","").replace(/\./g,"").replace(",",".");
		        return parseFloat( a );
		    },

		    "currency-asc": function ( a, b ) {
		    	return a - b;
		    },

		    "currency-desc": function ( a, b ) {
		    	return b - a;
		    }
		} );		
		$('[data-toggle="tooltip"]').tooltip();
		$('#standard_table').DataTable({
			"order" : [0, 'desc'],
			"pageLength": 25,
			"responsive" :false,
			"scrollX": true,
			// scrollCollapse: true,
	  //       fixedColumns:   {
	  //           leftColumns: 1	            
	  //       },
	  "language": {
	  	"lengthMenu": "Mostrando  _MENU_  cotações por página",
	  	"emptyTable": "Nenhuma cotação encontrada",
	  	"search":         "Pesquisar: ",
	  	"zeroRecords": "Nenhuma cotação encontrada",
	  	"info": "Mostrando página  _PAGE_  de  _PAGES_",
	  	"infoEmpty": "Nenhuma cotação encontrada",
	  	"infoFiltered": "(filtrado de _MAX_ cotações)",				
	  	"paginate": {
	  		"first":      "Primeira",
	  		"last":       "Última",
	  		"next":       "Próxima",
	  		"previous":   "Anterior"
	  	},
	  	"aria": {
	  		"sortAscending":  ": ative para a ordenação crescente",
	  		"sortDescending": ": ative para a ordenação decrescente"
	  	}					    
	  }    			
	});		
		$("#start_date").datepicker({
			format: "dd/mm/yyyy",
			todayBtn: true,
			autoclose: true,
			language: "pt-BR"			
		});		
		$("#end_date").datepicker({
			format: "dd/mm/yyyy",
			todayBtn: true,
			autoclose: true,
			language: "pt-BR"			
		});		

		// $(".remove_portfolio_button").click(function(){
			
		// });
		// $(".remove_portfolio_button").on("click",function(){
		// 	remove_portfolio(this);
		// });

	});	

	function remove_portfolio(obj){



		id = $(obj).attr('data-id');

		url = "{{ url('/admin/portfolio/delete') }}" + "/" + id;

		$.get(url,function(data){
			sim = JSON.parse(data);
			console.log(sim);
			$('#standard_table').DataTable().row($(obj).closest('tr')).remove().draw();
			return false;
		}).fail(function(error){
			console.log(error);
		});           

	}

	function limpar_portfolio(obj){
		
		swal({
			title: 'Confirma a limpeza do portfólio?',
			text: "Esta operação não pode ser revertida!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim, limpe o portfólio'
		}).then((result) => {
			url = "{{ url('/admin/portfolio/delete_all') }}";
			window.location.href = url;
		})      

	}
</script>
@stop
