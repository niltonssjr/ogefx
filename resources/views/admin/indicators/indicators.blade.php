@extends('adminlte::page')

@section('title', 'Indicadores')

@section('content')    
<span class='content-box-title'>Indicadores @if ($selected_period) - {{ $selected_period }} @endif @if ($selected_pair) - {{ $selected_pair }} @endif - {{ env('APP_NAME') }}</span>
<div class='content-box'>	
	@include('includes.alerts')	
	<form id="form_quotes_history" class="form-inline" action="{{ route('indicators.view')}}" method="post">
		{!! csrf_field() !!}
		<div class="form-group">
			<label for="pair">Moedas:</label>
			<select name="pair" class="form-control">
					<option value="sel">Selecione</option>
				@foreach($pairs as $pair)
					<option value="{{$pair->pair}}" @if(old('pair',$default_values->pair)==$pair->pair) selected @endif>{{$pair->pair}} </option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="period">Período:</label>
			<select name="period" class="form-control">
					<option value="sel">Selecione</option>
				@foreach($periods as $key=>$period)
					<option value="{{$key}}" @if(old('period',$default_values->period)==$period) selected @endif>{{$period}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for='start_date'>De:</label>
			<input type="text" name="start_date" id="start_date" class='form-control as_mask_date' value="{{ old('start_date',$default_values->start_date) }}">
		</div>		
		<div class="form-group">
			<label for='end_date'>Até:</label>
			<input type="text" name="end_date" id="end_date" class='form-control as_mask_date' value="{{ old('end_date',$default_values->end_date) }}">
		</div>				
		<button type="submit" class="btn btn-primary">Pesquisar</button>
	</form>
	<br/>
	@include('includes.tabela')
</div>
@stop

@section('css')
<link href="{{ url('/')}}/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.css"/>
<link rel="stylesheet" href='{{ url("/") }}/css/ogefx.css'>
@stop

@section('js')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/sorting/datetime-moment.js"></script>	
<script src='{{ url("/") }}/js/jquery.mask.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.pt-BR.min.js'></script>
<script src='{{ url("/") }}/js/auge_masks.js'></script>
<script type="text/javascript" src="{{ url('/')}}/js/sweetalert2.all.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$.fn.dataTable.moment( "DD/MM/YYYY");
		jQuery.extend( jQuery.fn.dataTableExt.oSort, {
		    "currency-pre": function ( a ) {
		        // a = (a==="-") ? 0 : a.replace( /[^\d\-\.]/g, "" );
		        a = a.replace("R$","").replace(/\./g,"").replace(",",".");
		        return parseFloat( a );
		    },
		 
		    "currency-asc": function ( a, b ) {
		        return a - b;
		    },
		 
		    "currency-desc": function ( a, b ) {
		        return b - a;
		    }
		} );		
		$('[data-toggle="tooltip"]').tooltip();
		$('#standard_table').DataTable({
			"order" : [0, 'desc'],
			"pageLength": 10,
			"responsive" :false,
			"scrollX": true,
			// scrollCollapse: true,
	  //       fixedColumns:   {
	  //           leftColumns: 1	            
	  //       },
			"language": {
				"lengthMenu": "Mostrando  _MENU_  cotações por página",
				"emptyTable": "Nenhuma cotação encontrada",
				"search":         "Pesquisar: ",
				"zeroRecords": "Nenhuma cotação encontrada",
				"info": "Mostrando página  _PAGE_  de  _PAGES_",
				"infoEmpty": "Nenhuma cotação encontrada",
				"infoFiltered": "(filtrado de _MAX_ cotações)",				
				"paginate": {
					"first":      "Primeira",
					"last":       "Última",
					"next":       "Próxima",
					"previous":   "Anterior"
				},
				"aria": {
					"sortAscending":  ": ative para a ordenação crescente",
					"sortDescending": ": ative para a ordenação decrescente"
				}					    
			}    			
		});		
		$("#start_date").datepicker({
			format: "dd/mm/yyyy",
			todayBtn: true,
			autoclose: true,
			language: "pt-BR"			
		});		
		$("#end_date").datepicker({
			format: "dd/mm/yyyy",
			todayBtn: true,
			autoclose: true,
			language: "pt-BR"			
		});		
	} );	

</script>
@stop
