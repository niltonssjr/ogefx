@extends('adminlte::page')

@section('title', 'Análise Técnica')

@section('content')    
<span class='content-box-title'>Análise Técnica @if ($selected_period) - {{ $selected_period }} @endif @if ($selected_pair) - {{ $selected_pair }} @endif - {{ env('APP_NAME') }}</span>
<div class='content-box'>	
	@include('includes.alerts')	
	<form id="form_quotes_history" class="form-inline" action="{{ route('analysis.view')}}" method="post">
		{!! csrf_field() !!}
		<div class="form-group">
			<label for="pair">Moedas:</label>
			<select name="pair" id='pair' class="form-control">
				<option value="sel">Selecione</option>
				@foreach($pairs as $pair)
				<option value="{{$pair->pair}}" @if(old('pair',$default_values->pair)==$pair->pair) selected @endif>{{$pair->pair}} </option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="period">Período:</label>
			<select name="period" class="form-control">
				<option value="sel">Selecione</option>
				@foreach($periods as $key=>$period)                    
				<option value="{{$key}}" @if(old('period',$default_values->period)==$period) selected @endif>{{$period}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-2">
			<label for='start_date'>De:</label>
			<input type="text" name="start_date" id="start_date" class='form-control as_mask_date' value="{{ old('start_date',$default_values->start_date) }}">
		</div>		
		<div class="form-group">
			<label for='end_date'>Até:</label>
			<input type="text" name="end_date" id="end_date" class='form-control as_mask_date' value="{{ old('end_date',$default_values->end_date) }}">
		</div>				
		<button type="submit" class="btn btn-primary">Pesquisar</button>
		<button type="button" class="btn btn-primary" onClick="$('#div_simulador').slideToggle()">Simulador</button> 
		<button type="button" class="btn btn-primary" onClick="$('#div_retornos').slideToggle()">Retornos</button> 
	</form>	
</div>


<div id="div_simulador">
	<br/>
	<span class="content-box-title">SIMULADOR DE PREVISÃO DE RETORNO</span>
	<div class="content-box">
		<form id="form_quotes_simulator" class="form-inline" action="" method="post">
			{!! csrf_field() !!}
			<div class="form-group">
				<label for='sim_last_quote'>Última cotação:</label>
				<input type="text" name="sim_last_quote" id="sim_last_quote" class='form-control as_mask_small_number' value="{{ $volat_data->last_d_quote }}">
			</div>      
			<div class="form-group">
				<label for="sim_period">Período:</label>
				<select name="sim_period" id="sim_period" class="form-control">
					<option value="sel" selected>Selecione</option>
					@foreach($periods as $key=>$period)                    
					<option value="{{$key}}">{{$period}}</option>
					@endforeach
				</select>
			</div>    
			<div class="form-group">
				<label for='sim_start_date'>De:</label>
				<input type="text" name="sim_start_date" id="sim_start_date" class='form-control as_mask_date' value="{{ old('sim_start_date',$default_values->start_date) }}">
			</div>      
			<div class="form-group">
				<label for='sim_end_date'>Até:</label>
				<input type="text" name="sim_end_date" id="sim_end_date" class='form-control as_mask_date' value="{{ old('sim_end_date',$default_values->end_date) }}">
			</div>                     
			<button type="button" id="sim_check_opportunity" class="btn btn-primary">Buscar oportunidade</button>
			<!-- <button type="button" id="sim_clean_opportunity" class="btn btn-default">Reiniciar</button> -->
		</form>
		<br/>
		<table class='table table-striped table-bordered'>
			<thead>
				<tr>
					<th>Data</th>
					<th>Tendência</th>
					<th>Cotação alvo</th>
					<th>Pontos</th>
					<th>F. Tendência</th>                                
					<th>Dir Volatilidade</th>                                
				</tr>
			</thead>
			<tbody>
				<tr>
					<td id="sim_res_ref_date" class="sim_result"></td>
					<td id="sim_res_tend" class="sim_result"></td>
					<td id="sim_res_target" class="sim_result"></td>
					<td id="sim_res_points" class="sim_result"></td>                
					<td id="sim_res_rsi" class="sim_result"></td>                
					<td id="sim_res_rvi" class="sim_result"></td>                
				</tr>
			</tbody>        
		</table>    
	</div>
</div>

<div id="div_retornos">
	<br/>
	<span class="content-box-title">ANÁLISE DOS ÚLTIMOS RETORNOS</span>        
	<div class="content-box" id="totais_retornos">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 card">
				<div>
					<div class="content-box-title">ùltima data</div>
					<div class="content-box">
						<p>{{$return_analysis->first_ref_date}}</p>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 card">
				<div>
					<div class="content-box-title">Abertura</div>
					<div class="content-box">
						<p>{{$return_analysis->first_quote}}</p>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 card">
				<div>
					<div class="content-box-title">Fechamento</div>
					<div class="content-box">
						<p>{{$return_analysis->last_quote}}</p>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 card">
				<div>
					<div class="content-box-title">Ganho</div>
					<div class="content-box">
						<p>{!!$return_analysis->gain!!}</p>
					</div>
				</div>
			</div>                        
		</div>        
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 card">
				<div>
					<div class="content-box-title">Média de retornos - Atual : {!!$ret_rv->last_retorno!!}</div>
					<div class="content-box">
						<table class='table table-striped table-bordered'>
							<thead>
								<tr>
									<th>DP-3</th>
									<th>DP-2</th>
									<th>DP-1</th>
									<th>Média</th>
									<th>DP+1</th>
									<th>DP+2</th>
									<th>DP+3</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>{!! $ret_rv->retorno->d_3 !!}</td>
									<td>{!! $ret_rv->retorno->d_2 !!}</td>
									<td>{!! $ret_rv->retorno->d_1 !!}</td>
									<td>{!! $ret_rv->retorno->d0 !!}</td>
									<td>{!! $ret_rv->retorno->d1 !!}</td>
									<td>{!! $ret_rv->retorno->d2 !!}</td>
									<td>{!! $ret_rv->retorno->d3 !!}</td>
								</tr>
<!--                                 <tr>
                                    <td>{!! $ret_rv->retorno_quotes->d_3 !!}</td>
                                    <td>{!! $ret_rv->retorno_quotes->d_2 !!}</td>
                                    <td>{!! $ret_rv->retorno_quotes->d_1 !!}</td>
                                    <td>{!! $ret_rv->retorno_quotes->d0 !!}</td>
                                    <td>{!! $ret_rv->retorno_quotes->d1 !!}</td>
                                    <td>{!! $ret_rv->retorno_quotes->d2 !!}</td>
                                    <td>{!! $ret_rv->retorno_quotes->d3 !!}</td>
                                </tr>  -->                               
                            </tbody>                            
                        </table>
                    </div>
                </div>
            </div>            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 card">
            	<div>
            		<div class="content-box-title">Índice RV - Atual : {!!$ret_rv->last_relacao_volume!!} - fator: {{$ret_rv->last_distancia_media}} - Índice: {{$ret_rv->last_diferenca_media}}</div>
            		<div class="content-box">
            			<table class='table table-striped table-bordered'>
            				<thead>
            					<tr>
            						<th>DP-3</th>
            						<th>DP-2</th>
            						<th>DP-1</th>
            						<th>Média</th>
            						<th>DP+1</th>
            						<th>DP+2</th>
            						<th>DP+3</th>
            					</tr>
            				</thead>
            				<tbody>
            					<tr>
            						<td>{!! $ret_rv->relacao_volume->d_3 !!}</td>
            						<td>{!! $ret_rv->relacao_volume->d_2 !!}</td>
            						<td>{!! $ret_rv->relacao_volume->d_1 !!}</td>
            						<td>{!! $ret_rv->relacao_volume->d0 !!}</td>
            						<td>{!! $ret_rv->relacao_volume->d1 !!}</td>
            						<td>{!! $ret_rv->relacao_volume->d2 !!}</td>
            						<td>{!! $ret_rv->relacao_volume->d3 !!}</td>
            					</tr>
<!--                                 <tr>
                                    <td>{!! $ret_rv->relacao_volume_quotes->d_3 !!}</td>
                                    <td>{!! $ret_rv->relacao_volume_quotes->d_2 !!}</td>
                                    <td>{!! $ret_rv->relacao_volume_quotes->d_1 !!}</td>
                                    <td>{!! $ret_rv->relacao_volume_quotes->d0 !!}</td>
                                    <td>{!! $ret_rv->relacao_volume_quotes->d1 !!}</td>
                                    <td>{!! $ret_rv->relacao_volume_quotes->d2 !!}</td>
                                    <td>{!! $ret_rv->relacao_volume_quotes->d3 !!}</td>
                                </tr>   -->                              
                            </tbody>                            
                        </table>
                    </div>
                </div>
            </div>               
        </div>
        <div class="row">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 card">
        		<div class="div_table_retornos_container">
        			<div id="div_table_retornos">
        				<table class='table table-striped table-bordered' id='table_analise_retornos'>
        					<thead>
        						<tr>
        							<th></th>
        							@foreach($return_analysis->ref_date as $an)     
        							<td>{{ $an }}</td>                        
        							@endforeach                                
        						</tr>
        					</thead>
        					<tbody>
        						<tr>
        							<td>Ganho</td>
        							@foreach($return_analysis->periods as $an)     
        							<td>{!! $an !!}</td>                        
        							@endforeach                
        						</tr>
        						<tr>
        							<td>Acumulado</td>
        							@foreach($return_analysis->summed as $an)     
        							<td>{!! $an !!}</td>                        
        							@endforeach                                
        						</tr>            
        						<tr>
        							<td>Cotação</td>
        							@foreach($return_analysis->quotes as $an)     
        							<td>{{ $an }}</td>                        
        							@endforeach                                
        						</tr>                         
        					</tbody>
        				</table>
        			</div>
        		</div>
        		<div class="portfolio_button">
        			<button class='btn btn-primary' onClick='add_to_portfolio()'>Adicionar ao portfolio</button>
        		</div>
        	</div>
        </div>
    </div>
</div>


<br/>
<div class="row">
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" id="div_forca_tendencia">        
		<span class="content-box-title">FORÇA TENDÊNCIA <span id="ft_semaforo">{!! $ft_semaforo !!}</span></span>        
		<div class="content-box">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Período</th>
						<th>Data</th>
						<th>Cotação</th>
						<th>T</th>                                                
						<th>I</th>                                                
						<th>A</th>                                                
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Diário</td>
						<td>{{$volat_data->rsi_d_ref_date}}</td>
						<td>{{$volat_data->rsi_d_quote}}</td>
						<td>{!!$volat_data->rsi_d_status!!}</td>                        
						<td>{!!$volat_data->rsi_d_ind!!}</td>                        
						<td>{!!$volat_data->rsi_d_last_ind!!}</td>                        
					</tr>
					<tr>
						<td>Semanal</td>
						<td>{{$volat_data->rsi_w_ref_date}}</td>
						<td>{{$volat_data->rsi_w_quote}}</td>
						<td>{!!$volat_data->rsi_w_status!!}</td> 
						<td>{!!$volat_data->rsi_w_ind!!}</td>                        
						<td>{!!$volat_data->rsi_w_last_ind!!}</td>                        
					</tr>
					<tr>
						<td>Mensal</td>
						<td>{{$volat_data->rsi_m_ref_date}}</td>
						<td>{{$volat_data->rsi_m_quote}}</td>
						<td>{!!$volat_data->rsi_m_status!!}</td>  
						<td>{!!$volat_data->rsi_m_ind!!}</td>                        
						<td>{!!$volat_data->rsi_m_last_ind!!}</td>                        
					</tr>                                      
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		<span class="content-box-title">
			DIREÇÃO TENDÊNCIA
			<span class="dt_semaforo_cruzamento_media">
				<i id="dir_cruzmedia_15" class="{{$indicadores_cruzamento_media->cruzm15}}"></i>
				<i id="dir_cruzmedia_30" class="{{$indicadores_cruzamento_media->cruzm30}}"></i>
				<i id="dir_cruzmedia_60" class="{{$indicadores_cruzamento_media->cruzm60}}"></i>
				<i id="dir_cruzmedia_240" class="{{$indicadores_cruzamento_media->cruzm240}}"></i>
				<i id="dir_cruzmedia_1440" class="{{$indicadores_cruzamento_media->cruzm1440}}"></i>

			</span>
		</span>        
		<div class="content-box">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Período</th>
						<th>Data</th>
						<th>Cotação</th>
						<th>T</th>                        
						<th>I</th>                        
						<th>A</th>                        
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Diário</td>
						<td>{{$volat_data->rvi_d_quote_date}}</td>
						<td>{{$volat_data->rvi_d_quote}}</td>
						<td>{!!$volat_data->rvi_d_status!!}</td>                        
						<td>{!!$volat_data->rvi_d_ind!!}</td>                        
						<td>{!!$volat_data->rvi_d_last_ind!!}</td>                        
					</tr>
					<tr>
						<td>Semanal</td>
						<td>{{$volat_data->rvi_w_quote_date}}</td>
						<td>{{$volat_data->rvi_w_quote}}</td>
						<td>{!!$volat_data->rvi_w_status!!}</td>   
						<td>{!!$volat_data->rvi_w_ind!!}</td>                                             
						<td>{!!$volat_data->rvi_w_last_ind!!}</td>                                             
					</tr>
					<tr>
						<td>Mensal</td>
						<td>{{$volat_data->rvi_m_quote_date}}</td>
						<td>{{$volat_data->rvi_m_quote}}</td>
						<td>{!!$volat_data->rvi_m_status!!}</td> 
						<td>{!!$volat_data->rvi_m_ind!!}</td>                                               
						<td>{!!$volat_data->rvi_m_last_ind!!}</td>                                               
					</tr>                       
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		<span class="content-box-title">STOP LOSS/TAKE PROFIT</span>        
		<div class="content-box">
			<table class="table table-striped">
				<thead>
					<tr style="text-align:center">
						<th>Período</th>
						<th>%</th>
						<th><i class="glyphicon glyphicon-triangle-top"></i></th>
						<th><i class="glyphicon glyphicon-triangle-bottom"></i></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Diário</td>
						<td>{{$volat_data->vol_d_p}}</td>
						<td>{{$volat_data->up_d}}</td>
						<td>{{$volat_data->down_d}}</td>
					</tr>
					<tr>
						<td>Semanal</td>
						<td>{{$volat_data->vol_w_p}}</td>
						<td>{{$volat_data->up_w}}</td>
						<td>{{$volat_data->down_w}}</td>
					</tr>
					<tr>
						<td>Mensal</td>
						<td>{{$volat_data->vol_m_p}}</td>
						<td>{{$volat_data->up_m}}</td>
						<td>{{$volat_data->down_m}}</td>
					</tr>                                        
				</tbody>
			</table>
		</div>
	</div>
</div>

<br/>
<div class="row">

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div>
			<div class="content-box-title">Média de retornos - Atual : {!!$ret_rv->last_retorno!!}</div>
			<div class="content-box">
				<table class='table table-striped table-bordered'>
					<thead>
						<tr>
							<th>DP-3</th>
							<th>DP-2</th>
							<th>DP-1</th>
							<th>Média</th>
							<th>DP+1</th>
							<th>DP+2</th>
							<th>DP+3</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>{!! $ret_rv->retorno->d_3 !!}</td>
							<td>{!! $ret_rv->retorno->d_2 !!}</td>
							<td>{!! $ret_rv->retorno->d_1 !!}</td>
							<td>{!! $ret_rv->retorno->d0 !!}</td>
							<td>{!! $ret_rv->retorno->d1 !!}</td>
							<td>{!! $ret_rv->retorno->d2 !!}</td>
							<td>{!! $ret_rv->retorno->d3 !!}</td>
						</tr>
<!--                         <tr>
                            <td>{!! $ret_rv->retorno_quotes->d_3 !!}</td>
                            <td>{!! $ret_rv->retorno_quotes->d_2 !!}</td>
                            <td>{!! $ret_rv->retorno_quotes->d_1 !!}</td>
                            <td>{!! $ret_rv->retorno_quotes->d0 !!}</td>
                            <td>{!! $ret_rv->retorno_quotes->d1 !!}</td>
                            <td>{!! $ret_rv->retorno_quotes->d2 !!}</td>
                            <td>{!! $ret_rv->retorno_quotes->d3 !!}</td>
                        </tr>  -->                               
                    </tbody>                            
                </table>
            </div>
        </div>
    </div>            
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    	<div>
    		<div class="content-box-title">Índice RV - Atual : {!!$ret_rv->last_relacao_volume!!} - fator: {{$ret_rv->last_distancia_media}} - Índice: {{$ret_rv->last_diferenca_media}}</div>
    		<div class="content-box">
    			<table class='table table-striped table-bordered'>
    				<thead>
    					<tr>
    						<th>DP-3</th>
    						<th>DP-2</th>
    						<th>DP-1</th>
    						<th>Média</th>
    						<th>DP+1</th>
    						<th>DP+2</th>
    						<th>DP+3</th>
    					</tr>
    				</thead>
    				<tbody>
    					<tr>
    						<td>{!! $ret_rv->relacao_volume->d_3 !!}</td>
    						<td>{!! $ret_rv->relacao_volume->d_2 !!}</td>
    						<td>{!! $ret_rv->relacao_volume->d_1 !!}</td>
    						<td>{!! $ret_rv->relacao_volume->d0 !!}</td>
    						<td>{!! $ret_rv->relacao_volume->d1 !!}</td>
    						<td>{!! $ret_rv->relacao_volume->d2 !!}</td>
    						<td>{!! $ret_rv->relacao_volume->d3 !!}</td>
    					</tr>
<!--                                 <tr>
                                    <td>{!! $ret_rv->relacao_volume_quotes->d_3 !!}</td>
                                    <td>{!! $ret_rv->relacao_volume_quotes->d_2 !!}</td>
                                    <td>{!! $ret_rv->relacao_volume_quotes->d_1 !!}</td>
                                    <td>{!! $ret_rv->relacao_volume_quotes->d0 !!}</td>
                                    <td>{!! $ret_rv->relacao_volume_quotes->d1 !!}</td>
                                    <td>{!! $ret_rv->relacao_volume_quotes->d2 !!}</td>
                                    <td>{!! $ret_rv->relacao_volume_quotes->d3 !!}</td>
                                </tr>   -->                              
                            </tbody>                            
                        </table>
                    </div>
                </div>
            </div>               
        </div>
<!-- <br/>
<div class="row">
    <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-12">
    <span class="content-box-title">SINALIZAÇÃO</span>        
        <div class="content-box">
            <span class="sinalizacao"></span>
        </div>
    </div>        
</div>   -->        
<br/>
<span class="content-box-title black_background">Força das moedas - Atualizado em <span id='forca_ultima_atualizacao'></span> <a href="#" id="button_show_forca_moedas" class='toggle_button'><i class="glyphicon glyphicon-menu-down"></i></a></span>
<div class="content-box center currency_power" id="forca_moedas_board">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<table class='table table-striped'>
				<thead>
					<tr>
						@foreach($forca_das_moedas->moedas_tratadas as $moeda)
						<td>{{$moeda}}</td>
						@endforeach
						@foreach($forca_das_moedas->commodities->d as $comm=>$dados_com)
						<td>{{$comm}}</td>
						@endforeach
						<td>Período</td>
					</tr>
				</thead>
				<tbody>
					@foreach($forca_das_moedas->forca_geral as $periodo => $dados_periodo)
					<tr>
						@foreach($dados_periodo as $moeda=>$dados_moeda)
						<td id="forca_geral_moeda_{{$moeda}}_{{$periodo}}_percentual" class='currency_power_limit_{{$dados_moeda->percentual_indicator}}'>{{$dados_moeda->percentual}}</td>
						@endforeach
						@foreach($forca_das_moedas->commodities->{$periodo} as $comm=>$dados_comm)
						<td id="forca_geral_moeda_{{$comm}}_{{$periodo}}_percentual" class='currency_power_limit_{{$dados_comm->percentual_indicator}}'>{{$dados_comm->percentual}}</td>
						@endforeach						
						<td class='currency_power_periodo'>{{$forca_das_moedas->periodos_tratados->{$periodo} }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

	@foreach($forca_das_moedas->forca_por_par as $moeda=>$forca_da_moeda)
	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 table_forca_moedas">
			<span class="content-box-title">{{$moeda}} - Diário</span>
			<table class='table table-striped center'>
				<thead>
					<tr>
						<th>Par</th>
						<th>Ind</th>
						<th>Buy</th>
						<th>Sell</th>
						<th>Diff</th>
					</tr>
				</thead>
				<tbody>
					@foreach($forca_da_moeda->d as $par=>$forca_par)
					<tr>
						<td id="{{$moeda}}-d-{{$par}}-par">{{$par}}</td>
						<td id="{{$moeda}}-d-{{$par}}-percentual" class='currency_power_limit_{{$forca_par->percentual_indicator}}'>{{$forca_par->percentual}}</td>
						<td id="{{$moeda}}-d-{{$par}}-buy">{{$forca_par->buy}}</td>
						<td id="{{$moeda}}-d-{{$par}}-sell">{{$forca_par->sell}}</td>
						<td id="{{$moeda}}-d-{{$par}}-diff">{{$forca_par->diff}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>			
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 table_forca_moedas">
			<span class="content-box-title">{{$moeda}} - Semanal</span>
			<table class='table table-striped'>
				<thead>
					<tr>
						<th>Par</th>
						<th>Ind</th>
						<th>Buy</th>
						<th>Sell</th>
						<th>Diff</th>
					</tr>
				</thead>
				<tbody>
					@foreach($forca_da_moeda->w as $par=>$forca_par)
					<tr>
						<td id="{{$moeda}}-w-{{$par}}-par">{{$par}}</td>
						<td id="{{$moeda}}-w-{{$par}}-percentual" class='currency_power_limit_{{$forca_par->percentual_indicator}}'>{{$forca_par->percentual}}</td>
						<td id="{{$moeda}}-w-{{$par}}-buy">{{$forca_par->buy}}</td>
						<td id="{{$moeda}}-w-{{$par}}-sell">{{$forca_par->sell}}</td>
						<td id="{{$moeda}}-w-{{$par}}-diff">{{$forca_par->diff}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>			
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 table_forca_moedas">
			<span class="content-box-title">{{$moeda}} - Mensal</span>
			<table class='table table-striped'>
				<thead>
					<tr>
						<th>Par</th>
						<th>Ind</th>
						<th>Buy</th>
						<th>Sell</th>
						<th>Diff</th>
					</tr>
				</thead>
				<tbody>
					@foreach($forca_da_moeda->m as $par=>$forca_par)
					<tr>
						<td id="{{$moeda}}-m-{{$par}}-par">{{$par}}</td>
						<td id="{{$moeda}}-m-{{$par}}-percentual" class='currency_power_limit_{{$forca_par->percentual_indicator}}'>{{$forca_par->percentual}}</td>
						<td id="{{$moeda}}-m-{{$par}}-buy">{{$forca_par->buy}}</td>
						<td id="{{$moeda}}-m-{{$par}}-sell">{{$forca_par->sell}}</td>
						<td id="{{$moeda}}-m-{{$par}}-diff">{{$forca_par->diff}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>			
		</div>				
	</div>
	@endforeach
</div>
<br/>
<span class="content-box-title">LIVRO DE OPORTUNIDADES DE MOEDAS <a href="#" id="button_show_tendencia"><i class="glyphicon glyphicon-menu-down"></i></a></span>
<div class="content-box" id="forca_tendencia_board">
	<div class="forca_tendencia_diario">
		<span class="content-box-title">DIÁRIO - {{$forca_tendencia_atual->max_ref_date_d}}</span>
		<div class="content-box">
			<table class='table table-striped table-bordered'>
				<thead>
					<tr>
						<th>Ativo</th>
						<th>Cotação</th>
						<th>T</th>
						<th>I</th>
						<th>Sinalizador</th>
					</tr>
				</thead>
				<tbody>
					@forelse($forca_tendencia_atual->quotes_d as $quote)
					<tr>
						<td>{{$quote->pair}}</td>
						<td>{{$quote->close}}</td>
						<td>{!! $quote->t !!}</td>
						<td>{{$quote->i}}</td>
						<td class='ft_semaforo'>{!! $quote->sinalizador !!}</td>
					</tr>
					@empty
					<tr><td colspan=5>Não há ativos nestas condições</td></tr>
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
	<br/>
	<div class="forca_tendencia_semanal">
		<span class="content-box-title">SEMANAL - {{$forca_tendencia_atual->max_ref_date_w}}</span>
		<div class="content-box">
			<table class='table table-striped table-bordered'>
				<thead>
					<tr>
						<th>Ativo</th>
						<th>Cotação</th>
						<th>T</th>
						<th>I</th>
						<th>Sinalizador</th>
					</tr>
				</thead>
				<tbody>
					@forelse($forca_tendencia_atual->quotes_w as $quote)
					<tr>
						<td>{{$quote->pair}}</td>
						<td>{{$quote->close}}</td>
						<td>{!! $quote->t !!}</td>
						<td>{{$quote->i}}</td>
						<td class='ft_semaforo'>{!! $quote->sinalizador !!}</td>
					</tr>
					@empty
					<tr><td colspan=5>Não há ativos nestas condições</td></tr>
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
	<br/>
	<div class="forca_tendencia_mensal">
		<span class="content-box-title">MENSAL - {{$forca_tendencia_atual->max_ref_date_m}}</span>
		<div class="content-box">
			<table class='table table-striped table-bordered'>
				<thead>
					<tr>
						<th>Ativo</th>
						<th>Cotação</th>
						<th>T</th>
						<th>I</th>
						<th>Sinalizador</th>
					</tr>
				</thead>
				<tbody>
					@forelse($forca_tendencia_atual->quotes_m as $quote)
					<tr>
						<td>{{$quote->pair}}</td>
						<td>{{$quote->close}}</td>
						<td>{!! $quote->t !!}</td>
						<td>{{$quote->i}}</td>
						<td class='ft_semaforo'>{!! $quote->sinalizador !!}</td>
					</tr>
					@empty
					<tr><td colspan=5>Não há ativos nestas condições</td></tr>
					@endforelse
				</tbody>
			</table>
		</div>
	</div>        
</div>

<br/>
<span class="content-box-title">SEMÁFOROS <a href="#" id="button_show_semaforos"><i class="glyphicon glyphicon-menu-down"></i></a></span>
<div class="content-box" id="semaforos_board">
	<div class="semaforos_diario">		
		<span class="content-box-title">DIÁRIO - <span id="sem_max_ref_date_d"></span></span>
		<div class="content-box">
			<table class='table table-striped table-bordered'>
				<thead>
					<tr id="sem_quotes_d_thead">
					</tr>
				</thead>
				<tbody>
					<tr id="sem_quotes_d_tbody">                  
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<br/>	
	<div class="forca_tendencia_semanal">		
		<span class="content-box-title">SEMANAL - <span id="sem_max_ref_date_w"></span></span>
		<div class="content-box">
			<table class='table table-striped table-bordered'>
				<thead>
					<tr id="sem_quotes_w_thead">
					</tr>
				</thead>
				<tbody>
					<tr id="sem_quotes_w_tbody">                  
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<br/>	
	<div class="forca_tendencia_mensal">		
		<span class="content-box-title">MENSAL - <span id="sem_max_ref_date_m"></span></span>
		<div class="content-box">
			<table class='table table-striped table-bordered'>
				<thead>
					<tr id="sem_quotes_m_thead">
					</tr>
				</thead>
				<tbody>
					<tr id="sem_quotes_m_tbody">                  
					</tr>
				</tbody>
			</table>
		</div>
	</div>      
</div>
<!-- <br/>
<span class="content-box-title">COTAÇÃO <a href="#" id="button_show_quote"><i class="glyphicon glyphicon-menu-down"></i></a></span>
<div class="" id="external_quote_chart">
   <iframe id="widgetMataf" style="border: none; overflow:hidden; background-color: transparent; height: 600px; width: 100%" src="https://www.mataf.net/pt/widget/currencyindex"></iframe> 
</div> -->

<!-- <br/>
<span class="content-box-title">COTAÇÃO</span>
<div class="chart-box">
	<div id="hc_hist_quotes" class="hc_charts"></div>
</div> -->

<br/>
<span class="content-box-title">Força Tendência</span>
<div class="chart-box">
	<div id="hc_forca" class='hc_charts'></div>	
</div>

<br/>
<span class="content-box-title">Direção Tendência</span>
<div class="chart-box">
	<div id="hc_volat"></div>	
</div>

<br/>
<span class="content-box-title">Index Volatility Average</span>
<div class="chart-box">
	<div id="hc_index_volat_avg"></div>   
</div>

<br/>
<span class="content-box-title">Gráfico RV</span>
<div class="chart-box">
	<div id="hc_rv"></div>   
</div>

<br/>
<span class="content-box-title">Gráfico Fator</span>
<div class="chart-box">
	<div id="hc_fator"></div>   
</div>

<br/>
<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div>
			<div class="content-box-title">
				Flow money {{$retornos_periodo_titulo}} - CR: {!! $retornos_atual_padrao !!}
			</div>
			<div class="content-box">
				<table class='table table-striped table-bordered'>
					<thead>
						<tr>
							<th>DP-3</th>
							<th>DP-2</th>
							<th>DP-1</th>
							<th>Média</th>
							<th>DP+1</th>
							<th>DP+2</th>
							<th>DP+3</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>{!! $retornos_desvio_padrao->d_3 !!}</td>
							<td>{!! $retornos_desvio_padrao->d_2 !!}</td>
							<td>{!! $retornos_desvio_padrao->d_1 !!}</td>
							<td>{!! $retornos_desvio_padrao->d0 !!}</td>
							<td>{!! $retornos_desvio_padrao->d1 !!}</td>
							<td>{!! $retornos_desvio_padrao->d2 !!}</td>
							<td>{!! $retornos_desvio_padrao->d3 !!}</td>
						</tr>                            
					</tbody>                            
				</table>
			</div>
		</div>
	</div>            
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div>
			<div class="content-box-title">
				Flow money {{$retornos_periodo_titulo}} normalizado - CR: {!! $retornos_atual_normalizado !!}
			</div>
			<div class="content-box">
				<table class='table table-striped table-bordered'>
					<thead>
						<tr>
							<th>DP-3</th>
							<th>DP-2</th>
							<th>DP-1</th>
							<th>Média</th>
							<th>DP+1</th>
							<th>DP+2</th>
							<th>DP+3</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>{!! $retornos_desvio_normalizado->d_3 !!}</td>
							<td>{!! $retornos_desvio_normalizado->d_2 !!}</td>
							<td>{!! $retornos_desvio_normalizado->d_1 !!}</td>
							<td>{!! $retornos_desvio_normalizado->d0 !!}</td>
							<td>{!! $retornos_desvio_normalizado->d1 !!}</td>
							<td>{!! $retornos_desvio_normalizado->d2 !!}</td>
							<td>{!! $retornos_desvio_normalizado->d3 !!}</td>
						</tr>                            
					</tbody>                            
				</table>
			</div>
		</div>
	</div>               
</div>
@can('isAdmin')
<br/>
<span class="content-box-title">FLOW MONEY - Extrair dados em CSV</span>
<div class="content-box">
	<div class="row">
		<div class='col-md-3 col-sm-6 col-xs-12'>
			<div class="form-group">
				<label>Moeda:</label>
				<select class='form-control' id="flow_money_extract_moeda">
					<option value='EUR'>EUR</option>
					<option value='GBP'>GBP</option>
					<option value='AUD'>AUD</option>
					<option value='NZD'>NZD</option>
					<option value='USD'>USD</option>
					<option value='CAD'>CAD</option>
					<option value='CHF'>CHF</option>
					<option value='JPY'>JPY</option>
				</select>
			</div>
		</div>
		<div class='col-md-3 col-sm-6 col-xs-12'>
			<div class="form-group">
				<label>Período:</label>
				<select class='form-control' id="flow_money_extract_periodo">
					<option value='d'>Diário</option>
					<option value='w'>Semanal</option>
					<option value='m'>Mensal</option>
				</select>
			</div>
		</div>	
		<div class='col-md-2 col-sm-4 col-xs-12'>
			<div class="form-group">
				<label for='start_date'>De:</label>
				<input type="text" id="flow_money_extract_start_date" class='form-control as_mask_date date_picker'>
			</div>		
		</div>
		<div class='col-md-2 col-sm-4 col-xs-12'>
			<div class="form-group">
				<label for='end_date'>Até:</label>
				<input type="text" id="flow_money_extract_end_date" class='form-control as_mask_date date_picker'>
			</div>		
		</div>
		<div class='col-md-2 col-sm-4 col-xs-12'>
			<div class="form-group">
				<label for='flow_money_extract_qtd_periodos'>Qtd Períodos:</label>
				<input type="text" id="flow_money_extract_qtd_periodos" value='12' class='form-control as_mask_phone_prefix'>
			</div>		
		</div>		
	</div>
	<div class="row">
		<div class="col-xs-12 right">
			<button class='btn btn-primary' id="btn_flow_money_extract">Baixar CSV</button>
		</div>
	</div>
</div>
@endcan
<br/>
<span class="content-box-title">Flow money {{$retornos_periodo_titulo}}</span>
<div class="chart-box">
	<div id="hc_retornos_padrao"></div>   
</div>


<br/>
<span class="content-box-title">Flow money {{$retornos_periodo_titulo}} normalizado</span>
<div class="chart-box">
	<div id="hc_retornos_normalizado"></div>   
</div>

<br/>
<span class="content-box-title">Volatilidade ação reversa <a href="#" id="button_show_graph_vol_acao_reversa" class='toggle_button'><i class="glyphicon glyphicon-menu-down"></i></a></span>
<div class="chart-box" id='graph_vol_acao_reversa'>
	<div id="hc_media_volat"></div>   
</div>

<br/>
<span class="content-box-title">Razão Força Index <a href="#" id="button_show_graph_razao_forca_index" class='toggle_button'><i class="glyphicon glyphicon-menu-down"></i></a></span>
<div class="chart-box" id='graph_razao_forca_index'>
	<div id="hc_razao_rvi"></div>	
</div>



<br/>
<span class="content-box-title">Índice Canal Convergente <a href="#" id="button_show_graph_indice_canal_conv" class='toggle_button'><i class="glyphicon glyphicon-menu-down"></i></a></span>
<div class="chart-box" id='graph_indice_canal_conv'>
	<div id="hc_indice_conv"></div>   
</div>


<br/>
<span class="content-box-title">Equilibrio média em função volatilidade <a href="#" id="button_show_graph_media_volatilidade" class='toggle_button'><i class="glyphicon glyphicon-menu-down"></i></a></span>
<div class="chart-box" id='graph_media_volatilidade'>
	<div id="hc_eq_media_volat"></div>   
</div>
@stop

@section('css')
<link href="{{ url('/')}}/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.css"/>
<link rel="stylesheet" href='{{ url("/") }}/css/ogefx.css?v=202004062359'>
@stop

@section('js')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/sorting/datetime-moment.js"></script>	
<script src='{{ url("/") }}/js/jquery.mask.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.pt-BR.min.js'></script>
<script src='{{ url("/") }}/js/auge_masks.js'></script>
<script type="text/javascript" src="{{ url('/')}}/js/sweetalert2.all.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script type="text/javascript">

	var color_md12 = "#577fa5";
	var color_md5 = "#c5a14b";
	var color_dark = "#666666";
	var graph1 = "#43ada6";



	$(document).ready(function() {
		$.fn.dataTable.moment( "DD/MM/YYYY");
		jQuery.extend( jQuery.fn.dataTableExt.oSort, {
			"currency-pre": function ( a ) {
		        // a = (a==="-") ? 0 : a.replace( /[^\d\-\.]/g, "" );
		        a = a.replace("R$","").replace(/\./g,"").replace(",",".");
		        return parseFloat( a );
		    },

		    "currency-asc": function ( a, b ) {
		    	return a - b;
		    },

		    "currency-desc": function ( a, b ) {
		    	return b - a;
		    }
		} );		
		$('[data-toggle="tooltip"]').tooltip();
		$('#standard_table').DataTable({
			"order" : [0, 'desc'],
			"pageLength": 10,
			"responsive" :false,
			"scrollX": true,
			// scrollCollapse: true,
	  //       fixedColumns:   {
	  //           leftColumns: 1	            
	  //       },
	  "language": {
	  	"lengthMenu": "Mostrando  _MENU_  cotações por página",
	  	"emptyTable": "Nenhuma cotação encontrada",
	  	"search":         "Pesquisar: ",
	  	"zeroRecords": "Nenhuma cotação encontrada",
	  	"info": "Mostrando página  _PAGE_  de  _PAGES_",
	  	"infoEmpty": "Nenhuma cotação encontrada",
	  	"infoFiltered": "(filtrado de _MAX_ cotações)",				
	  	"paginate": {
	  		"first":      "Primeira",
	  		"last":       "Última",
	  		"next":       "Próxima",
	  		"previous":   "Anterior"
	  	},
	  	"aria": {
	  		"sortAscending":  ": ative para a ordenação crescente",
	  		"sortDescending": ": ative para a ordenação decrescente"
	  	}					    
	  }    			
	});		
		$("#start_date").datepicker({
			format: "dd/mm/yyyy",
			todayBtn: true,
			autoclose: true,
			language: "pt-BR",
			clearBtn: true		
		});	

		$("#end_date").datepicker({
			format: "dd/mm/yyyy",
			todayBtn: true,
			autoclose: true,
			language: "pt-BR",
			clearBtn: true
		});		

		$(".date_picker").datepicker({
			format: "dd/mm/yyyy",
			todayBtn: true,
			autoclose: true,
			language: "pt-BR",
			clearBtn: true
		});		

		$("#sim_check_opportunity").click(function(){
			sim_check_opportunity();        
		});

		$("#sim_clean_opportunity").click(function(){
			$("#sim_last_quote").val("");
			$(".sim_result").html("");
		});

		$("#button_show_quote").click(function(){
			$("#external_quote_chart").slideToggle();
			$("#button_show_quote i").toggleClass('glyphicon-menu-up');
			return false;
		});

		$("#button_show_forca_moedas").click(function(){
			$("#forca_moedas_board").slideToggle();
			$("#button_show_forca_moedas i").toggleClass('glyphicon-menu-up');
			return false;
		});		

		$("#button_show_tendencia").click(function(){
			$("#forca_tendencia_board").slideToggle();
			$("#button_show_tendencia i").toggleClass('glyphicon-menu-up');
			return false;
		});

		$("#button_show_semaforos").click(function(){
			$("#semaforos_board").slideToggle();
			$("#button_show_semaforos i").toggleClass('glyphicon-menu-up');
			return false;
		}); 

		$("#button_show_graph_vol_acao_reversa").click(function(){
			$("#graph_vol_acao_reversa").slideToggle();
			$("#button_show_graph_vol_acao_reversa i").toggleClass('glyphicon-menu-up');
			return false;
		});          

		$("#button_show_graph_razao_forca_index").click(function(){
			$("#graph_razao_forca_index").slideToggle();
			$("#button_show_graph_razao_forca_index i").toggleClass('glyphicon-menu-up');
			return false;
		}); 

		$("#button_show_graph_indice_canal_conv").click(function(){
			$("#graph_indice_canal_conv").slideToggle();
			$("#button_show_graph_indice_canal_conv i").toggleClass('glyphicon-menu-up');
			return false;
		}); 

		$("#button_show_graph_media_volatilidade").click(function(){
			$("#graph_media_volatilidade").slideToggle();
			$("#button_show_graph_media_volatilidade i").toggleClass('glyphicon-menu-up');
			return false;
		});           

		var conv_dmy_ymd = function(parm){
			if (!parm) return null;

			var parts = parm.split("/");
			return parts[2] + "-" + parts[1] + '-' + parts[0];
		};

		$("#btn_flow_money_extract").click(function(){
			var link = "/admin/cotacoes/flow-money-csv/" 
			+ $("#flow_money_extract_moeda").val() + "/" 
			+ $("#flow_money_extract_periodo").val() + "/"
			+ $("#flow_money_extract_qtd_periodos").val() + "/"
			+ conv_dmy_ymd($("#flow_money_extract_start_date").val()) + "/"
			+ conv_dmy_ymd($("#flow_money_extract_end_date").val());
			window.location.replace(link);
		});

      // render_hc_hist_quotes();
      render_hc_forca();
      render_hc_volat();
      render_hc_media_volat();
      render_hc_razao_rvi();
      render_hc_index_volat_avg();
      render_hc_indice_conv();
      render_hc_eq_media_volat();       
      render_hc_retornos_padrao(); 
      render_hc_retornos_normalizado();
      render_hc_rv(); 
      render_hc_fator(); 

      forca_das_moedas();
      semaforo_todas_as_cotacoes();
      semaforo_cruzamento_media();

      // ind_forca_moedas = setInterval(function(){forca_das_moedas();},1000);
      ind_forca_moedas = setInterval(
      						function(){
      							forca_das_moedas();
      							semaforo_cruzamento_media();
      						},
      						(1 * 60 * 1000));

  } );	

	function semaforo_cruzamento_media_indicador(ind){

		if (ind == 0) return "glyphicon glyphicon-record neutral_percentage";
		if (ind == 1) return "glyphicon glyphicon-circle-arrow-down negative_percentage";
		return "glyphicon glyphicon-circle-arrow-up positive_percentage";

	}
	function semaforo_cruzamento_media(){

		var pair = document.getElementById("pair").value;
		var dir_cruzmedia_15 = document.getElementById("dir_cruzmedia_15");
		var dir_cruzmedia_30 = document.getElementById("dir_cruzmedia_30");
		var dir_cruzmedia_60 = document.getElementById("dir_cruzmedia_60");
		var dir_cruzmedia_240 = document.getElementById("dir_cruzmedia_240");
		var dir_cruzmedia_1440 = document.getElementById("dir_cruzmedia_1440");

		if (pair=="sel"){
			var neutral_class = semaforo_cruzamento_media_indicador(0);
			dir_cruzmedia_15.className = neutral_class;
			dir_cruzmedia_30.className = neutral_class;
			dir_cruzmedia_60.className = neutral_class;
			dir_cruzmedia_240.className = neutral_class;
			dir_cruzmedia_1440.className = neutral_class;
			return;
		}

		url = "{{ url('/admin/cotacoes/semaforo_cruzamento_media/') }}" + "/" + pair;

		$.get(url,function(data){

			retorno = JSON.parse(data);

			console.log(retorno);

			var ind_15_cruzamento_media = retorno.ind_15_cruzamento_media;
			var ind_30_cruzamento_media = retorno.ind_30_cruzamento_media;
			var ind_60_cruzamento_media = retorno.ind_60_cruzamento_media;
			var ind_240_cruzamento_media = retorno.ind_240_cruzamento_media;
			var ind_1440_cruzamento_media = retorno.ind_1440_cruzamento_media;

			dir_cruzmedia_15.className = semaforo_cruzamento_media_indicador(ind_15_cruzamento_media);
			dir_cruzmedia_30.className = semaforo_cruzamento_media_indicador(ind_30_cruzamento_media);
			dir_cruzmedia_60.className = semaforo_cruzamento_media_indicador(ind_60_cruzamento_media);
			dir_cruzmedia_240.className = semaforo_cruzamento_media_indicador(ind_240_cruzamento_media);
			dir_cruzmedia_1440.className = semaforo_cruzamento_media_indicador(ind_1440_cruzamento_media);

		});

		return;

	}

	function add_to_portfolio(){

		data = {
			pair: '{{$return_analysis->pair}}',
			period: '{{$return_analysis->period}}',
			start_date: '{{$return_analysis->first_ref_date_raw}}',
			end_date: '{{$return_analysis->last_ref_date_raw}}'
		};

		url = "{{ url('/admin/portfolio/create') }}";

		$.post(url,data,function(data){
			sim = JSON.parse(data);
			console.log(sim);
			swal('Adicionado','Portfolio adicionado','success');
		});           

	}
	function semaforo_todas_as_cotacoes(){

		url = "{{ url('/admin/cotacoes/semaforo_todas_as_moedas') }}";

		$.get(url,function(data){

			retorno = JSON.parse(data);

			console.log(retorno);

			$("#sem_max_ref_date_d").html(retorno.max_ref_date_d);
			$("#sem_max_ref_date_w").html(retorno.max_ref_date_w);
			$("#sem_max_ref_date_m").html(retorno.max_ref_date_m);

			var thead_d = document.getElementById("sem_quotes_d_thead");
			var tbody_d = document.getElementById("sem_quotes_d_tbody");

			thead_d.innerHTML = "";
			tbody_d.innerHTML = "";

			for (x in retorno.quotes_d){

				thead_d.innerHTML+= "<th>" + retorno.quotes_d[x].pair + "</th>";

				var td = document.createElement('td');
				td.classList.add('ft_semaforo');

				var sin = document.createElement('span');
				sin.classList.add('span_block');
				sin.innerHTML = retorno.quotes_d[x].sinalizador;
				td.appendChild(sin);

				var sp_tags = document.createElement("span");
				sp_tags.classList.add('span_block');

				var tag_rv = document.createElement("span");
				tag_rv.classList.add("semaforo_tag");
				tag_rv.classList.add("standard_tag");
				tag_rv.innerHTML = "RV";
				sp_tags.appendChild(tag_rv);

				var rv = document.createElement("span");
				if (retorno.quotes_d[x].ret_rv.last_relacao_volume_marked){
					rv.classList.add("rv_tag");
					rv.classList.add("red_tag");
				}
				rv.innerHTML = retorno.quotes_d[x].ret_rv.last_relacao_volume;
				sp_tags.appendChild(rv);
				td.appendChild(sp_tags);

				tbody_d.appendChild(td);

			}

			var thead_w = document.getElementById("sem_quotes_w_thead");
			var tbody_w = document.getElementById("sem_quotes_w_tbody");

			thead_w.innerHTML = "";
			tbody_w.innerHTML = "";

			for (x in retorno.quotes_w){

				thead_w.innerHTML+= "<th>" + retorno.quotes_w[x].pair + "</th>";

				var td = document.createElement('td');
				td.classList.add('ft_semaforo');

				var sin = document.createElement('span');
				sin.classList.add('span_block');
				sin.innerHTML = retorno.quotes_w[x].sinalizador;
				td.appendChild(sin);

				var sp_tags = document.createElement("span");
				sp_tags.classList.add('span_block');

				var tag_rv = document.createElement("span");
				tag_rv.classList.add("semaforo_tag");
				tag_rv.classList.add("standard_tag");
				tag_rv.innerHTML = "RV";
				sp_tags.appendChild(tag_rv);

				var rv = document.createElement("span");
				if (retorno.quotes_w[x].ret_rv.last_relacao_volume_marked){
					rv.classList.add("rv_tag");
					rv.classList.add("red_tag");
				}
				rv.innerHTML = retorno.quotes_w[x].ret_rv.last_relacao_volume;
				sp_tags.appendChild(rv);
				td.appendChild(sp_tags);

				tbody_w.appendChild(td);

			}

			var thead_m = document.getElementById("sem_quotes_m_thead");
			var tbody_m = document.getElementById("sem_quotes_m_tbody");

			thead_m.innerHTML = "";
			tbody_m.innerHTML = "";

			for (x in retorno.quotes_m){

				thead_m.innerHTML+= "<th>" + retorno.quotes_m[x].pair + "</th>";

				var td = document.createElement('td');
				td.classList.add('ft_semaforo');

				var sin = document.createElement('span');
				sin.classList.add('span_block');
				sin.innerHTML = retorno.quotes_m[x].sinalizador;
				td.appendChild(sin);

				var sp_tags = document.createElement("span");
				sp_tags.classList.add('span_block');

				var tag_rv = document.createElement("span");
				tag_rv.classList.add("semaforo_tag");
				tag_rv.classList.add("standard_tag");
				tag_rv.innerHTML = "RV";
				sp_tags.appendChild(tag_rv);

				var rv = document.createElement("span");
				if (retorno.quotes_m[x].ret_rv.last_relacao_volume_marked){
					rv.classList.add("rv_tag");
					rv.classList.add("red_tag");
				}
				rv.innerHTML = retorno.quotes_m[x].ret_rv.last_relacao_volume;
				sp_tags.appendChild(rv);
				td.appendChild(sp_tags);

				tbody_m.appendChild(td);

			}			
		});          

	}

	function usuario_nao_autorizado(error){
		window.location.ref="/expirado";
	}

	function forca_das_moedas(){

		url = "{{ url('/admin/cotacoes/forca') }}";

		$.ajax({
			url: url,
			success: forca_das_moedas_render,
			error: usuario_nao_autorizado
		});

	}

	function forca_das_moedas_render(data){

		

		retorno = JSON.parse(data);

		$("#forca_ultima_atualizacao").html(moment(retorno.ultima_atualizacao).format("D/MM/YYYY H:mm"));

		forca_commodities = retorno.commodities;
		for  (i_periodo in forca_commodities){
			for (i_comm in forca_commodities[i_periodo]){
				var percentual = forca_commodities[i_periodo][i_comm].percentual;
				var percentual_indicator = forca_commodities[i_periodo][i_comm].percentual_indicator;
				var periodo = forca_commodities[i_periodo][i_comm].periodo;
				var comm = forca_commodities[i_periodo][i_comm].commodity;
				$("#forca_geral_moeda_" + comm + "_" + periodo + "_percentual").html(percentual);
				$("#forca_geral_moeda_" + comm + "_" + periodo + "_percentual").removeClass("currency_power_limit_low");
				$("#forca_geral_moeda_" + comm + "_" + periodo + "_percentual").removeClass("currency_power_limit_high");
				$("#forca_geral_moeda_" + comm + "_" + periodo + "_percentual").addClass("currency_power_limit_"+ percentual_indicator);
			}
		}			

		forca_geral = retorno.forca_geral;
		for  (i_periodo in forca_geral){
			for (i_moeda in forca_geral[i_periodo]){
				var percentual = forca_geral[i_periodo][i_moeda].percentual;
				var percentual_indicator = forca_geral[i_periodo][i_moeda].percentual_indicator;
				var periodo = forca_geral[i_periodo][i_moeda].periodo;
				var moeda_base = forca_geral[i_periodo][i_moeda].moeda_base;
				$("#forca_geral_moeda_" + moeda_base + "_" + periodo + "_percentual").html(percentual);
				$("#forca_geral_moeda_" + moeda_base + "_" + periodo + "_percentual").removeClass("currency_power_limit_low");
				$("#forca_geral_moeda_" + moeda_base + "_" + periodo + "_percentual").removeClass("currency_power_limit_high");
				$("#forca_geral_moeda_" + moeda_base + "_" + periodo + "_percentual").removeClass("currency_power_limit_yellow");
				$("#forca_geral_moeda_" + moeda_base + "_" + periodo + "_percentual").removeClass("currency_power_limit_orange");								
				$("#forca_geral_moeda_" + moeda_base + "_" + periodo + "_percentual").addClass("currency_power_limit_"+ percentual_indicator);
			}
		}

		forca = retorno.forca_por_par;
		for (i_moeda in forca){
			for (i_periodo in forca[i_moeda]){
				for (i_par in forca[i_moeda][i_periodo]){
					var forca_valores = forca[i_moeda][i_periodo][i_par];
					var moeda = forca_valores.moeda;
					var periodo = forca_valores.periodo;
					var par = forca_valores.par;
					var percentual = forca_valores.percentual;
					var range_raw = forca_valores.range_raw;
					var buy = forca_valores.buy;
					var sell = forca_valores.sell;
					var diff = forca_valores.diff;
					var percentual_indicator = forca_valores.percentual_indicator;
					var buy_indicator = forca_valores.buy_indicator;
					var sell_indicator = forca_valores.sell_indicator;

					$("#"+moeda+"-"+periodo+"-"+par+"-par").html(par);

					$("#"+moeda+"-"+periodo+"-"+par+"-percentual").html(percentual);
					$("#"+moeda+"-"+periodo+"-"+par+"-percentual").removeClass("currency_power_limit_high");
					$("#"+moeda+"-"+periodo+"-"+par+"-percentual").removeClass("currency_power_limit_low");
					$("#"+moeda+"-"+periodo+"-"+par+"-percentual").removeClass("currency_power_limit_orange");
					$("#"+moeda+"-"+periodo+"-"+par+"-percentual").removeClass("currency_power_limit_yellow");
					$("#"+moeda+"-"+periodo+"-"+par+"-percentual").addClass("currency_power_limit_"+ percentual_indicator);	

					$("#"+moeda+"-"+periodo+"-"+par+"-buy").html(buy);
					$("#"+moeda+"-"+periodo+"-"+par+"-buy").removeClass("currency_power_limit_high");
					$("#"+moeda+"-"+periodo+"-"+par+"-buy").removeClass("currency_power_limit_low");
					$("#"+moeda+"-"+periodo+"-"+par+"-buy").removeClass("currency_power_limit_orange");
					$("#"+moeda+"-"+periodo+"-"+par+"-buy").removeClass("currency_power_limit_yellow");
					$("#"+moeda+"-"+periodo+"-"+par+"-buy").addClass("currency_power_limit_"+ buy_indicator);						
					$("#"+moeda+"-"+periodo+"-"+par+"-sell").html(sell);	
					$("#"+moeda+"-"+periodo+"-"+par+"-sell").removeClass("currency_power_limit_high");
					$("#"+moeda+"-"+periodo+"-"+par+"-sell").removeClass("currency_power_limit_low");
					$("#"+moeda+"-"+periodo+"-"+par+"-sell").removeClass("currency_power_limit_orange");
					$("#"+moeda+"-"+periodo+"-"+par+"-sell").removeClass("currency_power_limit_yellow");
					$("#"+moeda+"-"+periodo+"-"+par+"-sell").addClass("currency_power_limit_"+ sell_indicator);									
					$("#"+moeda+"-"+periodo+"-"+par+"-diff").html(diff);										
				}					
			}				
		}
		

	}

	function sim_check_opportunity(){

		periodo = $("#sim_period option:selected").val();          
		quote = $("#sim_last_quote").val();
		start_date = $("#sim_start_date").val();
		end_date = $("#sim_end_date").val();

		if (periodo=="sel"){
			swal("Período","Informe o período de simulação","warning");
			return false;
		}

		if (quote.trim()==""){
			swal("Cotação","Informe a cotação para simulação","warning");
			return false;
		}

		if (isNaN(quote)){
			swal("Cotação","A cotação informada para simulação não é válida",'error');
			return false;
		}

		url = "{{ url('/admin/analise-tecnica/simulador') }}";

		data = {
			quote: quote,
			period: periodo,
			pair: '{{ $selected_pair}}',
			start_date: start_date,
			end_date: end_date
		};

		$.post(url,data,function(ret){

			try{
				sim = JSON.parse(ret);
				console.log(sim);
				$("#sim_res_ref_date").html(sim.ref_date);
				$("#sim_res_tend").html(sim.action);
				$("#sim_res_target").html(sim.target_quote);
				$("#sim_res_points").html(sim.points);            
				$("#sim_res_rsi").html(sim.rsi_normal);
				$("#sim_res_rvi").html(sim.rvi);                
			} catch (e){
				alert(e);
				alert(ret);
			}

		});      

	}

	function render_hc_hist_quotes(){

		Highcharts.chart('hc_hist_quotes', {
			chart:{
				backgroundColor: '#000000'
			},
			title:{
				text: ""
			},	
			dateFormat: "%d",
			tooltip:{
				xDateFormat: '%d-%m-%Y',
				shared: true        
			},
			yAxis: {
				title: {
					text: ''
				},
				labels:{
					style:{color:'#fff'}
				},
				crosshair: true,
				gridLineWidth:0,
				tickAmount: 10
			},
			legend: {
				enabled: false
			},

			xAxis: {
				type: 'datetime',
				labels:{
					formatter: function(){        		
						var dat = new Date(this.value);
						return dat.toLocaleDateString('pt-BR');        		
					},
					style: {color:'#fff'}        	
				},
				crosshair: true,
				lineColor: '#fff',
				tickColor: '#fff',
				labelColor: '#fff',
				tickPixelInterval:50,
				showFirstLabel: true
			},
			series: [
			{
				name: 'Cotação',
				data: {!! $j_hc_hist_quotes !!},
				color: graph1,
				lineWidth:1     
			},
			{
				name: 'MD5',
				data: {!! $j_hc_hist_quotes_media_5 !!},
				lineWidth:2,
				color: color_md5 
			},
			{
				name: 'MD12',
				data: {!! $j_hc_hist_quotes_media_12 !!},
				lineWidth:2,
				color: color_md12     
			},        
			],

			responsive: {
				rules: [{
					condition: {
						maxWidth: 500
					},
					chartOptions: {
						legend: {
							layout: 'horizontal',
							align: 'center',
							verticalAlign: 'bottom'
						}
					}
				}]
			}

		});
	}

	function render_hc_forca(){

		Highcharts.chart('hc_forca', {
			chart:{
				backgroundColor: '#000000'
			},
			title:{
				text: ""
			},	
			dateFormat: "%d",
			tooltip:{
				xDateFormat: '%d-%m-%Y',
				shared: true        
			},
			yAxis: {
				title: {
					text: ''
				},
				labels:{
					style:{color:'#fff'}
				},
				crosshair: true,
				gridLineWidth:0,
				tickAmount: 11,
				max: 100,
				plotLines:  [
				{ 
					color: 'red',
					width: 2,
					value: 33.33,
					dashStyle: 'longdashdot',
					label:{
						text:'33.33',
						style:{
							color:"red"
						}
					}
				},
				{ 
					color: 'yellow',
					width: 2,
					value: 50,
					dashStyle: 'longdashdot',
					label:{
						text:'50',
						style:{color:"yellow"}
					}               
				},
				{ 
					color: 'blue',
					width: 2,
					value: 66.67,
					dashStyle: 'longdashdot',
					label:{
						text:'66.67',
						style:{color:"blue"}
					}               
				},                        
				],          
			},
			legend: {
				enabled: false
			},

			xAxis: {
				type: 'datetime',
				labels:{
					formatter: function(){        		
						var dat = new Date(this.value);
						return dat.toLocaleDateString('pt-BR');        		
					},
					style: {color:'#fff'}        	
				},
				crosshair: true,
				lineColor: '#fff',
				tickColor: '#fff',
				labelColor: '#fff',
				tickPixelInterval:50,
				showFirstLabel: true
			},
			series: [
			{
				name: 'FTP',
				data: {!! $j_hc_forca_rsi_normal !!},
				color: '#000',
				lineWidth:1     
			},
			{
				name: 'MD5',
				data: {!! $j_hc_forca_media_5 !!},
				color: color_md5,
				lineWidth:2          
			},
			{
				name: 'MD12',
				data: {!! $j_hc_forca_media_12 !!},
				lineWidth:2,  
				color: color_md12
			},
			],

			responsive: {
				rules: [{
					condition: {
						maxWidth: 500
					},
					chartOptions: {
						legend: {
							layout: 'horizontal',
							align: 'center',
							verticalAlign: 'bottom'
						}
					}
				}]
			}

		});
	}

	function render_hc_volat(){

		Highcharts.chart('hc_volat', {
			chart:{
				backgroundColor: '#000000'
			},
			title:{
				text: ""
			},	
			dateFormat: "%d",
			tooltip:{
				xDateFormat: '%d-%m-%Y',
				shared: true        
			},
			yAxis: {
				title: {
					text: ''
				},
				labels:{
					style:{color:'#fff'}
				},
				crosshair: true,
				gridLineWidth:0,
				tickAmount: 10,
				plotLines: [
				{ 
					color: 'red',
					width: 2,
					value: 40,
					dashStyle: 'longdashdot',
					label:{
						text:'40',
						style:{color:"red"}
					}
				},
				{ 
					color: 'yellow',
					width: 2,
					value: 50,
					dashStyle: 'longdashdot',
					label:{
						text:'50',
						style:{color:"yellow"}
					}            	
				},
				{ 
					color: 'blue',
					width: 2,
					value: 60,
					dashStyle: 'longdashdot',
					label:{
						text:'60',
						style:{color:"blue"}
					}            	
				},                        
				],        
			},
			legend: {
				enabled: false
			},

			xAxis: {
				type: 'datetime',
				labels:{
					formatter: function(){        		
						var dat = new Date(this.value);
						return dat.toLocaleDateString('pt-BR');        		
					},
					style: {color:'#fff'}        	
				},
				crosshair: true,
				lineColor: '#fff',
				tickColor: '#fff',
				labelColor: '#fff',
				tickPixelInterval:50,
				showFirstLabel: true
			},
			series: [
			{
				name: 'RV',
				data: {!! $j_hc_volat_rvi !!},
				color: graph1,
				lineWidth:1         
			}
			],

			responsive: {
				rules: [{
					condition: {
						maxWidth: 500
					},
					chartOptions: {
						legend: {
							layout: 'horizontal',
							align: 'center',
							verticalAlign: 'bottom'
						}
					}
				}]
			}

		});
	}

	function render_hc_retornos_padrao(){

		Highcharts.chart('hc_retornos_padrao', {
			chart:{
				backgroundColor: '#000000'
			},
			title:{
				text: ""
			},    
			dateFormat: "%d",
			tooltip:{
				xDateFormat: '%d-%m-%Y',
				shared: true        
			},
			yAxis: {
				title: {
					text: ''
				},  
				labels:{
					style:{color:'#fff'}
				},
				crosshair: true,
				gridLineWidth:0,
				tickAmount: 10,
				plotLines:  [
				{ 
					color: 'red',
					width: 2,
					value: 0,
					dashStyle: 'longdashdot',
					label:{
						text:'0',
						style:{
							color:"red"
						}
					}
				}
				],                        
			},
			legend: {
				enabled: false
			},

			xAxis: {
				type: 'datetime',
				labels:{
					formatter: function(){               
						var dat = new Date(this.value);
						return dat.toLocaleDateString('pt-BR');               
					},
					style: {color:'#fff'}         
				},
				crosshair: true,
				lineColor: '#fff',
				tickColor: '#fff',
				labelColor: '#fff',
				tickPixelInterval:50,
				showFirstLabel: true
			},
			series: [
			{
				name: '{{$retornos_periodo_sigla}}',
				data: {!! $j_hc_retornos_padrao !!},
				color: graph1,
				lineWidth:1     
			},

			],

			responsive: {
				rules: [{
					condition: {
						maxWidth: 500
					},
					chartOptions: {
						legend: {
							layout: 'horizontal',
							align: 'center',
							verticalAlign: 'bottom'
						}
					}
				}]
			}

		});
	}

	function render_hc_retornos_normalizado(){

		Highcharts.chart('hc_retornos_normalizado', {
			chart:{
				backgroundColor: '#000000'
			},
			title:{
				text: ""
			},    
			dateFormat: "%d",
			tooltip:{
				xDateFormat: '%d-%m-%Y',
				shared: true        
			},
			yAxis: {
				title: {
					text: ''
				},
				labels:{
					style:{color:'#fff'}
				},
				crosshair: true,
				gridLineWidth:0,
				tickAmount: 11,
				max: 100,
				plotLines:  [
				{ 
					color: 'red',
					width: 2,
					value: 50,
					dashStyle: 'longdashdot',
					label:{
						text:'50%',
						style:{
							color:"red"
						}
					}
				}],                
			},
			legend: {
				enabled: false
			},

			xAxis: {
				type: 'datetime',
				labels:{
					formatter: function(){               
						var dat = new Date(this.value);
						return dat.toLocaleDateString('pt-BR');               
					},
					style: {color:'#fff'}         
				},
				crosshair: true,
				lineColor: '#fff',
				tickColor: '#fff',
				labelColor: '#fff',
				tickPixelInterval:50,
				showFirstLabel: true
			},
			series: [
			{
				name: '{{$retornos_periodo_sigla}}',
				data: {!! $j_hc_retornos_normalizado !!},
				color: graph1,
				lineWidth:1     
			},

			],

			responsive: {
				rules: [{
					condition: {
						maxWidth: 500
					},
					chartOptions: {
						legend: {
							layout: 'horizontal',
							align: 'center',
							verticalAlign: 'bottom'
						}
					}
				}]
			}

		});
	}
	function render_hc_media_volat(){

		Highcharts.chart('hc_media_volat', {
			chart:{
				backgroundColor: '#000000'
			},
			title:{
				text: ""
			},  
			dateFormat: "%d",
			tooltip:{
				xDateFormat: '%d-%m-%Y',
				shared: true        
			},
			yAxis: {
				title: {
					text: ''
				},
				labels:{
					style:{color:'#fff'}
				},
				crosshair: true,
				gridLineWidth:0,
				tickAmount: 11,
				max: 100,
				plotLines: [
				{ 
					color: 'red',
					width: 2,
					value: 40,
					dashStyle: 'longdashdot',
					label:{
						text:'40',
						style:{color:"red"}
					}
				},
				{ 
					color: 'yellow',
					width: 2,
					value: 50,
					dashStyle: '',
					label:{
						text:'50',
						style:{color:"yellow"}
					}               
				},
				{ 
					color: 'blue',
					width: 2,
					value: 60,
					dashStyle: 'longdashdot',
					label:{
						text:'60',
						style:{color:"blue"}
					}               
				},                        
				],        
			},
			legend: {
				enabled: false
			},

			xAxis: {
				type: 'datetime',
				labels:{
					formatter: function(){              
						var dat = new Date(this.value);
						return dat.toLocaleDateString('pt-BR');             
					},
					style: {color:'#fff'}           
				},
				crosshair: true,
				lineColor: '#fff',
				tickColor: '#fff',
				labelColor: '#fff',
				tickPixelInterval:50,
				showFirstLabel: true
			},
			series: [
			{
				name: 'MD5',
				data: {!! $j_hc_forca_media_5 !!},
				color: graph1,
				lineWidth:1         
			}
			],

			responsive: {
				rules: [{
					condition: {
						maxWidth: 500
					},
					chartOptions: {
						legend: {
							layout: 'horizontal',
							align: 'center',
							verticalAlign: 'bottom'
						}
					}
				}]
			}

		});
	}


	function render_hc_razao_rvi(){

		Highcharts.chart('hc_razao_rvi', {
			chart:{
				backgroundColor: '#000000'
			},
			title:{
				text: ""
			},	
			dateFormat: "%d",
			tooltip:{
				xDateFormat: '%d-%m-%Y',
				shared: true        
			},
			yAxis: {
				title: {
					text: ''
				},
				labels:{
					style:{color:'#fff'}
				},
				crosshair: true,
				gridLineWidth:0,
				tickAmount: 10,
				plotLines: [
				{ 
					color: 'red',
					width: 2,
					value: 1,
					dashStyle: 'longdashdot',
					label:{
						text:'1',
						style:{color:"red"}
					}
				},
				],        
			},
			legend: {
				enabled: false
			},

			xAxis: {
				type: 'datetime',
				labels:{
					formatter: function(){        		
						var dat = new Date(this.value);
						return dat.toLocaleDateString('pt-BR');        		
					},
					style: {color:'#fff'}        	
				},
				crosshair: true,
				lineColor: '#fff',
				tickColor: '#fff',
				labelColor: '#fff',
				tickPixelInterval:50,
				showFirstLabel: true
			},
			series: [
			{
				name: 'RFI',
				data: {!! $j_hc_rsi_rvi !!},
				color: graph1,
				lineWidth:1        
			}
			],

			responsive: {
				rules: [{
					condition: {
						maxWidth: 500
					},
					chartOptions: {
						legend: {
							layout: 'horizontal',
							align: 'center',
							verticalAlign: 'bottom'
						}
					}
				}]
			}

		});
	}

	function render_hc_index_volat_avg(){

		Highcharts.chart('hc_index_volat_avg', {
			chart:{
				backgroundColor: '#000000'
			},
			title:{
				text: ""
			},  
			dateFormat: "%d",
			tooltip:{
				xDateFormat: '%d-%m-%Y',
				shared: true        
			},
			yAxis: {
				title: {
					text: ''
				},
				labels:{
					style:{color:'#fff'},
					formatter: function(){
						return this.value + "%";
					}
				},
				crosshair: true,
				gridLineWidth:0,
				tickAmount: 10,
				plotLines: [
				{ 
					color: 'red',
					width: 2,
					value: 0,
					dashStyle: 'longdashdot',
					label:{
						text:'0%',
						style:{color:"red"}
					}
				},
				],        
			},
			legend: {
				enabled: false
			},

			xAxis: {
				type: 'datetime',
				labels:{
					formatter: function(){              
						var dat = new Date(this.value);
						return dat.toLocaleDateString('pt-BR');             
					},
					style: {color:'#fff'}           
				},
				crosshair: true,
				lineColor: '#fff',
				tickColor: '#fff',
				labelColor: '#fff',
				tickPixelInterval:50,
				showFirstLabel: true
			},
			series: [
			{
				name: 'IVA',
				data: {!! $j_hc_index_volat_avg_afast_ret !!},
				color: graph1,
				lineWidth:1            
			},
			{
				name: 'MD5',
				data: {!! $j_hc_forca_media_5 !!},
				lineWidth:2,
				color:color_md5           
			},
			{
				name: 'MD12',
				data: {!! $j_hc_forca_media_12 !!},
				lineWidth:2,
				color: color_md12         
			},        
			],

			responsive: {
				rules: [{
					condition: {
						maxWidth: 500
					},
					chartOptions: {
						legend: {
							layout: 'horizontal',
							align: 'center',
							verticalAlign: 'bottom'
						}
					}
				}]
			}

		});
	}

	function render_hc_rv(){

		Highcharts.chart('hc_rv', {
			chart:{
				backgroundColor: '#000000'
			},
			title:{
				text: ""
			},  
			dateFormat: "%d",
			tooltip:{
				xDateFormat: '%d-%m-%Y',
				shared: true        
			},
			yAxis: {
				title: {
					text: ''
				},
				labels:{
					style:{color:'#fff'},
					formatter: function(){
						return this.value + "%";
					}
				},
				crosshair: true,
				gridLineWidth:0,
				tickAmount: 10,
				// plotLines: [
				// { 
				// 	color: 'red',
				// 	width: 2,
				// 	value: 0,
				// 	dashStyle: 'longdashdot',
				// 	label:{
				// 		text:'0%',
				// 		style:{color:"red"}
				// 	}
				// },
				// ],        
			},
			legend: {
				enabled: false
			},

			xAxis: {
				type: 'datetime',
				labels:{
					formatter: function(){              
						var dat = new Date(this.value);
						return dat.toLocaleDateString('pt-BR');             
					},
					style: {color:'#fff'}           
				},
				crosshair: true,
				lineColor: '#fff',
				tickColor: '#fff',
				labelColor: '#fff',
				tickPixelInterval:50,
				showFirstLabel: true
			},
			series: [
			{
				name: 'RV',
				data: {!! $j_hc_rv !!},
				color: graph1,
				lineWidth:1            
			},
			],

			responsive: {
				rules: [{
					condition: {
						maxWidth: 500
					},
					chartOptions: {
						legend: {
							layout: 'horizontal',
							align: 'center',
							verticalAlign: 'bottom'
						}
					}
				}]
			}

		});
	}
	function render_hc_fator(){

		Highcharts.chart('hc_fator', {
			chart:{
				backgroundColor: '#000000'
			},
			title:{
				text: ""
			},  
			dateFormat: "%d",
			tooltip:{
				xDateFormat: '%d-%m-%Y',
				shared: true        
			},
			yAxis: {
				title: {
					text: ''
				},
				labels:{
					style:{color:'#fff'},
					formatter: function(){
						return this.value + "%";
					}
				},
				crosshair: true,
				gridLineWidth:0,
				tickAmount: 10,
				plotLines: [
				{ 
					color: 'red',
					width: 2,
					value: 0,
					dashStyle: 'longdashdot',
					label:{
						text:'0%',
						style:{color:"red"}
					}
				},
				],        
			},
			legend: {
				enabled: false
			},

			xAxis: {
				type: 'datetime',
				labels:{
					formatter: function(){              
						var dat = new Date(this.value);
						return dat.toLocaleDateString('pt-BR');             
					},
					style: {color:'#fff'}           
				},
				crosshair: true,
				lineColor: '#fff',
				tickColor: '#fff',
				labelColor: '#fff',
				tickPixelInterval:50,
				showFirstLabel: true
			},
			series: [
			{
				name: 'Fator',
				data: {!! $j_hc_fator !!},
				lineWidth:1,
				color:color_md5           
			},
			],

			responsive: {
				rules: [{
					condition: {
						maxWidth: 500
					},
					chartOptions: {
						legend: {
							layout: 'horizontal',
							align: 'center',
							verticalAlign: 'bottom'
						}
					}
				}]
			}

		});
	}
	function render_hc_indice_conv(){

		Highcharts.chart('hc_indice_conv', {
			chart:{
				backgroundColor: '#000000'
			},
			title:{
				text: ""
			},  
			dateFormat: "%d",
			tooltip:{
				xDateFormat: '%d-%m-%Y',
				shared: true        
			},
			yAxis: {
				title: {
					text: ''
				},
				labels:{
					style:{color:'#fff'},
					formatter: function(){
						return this.value + "%";
					}
				},
				crosshair: true,
				gridLineWidth:0,
				tickAmount: 10,
				plotLines: [
				{ 
					color: 'red',
					width: 2,
					value: 0,
					dashStyle: 'longdashdot',
					label:{
						text:'0%',
						style:{color:"red"}
					}
				},
				],        
			},
			legend: {
				enabled: false
			},

			xAxis: {
				type: 'datetime',
				labels:{
					formatter: function(){              
						var dat = new Date(this.value);
						return dat.toLocaleDateString('pt-BR');             
					},
					style: {color:'#fff'}           
				},
				crosshair: true,
				lineColor: '#fff',
				tickColor: '#fff',
				labelColor: '#fff',
				tickPixelInterval:50,
				showFirstLabel: true
			},
			series: [
			{
				name: 'ICC',
				data: {!! $j_hc_indice_conv !!},
				color: graph1,
				lineWidth:1             
			},
			],

			responsive: {
				rules: [{
					condition: {
						maxWidth: 500
					},
					chartOptions: {
						legend: {
							layout: 'horizontal',
							align: 'center',
							verticalAlign: 'bottom'
						}
					}
				}]
			}

		});
	}

	function render_hc_eq_media_volat(){

		Highcharts.chart('hc_eq_media_volat', {
			chart:{
				backgroundColor: '#000000'
			},
			title:{
				text: ""
			},  
			dateFormat: "%d",
			tooltip:{
				xDateFormat: '%d-%m-%Y',
				shared: true        
			},
			yAxis: {
				title: {
					text: ''
				},
				labels:{
					style:{color:'#fff'},
					formatter: function(){
						return this.value + "%";
					}
				},
				crosshair: true,
				gridLineWidth:0,
				tickAmount: 10,

				plotLines: [
				{ 
					color: 'red',
					width: 2,
					value: 0,
					dashStyle: 'longdashdot',
					label:{
						text:'0%',
						style:{color:"red"}
					}
				},
				],        
			},
			legend: {
				enabled: false
			},

			xAxis: {
				type: 'datetime',
				labels:{
					formatter: function(){              
						var dat = new Date(this.value);
						return dat.toLocaleDateString('pt-BR');             
					},
					style: {color:'#fff'}           
				},
				crosshair: true,
				lineColor: '#fff',
				tickColor: '#fff',
				labelColor: '#fff',
				tickPixelInterval:50,
				showFirstLabel: true
			},
			series: [
			{
				name: 'EFV',
				data: {!! $j_hc_eq_media_volat !!},
				color: graph1,
				lineWidth:1             
			},
			],

			responsive: {
				rules: [{
					condition: {
						maxWidth: 500
					},
					chartOptions: {
						legend: {
							layout: 'horizontal',
							align: 'center',
							verticalAlign: 'bottom'
						}
					}
				}]
			}

		});
	}
</script>
@stop
