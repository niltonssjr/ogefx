@extends('adminlte::page')

@section('title', 'Carregar cotações')

@section('content')
<div class="row">
	<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
		<span class='content-box-title'>Importação de cotações - {{ env('APP_NAME') }}</span>
		<div class='content-box'>	
			@include('includes.alerts')	
			<form action="{{ route('quotes.loadsinglefile')}}" method='post' enctype="multipart/form-data">
				{!! csrf_field(); !!}
				<label for="quotesFile">Arquivo de cotações em formato ZIP</label>
				<input type="file" class="form-control" name="quotesFile"><br/>
				<div>
					<button class="btn btn-primary">Importar</button>
					<button type="button" class="btn btn-danger" id="quote_clean_database">Reiniciar base de cotações</button>
				</div>			
			</form>
		</div>		
	</div>	
</div>
<br/>
<div class="row">
	<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
		<span class='content-box-title'>Últimos arquivos carregados</span>
		<div class='content-box'>	
			<table class='table table-striped'>
				<thead>
					<tr>						
						<th>Nome</th>
						<th>Tamanho</th>
						<th>Data</th>
					</tr>
				</thead>
				<tbody>
					@foreach($fileData as $row)
					<tr>						
						<td>{{ $row->fileName}}</td>
						<td>{{ $row->fileSize}}</td>
						<td>{{ $row->fileDate}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>		
	</div>	
</div>
@stop

@section('css')
<link href="{{ url('/')}}/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link rel="stylesheet" href='{{ url("/") }}/css/ogefx.css'>
@stop

@section('js')
<script src='{{ url("/") }}/js/jquery.mask.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.pt-BR.min.js'></script>
<script src='{{ url("/") }}/js/auge_masks.js'></script>
<script src='{{ url("/") }}/js/sweetalert2.all.js'></script>
<script>
	$(document).ready(function(){
		//datepicker for due_date
		$('.as_select2').select2();
		$(".as_datepicker").datepicker({
			format: "dd/mm/yyyy",
			todayBtn: true,
			autoclose: true,
			language: "pt-BR",
			startDate: "1d"
		});

		$("#quote_clean_database").click(function(){
			restart_quote_database();
		});
	});

	function restart_quote_database(){

		swal({
			title: 'Confirma a limpeza da base de cotações?',
			text: "Esta operação não pode ser revertida!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim, limpe o banco de dados'
		}).then((result) => {
			if (result.value) {
				var url = "{{ url ('/') }}" + "/admin/cotacoes/reiniciar";
				$.get(url,function(data){
					var retorno = JSON.parse(data);
					console.log(retorno);
					if (retorno.success){
						swal('Apagado!','Sua base de cotações foi reiniciada.','success');
					}
				});
			}
		})
	}

</script>
@stop



