@extends('adminlte::page')

@section('title', 'Cotações')

@section('content')    
<span class='content-box-title'>Cotações - Índice {{ $period }} - {{ env('APP_NAME') }} </span>
<div class='content-box'>	
	@include('includes.alerts')
	<div class="row" style="text-align:right">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<button class="btn btn-primary" id="load_single_file">Atualizar cotações</button>
		</div>		
	</div>
	<br/>
	@include('includes.tabela')
</div>
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.css"/>
<link rel="stylesheet" href='{{ url("/") }}/css/ogefx.css'>
@stop

@section('js')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
<script type="text/javascript" src="{{ url('/')}}/js/sweetalert2.all.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
		$('#standard_table').DataTable({
			"responsive" :true,
			"language": {
				"lengthMenu": "Mostrando  _MENU_  cotações por página",
				"emptyTable": "Nenhuma cotação encontrada",
				"search":         "Pesquisar: ",
				"zeroRecords": "Nenhuma cotação encontrada",
				"info": "Mostrando página  _PAGE_  de  _PAGES_",
				"infoEmpty": "Nenhuma cotação encontrada",
				"infoFiltered": "(filtrado de _MAX_ cotações)",
				"paginate": {
					"first":      "Primeira",
					"last":       "Última",
					"next":       "Próxima",
					"previous":   "Anterior"
				},
				"aria": {
					"sortAscending":  ": ative para a ordenação crescente",
					"sortDescending": ": ative para a ordenação decrescente"
				}					    
			}    			
		});

		$("#load_single_file").click(function(){
			load_single_file();
		});

	} );	

	function load_single_file(){

		url = "{{ url('/admin/cotacoes/carregar')}}";

		$.get(url,function(quote_return){
			console.log(quote_return);
			return false;
		});

	}

</script>
@stop
