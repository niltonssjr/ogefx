@extends('adminlte::page')

@section('title', 'Meu perfil')

@section('content')
<div class="row">
	<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
		<span class='content-box-title'>Meu perfil - {{ env('APP_NAME') }}</span>
		<div class='content-box'>	
		@include('includes.alerts')	
			<form action="{{ route('myprofile.update')}}" method='post'>
				{!! csrf_field(); !!}
				<input type="hidden" name="user_id" value="{{ old('user_id',$user_form->id)}}">
				<div class=''>
					<div class="form-group">
						<label for="user_name">Nome:</label>
						<input type="text" class='form-control' name="user_name" placeholder="Nome do usuário" value="{{ old('user_name',$user_form->name) }}">	
					</div>							
				</div>
				<div class=''>
					<div class="form-group">
						<label for="user_email">E-mail:</label>
						<input type="email" class='form-control' name="user_email" placeholder="Email do usuário" value="{{ old('user_email',$user_form->email) }}">	
					</div>					
				</div>
				<div>
					<button class="btn btn-primary">Atualizar</button>
				</div>			
			</form>
		</div>		
	</div>	
</div>

@stop

@section('css')
<link href="{{ url('/')}}/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link rel="stylesheet" href='{{ url("/") }}/css/ogefx.css'>
@stop

@section('js')
<script src='{{ url("/") }}/js/jquery.mask.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.pt-BR.min.js'></script>
<script src='{{ url("/") }}/js/auge_masks.js'></script>
<script src='{{ url("/") }}/js/sweetalert2.all.js'></script>
<script>
	$(document).ready(function(){
		//datepicker for due_date
		$('.as_select2').select2();
		$(".as_datepicker").datepicker({
			format: "dd/mm/yyyy",
			todayBtn: true,
			autoclose: true,
			language: "pt-BR",
			startDate: "1d"
		});
	});
</script>
@stop



