@extends('adminlte::page')

@section('title', 'Pares de moedas analisados')

@section('content')   
<span class='content-box-title'>Cadastramento de novos ativos - {{ env('APP_NAME') }}</span>
<div class="content-box currency_adm">
	@include('includes.alerts')
	<form method='post' action="{{route('currencies.post')}}">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
				<div class="form-group">
					<label>Código</label>
					<input type="text" class='form-control' name="currency_name" value="{{ old('currency_name')}}">
				</div>
			</div>	
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
				<div class="form-group">
					<button type='submit' class='btn btn-primary'>Cadastrar</button>				
				</div>
			</div>				
		</div>
	</form>
</div>
<div class='content-box'>	
	<div class="row">
		@foreach($currencies as $currency)		
		<div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
			<div class='currency_label'>
			<button class='btn btn-small btn-danger' onclick='removal_confirmation({{$currency->id}})'><i class='fa fa-trash'></i> </button>
			{{$currency->name}}
			</div>
		</div>
		@endforeach
	</div>
</div>
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.css"/>
<link rel="stylesheet" href='{{ url("/") }}/css/ogefx.css'>
@stop

@section('js')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
<script type="text/javascript" src="{{ url('/')}}/js/sweetalert2.all.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
		$('#standard_table').DataTable({
			"responsive" :true,
			"language": {
				"lengthMenu": "Mostrando  _MENU_  pares por página",
				"emptyTable": "Nenhum par de moedas encontrado",
				"search":         "Pesquisar: ",
				"zeroRecords": "Nenhum par de moedas encontrado",
				"info": "Mostrando página  _PAGE_  de  _PAGES_",
				"infoEmpty": "Nenhum par de moedas encontrado",
				"infoFiltered": "(filtrado de _MAX_ pares de moedas)",
				"paginate": {
					"first":      "Primeira",
					"last":       "Última",
					"next":       "Próxima",
					"previous":   "Anterior"
				},
				"aria": {
					"sortAscending":  ": ative para a ordenação crescente",
					"sortDescending": ": ative para a ordenação decrescente"
				}					    
			}    			
		});
	} );

	function removal_confirmation(id){
		swal({
			title: 'Confirma a exclusão do ativo?',
			text: "Esta ação não pode ser revertida, e as cotações referentes serão excluídas!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim, remova o ativo!'
		}).then((result) => {
			if (result.value) {
				target_route = "{{ route('currencies.delete',['id'=>'##@@##'])}}";					
				target_url = target_route.replace('##@@##',id);				
				window.location.replace(target_url);				
			}
		})
	}

</script>
@stop
