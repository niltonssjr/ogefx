@extends('adminlte::page')

@section('title', 'Configurações do sistema')

@section('content')
<div class="row">
	<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
		<span class='content-box-title'>Configurações do sistema</span>
		<div class='content-box'>	
			@include('includes.alerts')			
			<form action="{{ route('systemsettings.update')}}" method='post'>
				{!! csrf_field(); !!}	
				<span class="content-box-subtitle">E-mail para a busca de leads</span>			
				<div class='row'>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="form-group">
							<label for="email_server">Servidor de e-mail:</label>
							<input type="text" class='form-control' name="email_server" placeholder="Servidor de e-mail" value="{{ old('email_server',$config->email_server) }}">	
						</div>							
					</div>					
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="form-group">
							<label for="email_account">Email:</label>
							<input type="email" class='form-control' name="email_account" placeholder="E-mail" value="{{ old('email_account',$config->email_account) }}">	
						</div>							
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="form-group">
							<label for="email_password">Senha:</label>
							<input type="password" class='form-control' name="email_password" placeholder="Senha do e-mail" value="{{ old('email_password',$config->email_password) }}">
						</div>							
					</div>					
				</div>
				<span class="content-box-subtitle">Período de testes</span>			
				<div class='row'>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="form-group">
							<label for="trial_days">Dias para teste:</label>
							<input type="text" class='form-control as_mask_days' name="trial_days" value="{{ old('trial_days',$config->trial_days) }}">	
							<small>Válido para novos leads</small>
						</div>							
					</div>	
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="form-group">
							<label for="trial_users">Usuários Trial no momento:</label>
							<input type="text" class='form-control' name="trial_users" value="{{ old('trial_users',$config->trial_users) }}" disabled>	
							<small>Usuários testando a aplicação no momento.</small>
						</div>							
					</div>														
				</div>	
				<span class="content-box-subtitle">Força das moedas - Entradas</span>			
				<div class='row'>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="form-group">
							<label for="trial_days">Limite percentual inferior:</label>
							<input type="text" class='form-control as_mask_phone_prefix' name="currency_power_low" value="{{ old('currency_power_low',$config->currency_power_low) }}">	
							<small>Moedas abaixo deste percentual serão demonstradas em vermelho na tela</small>
						</div>							
					</div>	
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="form-group">
							<label for="trial_days">Limite percentual superior:</label>
							<input type="text" class='form-control as_mask_phone_prefix' name="currency_power_high" value="{{ old('currency_power_high',$config->currency_power_high) }}">	
							<small>Moedas acima deste percentual serão demonstradas em verde na tela</small>
						</div>							
					</div>														
				</div>		
				<span class="content-box-subtitle">Servidor de entrada de dados</span>			
				<div class='row'>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="form-group">
							<label for="servidor_dados_ip">Endereço IP:</label>
							<input type="text" class='form-control' name="servidor_dados_ip" value="{{ old('servidor_dados_ip',$config->servidor_dados_ip) }}">	
							<small>Informe o endereço do servidor com o http</small>
						</div>							
					</div>	
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="form-group">
							<label for="servidor_dados_porta">Porta:</label>
							<input type="text" class='form-control' name="servidor_dados_porta" value="{{ old('servidor_dados_porta',$config->servidor_dados_porta) }}">	
							<small>Informe a porta</small>
						</div>							
					</div>													
				</div>		
				<span class="content-box-subtitle">Referência cruzada - percentual x pontos - Limites inferiores</span>
				<div class='row'>
					<div class="col-xs-1">
						<div class="form-group">
							<label for="forca_percent_pontos_1" class='forca_percent_pontos_label'>1</label>
							<input type="text" class='form-control as_mask_phone_prefix right' name="forca_percent_pontos_1" value="{{ old('forca_percent_pontos_1',$config->forca_percent_pontos_1) }}">
						</div>							
					</div>
					<div class="col-xs-1">
						<div class="form-group">
							<label for="forca_percent_pontos_2" class='forca_percent_pontos_label'>2</label>
							<input type="text" class='form-control as_mask_phone_prefix right' name="forca_percent_pontos_2" value="{{ old('forca_percent_pontos_2',$config->forca_percent_pontos_2) }}">
						</div>						
					</div>					

					<div class="col-xs-1">
						<div class="form-group">
							<label for="forca_percent_pontos_3" class='forca_percent_pontos_label'>3</label>
							<input type="text" class='form-control as_mask_phone_prefix right' name="forca_percent_pontos_3" value="{{ old('forca_percent_pontos_3',$config->forca_percent_pontos_3) }}">
						</div>						
					</div>	
					<div class="col-xs-1">
						<div class="form-group">
							<label for="forca_percent_pontos_4" class='forca_percent_pontos_label'>4</label>
							<input type="text" class='form-control as_mask_phone_prefix right' name="forca_percent_pontos_4" value="{{ old('forca_percent_pontos_4',$config->forca_percent_pontos_4) }}">
						</div>						
					</div>	
					<div class="col-xs-1">
						<div class="form-group">
							<label for="forca_percent_pontos_5" class='forca_percent_pontos_label'>5</label>
							<input type="text" class='form-control as_mask_phone_prefix right' name="forca_percent_pontos_5" value="{{ old('forca_percent_pontos_5',$config->forca_percent_pontos_5) }}">
						</div>						
					</div>	
					<div class="col-xs-1">
						<div class="form-group">
							<label for="forca_percent_pontos_6" class='forca_percent_pontos_label'>6</label>
							<input type="text" class='form-control as_mask_phone_prefix right' name="forca_percent_pontos_6" value="{{ old('forca_percent_pontos_6',$config->forca_percent_pontos_6) }}">
						</div>						
					</div>	
					<div class="col-xs-1">
						<div class="form-group">
							<label for="forca_percent_pontos_7" class='forca_percent_pontos_label'>7</label>
							<input type="text" class='form-control as_mask_phone_prefix right' name="forca_percent_pontos_7" value="{{ old('forca_percent_pontos_7',$config->forca_percent_pontos_7) }}">
						</div>						
					</div>	
					<div class="col-xs-1">
						<div class="form-group">
							<label for="forca_percent_pontos_8" class='forca_percent_pontos_label'>8</label>
							<input type="text" class='form-control as_mask_phone_prefix right' name="forca_percent_pontos_8" value="{{ old('forca_percent_pontos_8',$config->forca_percent_pontos_8) }}">
						</div>						
					</div>	
					<div class="col-xs-1">
						<div class="form-group">
							<label for="forca_percent_pontos_9" class='forca_percent_pontos_label'>9</label>
							<input type="text" class='form-control as_mask_phone_prefix right' name="forca_percent_pontos_9" value="{{ old('forca_percent_pontos_9',$config->forca_percent_pontos_9) }}">
						</div>						
					</div>																																								
				</div>		
				<span class="content-box-subtitle">Valores para buy / sell relativos aos pontos</span>
				<div class="row">
					<div class="col-xs-1">
						<span class="center_block currency_power_limit_high forca_buy_sell_config">Buy</span>
					</div>
					<div class="col-xs-1">
						<span class="center_block forca_buy_sell_config">Buy >=</span>
					</div>			
					<div class="col-xs-1">
						<input type="text" class='form-control as_mask_bank_digit right' name="forca_buy_sell_high_buy" value="{{ old('forca_buy_sell_high_buy',$config->forca_buy_sell_high_buy) }}">
					</div>		
					<div class="col-xs-1">
						<span class="center_block forca_buy_sell_config">e Sell <=</span>
					</div>			
					<div class="col-xs-1">
						<input type="text" class='form-control as_mask_bank_digit right' name="forca_buy_sell_high_sell" value="{{ old('forca_buy_sell_high_sell',$config->forca_buy_sell_high_sell) }}">
					</div>											
				</div>			
				<hr>
				<div class="row">
					<div class="col-xs-1">
						<span class="center_block currency_power_limit_low forca_buy_sell_config">Sell</span>
					</div>
					<div class="col-xs-1">
						<span class="center_block forca_buy_sell_config">Sell >=</span>
					</div>			
					<div class="col-xs-1">
						<input type="text" class='form-control as_mask_bank_digit right' name="forca_buy_sell_low_sell" value="{{ old('forca_buy_sell_low_sell',$config->forca_buy_sell_low_sell) }}">
					</div>	
					<div class="col-xs-1">
						<span class="center_block forca_buy_sell_config">e Buy <=</span>
					</div>		
					<div class="col-xs-1">
						<input type="text" class='form-control as_mask_bank_digit right' name="forca_buy_sell_low_buy" value="{{ old('forca_buy_sell_low_buy',$config->forca_buy_sell_low_buy) }}">
					</div>																		
				</div>	



				<span class="content-box-subtitle">Limites inferiores para qualificar pontos relativos às forças</span>
				<div class="row">
					<div class="col-xs-3">
						<label class='center_block currency_power_limit_high'> A partir de </label>
						<input type="text" class='form-control as_mask_bank_digit right' name="forca_pontos_indicador_green" value="{{ old('forca_pontos_indicador_green',$config->forca_pontos_indicador_green) }}">
					</div>
					<div class="col-xs-3">
						<label class='center_block currency_power_limit_yellow'> A partir de </label>
						<input type="text" class='form-control as_mask_bank_digit right' name="forca_pontos_indicador_yellow" value="{{ old('forca_pontos_indicador_yellow',$config->forca_pontos_indicador_yellow) }}">
					</div>
					<div class="col-xs-3">
						<label class='center_block currency_power_limit_orange'> A partir de </label>
						<input type="text" class='form-control as_mask_bank_digit right' name="forca_pontos_indicador_orange" value="{{ old('forca_pontos_indicador_orange',$config->forca_pontos_indicador_orange) }}">
					</div>										
					<div class="col-xs-3">
						<label class='center_block currency_power_limit_low'> Outros </label>
						<input type="text" class='form-control as_mask_bank_digit right' name="forca_pontos_indicador_red" value="{{ old('forca_pontos_indicador_red',$config->forca_pontos_indicador_red) }}">
					</div>						
				</div>

				<div class='row mt-15'>	
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 right">
						<button class="btn btn-primary">Atualizar</button>
					</div>				
				</div>		

			</form>
		</div>		
	</div>	
</div>

@stop

@section('css')
<link href="{{ url('/')}}/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link rel="stylesheet" href='{{ url("/") }}/css/ogefx.css?v=202004141719'>
@stop

@section('js')
<script src='{{ url("/") }}/js/jquery.mask.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.pt-BR.min.js'></script>
<script src='{{ url("/") }}/js/auge_masks.js'></script>
<script src='{{ url("/") }}/js/sweetalert2.all.js'></script>
<script>
	$(document).ready(function(){
		//datepicker for due_date
		$('.as_select2').select2();
		$(".as_datepicker").datepicker({
			format: "dd/mm/yyyy",
			todayBtn: true,
			autoclose: true,
			language: "pt-BR",
			startDate: "1d"
		});
	});
</script>
@stop



