<!DOCTYPE html>
<html>
<head>
	<title>Bem-vindo ao Trader System!</title>
</head>
<style type="text/css">
	*{
		font-family: Helvetica;
		line-height:25px;
	}
</style>
<body>

	<div style="width:80%;margin-left:10%;text-align:center;">
		<img src="https://tradersystemctl.ogefx.com.br/img/logo_colorido_500.png" style='max-width:300px;'/>
		<h2>Bem-vindo ao Trader System CTL, {{$user['name']}}.</h2>
		<br/>
		Seu e-mail para acesso é o {{$user['email']}}.
		<br/>Caso você tenha se cadastrado, use a senha informada para o acesso. 
		<br/>Caso tenha sido cadastrado por outra pessoa,<br/> sua senha é igual à primeira parte do seu e-mail (aquela antes do @).
		<br/>Lembre-se de trocar a sua senha no primeiro acesso, caso esteja utilizando a senha padrão.
		<br/>Abraço, e espero que aproveite bem o Trader System CTL!	
		<br/>
		<a style="display:inline-block;padding:10px 30px;background-color:#3c8dbc;border-radius:10px;margin-top:20px;color:white;text-decoration:none;" href="{{ url('/')}}">Acessar o Trader System CTL</a>
	</div>

</body>

</html>