@extends('adminlte::page')

@section('title', 'Meu perfil')

@section('content')
<div class="row">
	<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
		<span class='content-box-title'>Alteração de senha - {{ env('APP_NAME') }}</span>
		<div class='content-box'>	
		@include('includes.alerts')	
			<form method='post' action="{{ route('myaccess.update') }}">
				{!! csrf_field(); !!}
				<div class="form-group">
					<label for='pw_password'>Nova senha:</label>
					<input type="password" name="pw_password" class='form-control' value="{{ old('pw_password')}}">
				</div>
				<div class="form-group">
					<label for='pw_password_confirmation'>Confirme a nova senha:</label>
					<input type="password" name="pw_password_confirmation" class='form-control' value="{{ old('pw_password_confirmation')}}">
				</div>				
				<input type="submit" class='btn btn-primary' value="Atualizar">
			</form>	
		</div>		
	</div>	
</div>

@stop

@section('css')
<link href="{{ url('/')}}/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link rel="stylesheet" href='{{ url("/") }}/css/ogefx.css'>
@stop

@section('js')
<script src='{{ url("/") }}/js/jquery.mask.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.pt-BR.min.js'></script>
<script src='{{ url("/") }}/js/auge_masks.js'></script>
<script src='{{ url("/") }}/js/sweetalert2.all.js'></script>
<script>
	$(document).ready(function(){
		//datepicker for due_date
		$('.as_select2').select2();
		$(".as_datepicker").datepicker({
			format: "dd/mm/yyyy",
			todayBtn: true,
			autoclose: true,
			language: "pt-BR",
			startDate: "1d"
		});
	});
</script>
@stop



