<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RetornoAndRvOnQuotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotes',function(Blueprint $table){
            $table->double('rv',16,6)->default(0);
            $table->double('retorno',16,6)->default(0);
            $table->double('close_md_exp_6',16,6)->default(0);
            $table->double('close_md_exp_12',16,6)->default(0);
            $table->double('close_md_exp_24',16,6)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
