<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NovosIndicesOnQuotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("quotes",function(Blueprint $table){
            $table->double('distancia_media',16,6)->default(0);
            $table->double('relacao_volume',16,6)->default(0);
            $table->double('relacao_volume_md150',16,6)->default(0);
            $table->double('relacao_volume_sdev150',16,6)->default(0);
            $table->double('diferenca_media',16,6)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
