<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
// use DB;

class EnlargeNumberFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "alter table quotes
        modify column ganho double(16,6), 
        modify column perda double(16,6),
        modify column mg double(16,6),
        modify column mp double(16,6),
        modify column rs_inv double(16,6),
        modify column rsi_14_inv double(16,6),
        modify column rsi_normal double(16,6),
        modify column rs_normal double(16,6),
        modify column pt double(16,6),
        modify column usd double(16,6),
        modify column dsd double(16,6),
        modify column u double(16,6),
        modify column d double(16,6),
        modify column rvi double(16,6),
        modify column mms_ptcci_20 double(16,6),
        modify column true_range double(16,6),
        modify column atr_d1 double(16,6),
        modify column mean_devation double(16,6),
        modify column ciclo_cci_20 double(16,6),
        modify column mm21 double(16,6),
        modify column ln double(16,6),
        modify column desv_p double(16,6),
        modify column f_afast double(16,6),
        modify column mvh double(16,6),
        modify column emv double(16,6),
        modify column media_14_emv double(16,6),
        modify column media_exp_5_rsi double(16,6),
        modify column media_exp_12_rsi double(16,6),
        modify column razao_gp_inv double(16,6),
        modify column razao_pg double(16,6),
        modify column razao_rsi_rvi double(16,6),
        modify column razao_rvi_rsi_inv double(16,6)
        ";
        DB::statement($sql);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
