<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LastRviRsiQuotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotes',function(Blueprint $table){
            $table->date('last_rsi_bs_ref_date')->nullable();
            $table->float('last_rsi_bs_rsi_normal',5,2)->default(0);
            $table->float('last_rsi_bs_close',16,6)->default(0);
            $table->date('last_rvi_bs_ref_date')->nullable();
            $table->float('last_rvi_bs_rvi',5,2)->default(0);
            $table->float('last_rvi_bs_close',16,6)->default(0);     
            $table->float('m_vol_5',16,6)->default(0);
            $table->float('m_vol_20',16,6)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotes',function(Blueprint $table){
            $table->dropColumn(['last_rsi_bs_ref_date',
                                'last_rsi_bs_rsi_normal',
                                'last_rsi_bs_close',
                                'last_rvi_bs_ref_date',
                                'last_rvi_bs_rvi',
                                'last_rvi_bs_close',
                                'm_vol_5',
                                'm_vol_20'
                    ]);
        });
    }
}
