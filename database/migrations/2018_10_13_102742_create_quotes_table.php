<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('period');
            $table->string('pair');
            $table->date('ref_date');
            $table->double('open',14,6)->default(0);
            $table->double('close',14,6)->default(0);
            $table->double('minimum',14,6)->default(0);
            $table->double('maximum',14,6)->default(0);
            $table->bigInteger('volume')->default(0);
            $table->timestamps('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
}
