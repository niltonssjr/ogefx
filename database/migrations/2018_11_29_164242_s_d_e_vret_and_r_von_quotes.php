<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SDEVretAndRVonQuotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotes',function(Blueprint $table){
            $table->double('afast_ret',16,6)->default(0);
            $table->double('retorno_md150',16,6)->default(0);
            $table->double('rv_md150',16,6)->default(0);
            $table->double('retorno_sdev150',16,6)->default(0);
            $table->double('rv_sdev150',16,6)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
