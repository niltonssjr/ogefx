<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IndicatorsOnQuotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotes', function (Blueprint $table) {
            $table->double('volume_milimiza')->default(0);
            $table->double('ganho',14,6)->default(0);
            $table->double('perda',14,6)->default(0);
            $table->double('mg',14,6)->default(0);
            $table->double('mp',14,6)->default(0);
            $table->double('rs_inv',14,6)->default(0);
            $table->double('rsi_14_inv',14,6)->default(0);
            $table->double('rsi_normal',14,6)->default(0);
            $table->double('rs_normal',14,6)->default(0);
            $table->double('pt',14,6)->default(0);
            $table->double('usd',14,6)->default(0);
            $table->double('dsd',14,6)->default(0);
            $table->double('u',14,6)->default(0);
            $table->double('d',14,6)->default(0);
            $table->double('rvi',14,6)->default(0);
            $table->double('mms_ptcci_20',14,6)->default(0);
            $table->double('true_range',14,6)->default(0);
            $table->double('atr_d1',14,6)->default(0);
            $table->double('mean_devation',14,6)->default(0);
            $table->double('ciclo_cci_20',14,6)->default(0);
            $table->double('mm21',14,6)->default(0);
            $table->double('ln',14,6)->default(0);
            $table->double('desv_p',14,6)->default(0);
            $table->double('f_afast',14,6)->default(0);
            $table->double('mvh',14,6)->default(0);
            $table->double('emv',14,6)->default(0);
            $table->double('media_14_emv',14,6)->default(0);
            $table->double('media_exp_5_rsi',14,6)->default(0);
            $table->double('media_exp_12_rsi',14,6)->default(0);
            $table->double('razao_gp_inv',14,6)->default(0);            
            $table->double('razao_pg',14,6)->default(0);
            $table->double('razao_rsi_rvi',14,6)->default(0);
            $table->double('razao_rvi_rsi_inv',14,6)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotes', function (Blueprint $table) {
            $table->dropColumn([
                    'volume_milimiza',
                    'ganho',
                    'perda',
                    'mg',
                    'mp',
                    'rs_inv',
                    'rsi_14_inv',
                    'rsi_normal',
                    'rs_normal',
                    'pt',
                    'usd',
                    'dsd',
                    'u',
                    'd',
                    'rvi',
                    'mms_ptcci_20',
                    'true_range',
                    'atr_d1',
                    'mean_devation',
                    'ciclo_cci_20',
                    'mm21',
                    'ln',
                    'desv_p',
                    'f_afast',
                    'mhv',
                    'emv',
                    'media_14_emv',
                    'media_exp_5_rsi',
                    'media_exp_12_rsi',
                    'razao_gp_inv',
                    'razao_pg',
                    'razao_rsi_rvi',
                    'razao_rvi_rsi_inv'
                ]);
        });
    }
}
