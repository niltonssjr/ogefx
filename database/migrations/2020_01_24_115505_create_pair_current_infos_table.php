<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePairCurrentInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pair_current_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pair')->nullable();
            $table->date('ref_date')->nullable();
            $table->integer('ind_15_cruzamento_media')->default(0);
            $table->integer('ind_30_cruzamento_media')->default(0);
            $table->integer('ind_60_cruzamento_media')->default(0);
            $table->integer('ind_240_cruzamento_media')->default(0);
            $table->integer('ind_1440_cruzamento_media')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pair_current_infos');
    }
}
