<?php

use Illuminate\Database\Seeder;
use App\Models\Currency;
use App\Models\Quote;

class CurrenciesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $quotes = Quote::select('pair')->distinct()->get();

        foreach($quotes as $quote){
            Currency::create(['name'=>$quote->pair]);
        }

    }
}
