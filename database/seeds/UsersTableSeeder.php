<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
    		'name'		=> 'OGEFX',
    		'email'		=> 'ogefx@gmail.com',
    		'password'	=> bcrypt('osmarogefx'),
    		'level'		=> 'admin',
    		'ativo'	=> true            
    	]); 
        User::create([
    		'name'		=> 'Augesystems',
    		'email'		=> 'augesystems@gmail.com',
    		'password'	=> bcrypt('auge@ogefx'),
    		'level'		=> 'admin',
    		'ativo'	=> true            
    	]);     	
        User::create([
    		'name'		=> 'Nilton Júnior',
    		'email'		=> 'niltonssjr@gmail.com',
    		'password'	=> bcrypt('auge@ogefx'),
    		'level'		=> 'user',
    		'ativo'	=> true            
    	]);     	    	

    }
}
