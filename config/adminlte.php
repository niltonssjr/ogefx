<?php
$cor_padrao = 'blue';
if (env('APP_ENV')=='staging') $cor_padrao = 'red';
return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'OGEFX',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<img src="/img/logo_small.png" style="max-width:60%;">',
    // 'logo' => '<img src="'. url('/') .'/img/logo_small.png" style="max-width:60%;">',
    // 'logo' => '<img src="'. config('app.url') .'/img/logo_small.png" style="max-width:60%;">',
    // 'logo' => '<b>Bzinga</b>',

    'logo_mini' => '<b>OGE</b>',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => $cor_padrao,

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => "top-nav",

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'home',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => null,

    

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */

    'menu' => [
        'ANÁLISE',
        [
            // 'text' => $url,            
            'text' => 'Análise',            
            'url' => '',
            'icon' => 'money',
            'icon_color' => 'aqua',            
            'submenu' => [
                [
                    'text' => 'Painel',
                    'url' => 'admin/analise-tecnica',
                    'icon' => 'money',
                    'icon_color' => 'aqua',                     
                ],   
                [
                    'text' => 'Meu portfolio',
                    'url' => 'admin/portfolio',
                    'icon' => 'money',
                    'icon_color' => 'aqua',                    
                ],                              
            ]
        ],              
        'PARES DE MOEDAS',
        [
            'text' => 'Cotações',
            'url' => '',
            'icon' => 'money',
            'icon_color' => 'aqua',
            'can' => 'isAdmin',
            'submenu' =>[
                [
                    'text' => 'Moedas',            
                    'url' => 'admin/moedas',
                    'icon' => 'money',
                    'icon_color' => 'aqua',
                    'can' => 'isAdmin',
                ],   
                [
                    'text' => 'Histórico',
                    'url' => 'admin/cotacoes/historico',
                    'icon' => 'money',
                    'icon_color' => 'aqua',
                    'can' => 'isAdmin',
                ],    
                 [
                    'text' => 'Índices',
                    'url' => 'admin/indicadores',
                    'icon' => 'money',
                    'icon_color' => 'aqua',
                    'can' => 'isAdmin',
                ],
                [
                    'text' => 'Importação',
                    'url' => 'admin/cotacoes/carregar',
                    'icon' => 'archive',
                    'icon_color' => 'aqua',
                    'can' => 'isAdmin',
                ],                                                      
            ],
        ],      
        [
            'text'        => 'Usuários',
            'url'         => 'admin/dashboard',
            'icon'        => 'user',
            'icon_color'  => 'aqua',
            'can' => 'isAdmin',
            'submenu'     => [
                [
                    'text' => 'Cadastrar usuário',
                    'url'  => 'admin/usuarios/incluir',
                    'icon' => 'plus',
                    'icon_color'  => 'aqua',
                ],
                [
                    'text' => 'Todos os usuários',
                    'url'  => 'admin/usuarios/listar',
                    'icon' => 'list',
                    'icon_color'  => 'aqua',

                ]
            ],
        ], 
        [
            'text'        => 'Configurações',
            'url'         => 'admin/configuracoes',
            'icon'        => 'cog',
            'icon_color'  => 'aqua',
            'can' => 'isAdmin'
        ],                             
        'CONFIGURAÇÕES DE CONTA',
        [
            'text' => 'Minha conta',            
            'url' => '#',
            'icon' => 'info',
            'icon_color' => 'aqua',
            'submenu' =>[
                [
                    'text' => 'Meu perfil',
                    'url'  => 'admin/meuperfil',
                    'icon' => 'info',
                    'icon_color'  => 'aqua',
                ],
                [
                    'text' => 'Alterar senha',
                    'url'  => 'admin/meuacesso',
                    'icon' => 'lock',
                    'icon_color'  => 'aqua',
                ],                    
            ]
        ],         

    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => true,
        'select2'    => true,
        'chartjs'    => true,
    ],
];
