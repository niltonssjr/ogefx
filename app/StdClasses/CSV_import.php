<?php
namespace App\StdClasses;	
	//Import CSV file class
	//Author: Nilton Junior (augesystems@gmail.com)
	//The class receives the file name and retrieves a line or the entire file

	class CSV_import{

		//raw file name
		protected $file_name;

		//column separator
		protected $separator = ",";

		//end of file indicator
		public $eof = false;

		//file control variable
		protected $file_control;

		//indicates if the file is already openend
		protected $opened = false;

		//file headers
		public $headers = [];

		//columns retrieved from a single line
		public $columns = [];

		//array with the complete data
		public $data = [];

		//errors
		public $erros = [];

		//Amount of lines read
		public $read_records = 0;

		//Receives the filename
		public function getFileName($file){
			$this->file_name = $file;
		}

		//Receives the column separator
		public function getSeparator($sep){
			$this->separator = $sep;
		}

		//retrieves the next record
		public function next_record(){
			
			$this->read_file();

		}

		//retrieves the entire file with one access
		public function get($file,$sep){

			$this->getFileName($file);
			$this->getSeparator($sep);

			$this->data = array_map("split_csv",file($file));

			return $this->data;

		}

		private function split_csv($data){
			return explode($this->separator,$data);
		}

		//access to the file
		private function read_file(){
			
			if (!$this->opened){
				if (strlen(trim($this->file_name))==0){
					$this->erros[] = "Arquivo a ser importado não informado";
					return;
				}
				$this->file_control = fopen($this->file_name,"r");
				$this->opened = true;
			}

			$fields = fgetcsv($this->file_control,0,$this->separator);									

//			if ($this->read_records ==0){
//				$this->headers = $fields;
//				$fields = fgetcsv($this->file_control,0,$this->separator);						
//			} 

			if (!$this->eof = feof($this->file_control)){
				$this->read_records +=1;
				$this->columns = $fields;
			}
			
		}



	}

?>