<?php 

namespace App\StdClasses;

use Gate;
use Zipper;
use Log;
use Illuminate\Support\Facades\Storage;

class StdFunctions{

	public static function fmt_datetime_dmy($parm_date){

		if (!$parm_date) return "";

		$date = date_create($parm_date);

		$return_date = date_format($date,"d/m/Y");

		return $return_date;

	}

	public static function fmt_datetime_dmyhis($parm_date){

		if (!$parm_date) return "";

		$date = date_create($parm_date);

		$return_date = date_format($date,"d/m/Y H:i:s");

		return $return_date;

	}

	public static function fmt_dateclass_dmy($parm_date){

		if (!$parm_date) return "";

		$date = $parm_date;		

		$return_date = date_format($date,"d/m/Y");

		return $return_date;

	}

	public static function fmt_dmyhis_ymdhis($parm_date){

		if (!$parm_date) return "";

		$date = date_create_from_format('d/m/Y H:i:s',$parm_date);

		$return_date = date_format($date,"Y-m-d H:i:s");

		return $return_date;

	}

	public static function fmt_dmy_ymd($parm_date){

		if (!$parm_date) return "";

		$date = date_create_from_format('d/m/Y',$parm_date);

		$return_date = date_format($date,"Y-m-d");

		return $return_date;

	}

	public static function fmt_float_to_currency($value){

		if (!$value) return "R$ 0,00";

		$currency_value = (float)$value;

		$currency_value = "R$ ".number_format($currency_value,2,",",".");

		return $currency_value;				
	}

	public static function float_to_percentage($value){
		$percent = number_format(round($value * 100,2),2,",",".");
		return $percent . "%";
	}

	public static function percentage($value){

		$percent = number_format($value,2,",",".");
		return $percent . "%";		

	}

	public static function percentage_color($value){

		$percent = number_format($value,2,",",".");
		$formatted = $percent . "%";

		$class="positive_percentage";
		if ($value<0) $class="negative_percentage";
		
		return "<span class='{$class}'>{$formatted}</span>";		

	}

	public static function float_to_percentage_color($value){

		$percent = number_format(round($value * 100,2),2,",",".");
		$formatted = $percent . "%";

		$class="positive_percentage";
		if ($value<0) $class="negative_percentage";
		
		return "<span class='{$class}'>{$formatted}</span>";		

	}

	public static function float_to_percentage_no_signal($value){
		$percent = number_format(round($value * 100,2),2,",",".");
		return $percent;
	}

	public static function float_to_percentage_no_signal_en($value){
		$percent = number_format(round($value * 100,2),2,".","");
		return $percent;
	}

	public static function allow_admin(){

		if (!Gate::allows('isAdmin')) abort(404);

	}

	public static function allow_active(){

		$plano_validade = auth()->user()->plano_validade;

		if ($plano_validade == null || $plano_validade == "") return;

		$today = date_create();
		$plano_validade = str_replace("00:00:00","23:59:59",$plano_validade);
		$date_validade = date_create($plano_validade);
		
		if ($date_validade < $today) abort(429);

	}

	public static function next_business_date($date,$period){

		$period_ext = [
			'd' => "1 weekday",
			'w' => '1 week',
			'm' => '1 month'
		];		

		$next_business_day = date_add($date,date_interval_create_from_date_string($period_ext[$period]));
		return $next_business_day;

	}

	public static function currency_name($currency){

		$currencies = array (
			'AFA' => 'Afegane (1927–2002)',
			'AFN' => 'Afegane afegão',
			'MGA' => 'Ariary malgaxe',
			'ARA' => 'Austral argentino',
			'THB' => 'Baht tailandês',
			'PAB' => 'Balboa panamenha',
			'ETB' => 'Birr etíope',
			'BOB' => 'Boliviano',
			'BOL' => 'Boliviano (1863–1963)',
			'VEF' => 'Bolívar venezuelano',
			'VEB' => 'Bolívar venezuelano (1871–2008)',
			'GHC' => 'Cedi de Gana (1979–2007)',
			'GHS' => 'Cedi ganês',
			'SVC' => 'Colom salvadorenho',
			'CRC' => 'Colón costarriquenho',
			'CSK' => 'Coroa Forte checoslovaca',
			'ISJ' => 'Coroa antiga islandesa',
			'DKK' => 'Coroa dinamarquesa',
			'SKK' => 'Coroa eslovaca',
			'EEK' => 'Coroa estoniana',
			'ISK' => 'Coroa islandesa',
			'NOK' => 'Coroa norueguesa',
			'SEK' => 'Coroa sueca',
			'CZK' => 'Coroa tcheca',
			'BRC' => 'Cruzado brasileiro (1986–1989)',
			'BRN' => 'Cruzado novo brasileiro (1989–1990)',
			'BRZ' => 'Cruzeiro brasileiro (1942–1967)',
			'BRE' => 'Cruzeiro brasileiro (1990–1993)',
			'BRR' => 'Cruzeiro brasileiro (1993–1994)',
			'BRB' => 'Cruzeiro novo brasileiro (1967–1986)',
			'ZMK' => 'Cuacha zambiano (1968–2012)',
			'AOK' => 'Cuanza angolano (1977–1990)',
			'AOR' => 'Cuanza angolano reajustado (1995–1999)',
			'GEK' => 'Cupom Lari georgiano',
			'MDC' => 'Cupon moldávio',
			'NIO' => 'Córdoba nicaraguense',
			'NIC' => 'Córdoba nicaraguense (1988–1991)',
			'GMD' => 'Dalasi gambiano',
			'DZD' => 'Dinar argelino',
			'BHD' => 'Dinar bareinita',
			'YUN' => 'Dinar conversível iugoslavo (1990–1992)',
			'HRD' => 'Dinar croata',
			'BAD' => 'Dinar da Bósnia-Herzegovina (1992–1994)',
			'YUD' => 'Dinar forte iugoslavo (1966–1990)',
			'YDD' => 'Dinar iemenita',
			'IQD' => 'Dinar iraquiano',
			'JOD' => 'Dinar jordaniano',
			'KWD' => 'Dinar kuwaitiano',
			'LYD' => 'Dinar líbio',
			'MKD' => 'Dinar macedônio',
			'MKN' => 'Dinar macedônio (1992–1993)',
			'YUM' => 'Dinar noviy iugoslavo (1994–2002)',
			'YUR' => 'Dinar reformado iugoslavo (1992–1993)',
			'SDD' => 'Dinar sudanês (1992–2007)',
			'RSD' => 'Dinar sérvio',
			'CSD' => 'Dinar sérvio (2002–2006)',
			'TND' => 'Dinar tunisiano',
			'AED' => 'Dirrã dos Emirados Árabes Unidos',
			'MAD' => 'Dirrã marroquino',
			'STD' => 'Dobra de São Tomé e Príncipe',
			'VND' => 'Dong vietnamita',
			'VNN' => 'Dong vietnamita (1978–1985)',
			'GRD' => 'Dracma grego',
			'AMD' => 'Dram armênio',
			'USD' => 'Dólar americano',
			'AUD' => 'Dólar australiano',
			'BSD' => 'Dólar bahamense',
			'BBD' => 'Dólar barbadense',
			'BZD' => 'Dólar belizenho',
			'BMD' => 'Dólar bermudense',
			'BND' => 'Dólar bruneano',
			'CAD' => 'Dólar canadense',
			'KYD' => 'Dólar das Ilhas Caiman',
			'SBD' => 'Dólar das Ilhas Salomão',
			'HKD' => 'Dólar de Hong Kong',
			'TTD' => 'Dólar de Trinidad e Tobago',
			'CNX' => 'Dólar do Banco Popular da China',
			'XCD' => 'Dólar do Caribe Oriental',
			'ZWD' => 'Dólar do Zimbábue (1980–2008)',
			'ZWR' => 'Dólar do Zimbábue (2008)',
			'ZWL' => 'Dólar do Zimbábue (2009)',
			'FJD' => 'Dólar fijiano',
			'GYD' => 'Dólar guianense',
			'JMD' => 'Dólar jamaicano',
			'LRD' => 'Dólar liberiano',
			'NAD' => 'Dólar namibiano',
			'NZD' => 'Dólar neozelandês',
			'USN' => 'Dólar norte-americano (Dia seguinte)',
			'USS' => 'Dólar norte-americano (Mesmo dia)',
			'RHD' => 'Dólar rodesiano',
			'SGD' => 'Dólar singapuriano',
			'SRD' => 'Dólar surinamês',
			'GQE' => 'Ekwele da Guiné Equatorial',
			'CVE' => 'Escudo cabo-verdiano',
			'CLE' => 'Escudo chileno',
			'GWE' => 'Escudo da Guiné Portuguesa',
			'MZE' => 'Escudo de Moçambique',
			'PTE' => 'Escudo português',
			'TPE' => 'Escudo timorense',
			'EUR' => 'Euro',
			'CHE' => 'Euro WIR',
			'AWG' => 'Florim arubano',
			'ANG' => 'Florim das Antilhas Holandesas',
			'SRG' => 'Florim do Suriname',
			'NLG' => 'Florim holandês',
			'HUF' => 'Forint húngaro',
			'XOF' => 'Franco CFA de BCEAO',
			'XAF' => 'Franco CFA de BEAC',
			'XPF' => 'Franco CFP',
			'XFU' => 'Franco UIC francês',
			'CHW' => 'Franco WIR',
			'BEF' => 'Franco belga',
			'BEC' => 'Franco belga (conversível)',
			'BEL' => 'Franco belga (financeiro)',
			'BIF' => 'Franco burundiano',
			'KMF' => 'Franco comorense',
			'CDF' => 'Franco congolês',
			'LUC' => 'Franco conversível de Luxemburgo',
			'MGF' => 'Franco de Madagascar',
			'MLF' => 'Franco de Mali',
			'DJF' => 'Franco djibutiense',
			'LUL' => 'Franco financeiro de Luxemburgo',
			'FRF' => 'Franco francês',
			'GNF' => 'Franco guineano',
			'LUF' => 'Franco luxemburguês',
			'MAF' => 'Franco marroquino',
			'MCF' => 'Franco monegasco',
			'RWF' => 'Franco ruandês',
			'CHF' => 'Franco suíço',
			'XFO' => 'Franco-ouro francês',
			'XRE' => 'Fundos RINET',
			'HTG' => 'Gourde haitiano',
			'PYG' => 'Guarani paraguaio',
			'UAH' => 'Hryvnia ucraniano',
			'KRH' => 'Hwan da Coreia do Sul (1953–1962)',
			'JPY' => 'Iene japonês',
			'PEI' => 'Inti peruano',
			'UAK' => 'Karbovanetz ucraniano',
			'PGK' => 'Kina papuásia',
			'LAK' => 'Kip laosiano',
			'HRK' => 'Kuna croata',
			'MWK' => 'Kwacha malawiana',
			'ZMW' => 'Kwacha zambiano',
			'AOA' => 'Kwanza angolano',
			'BUK' => 'Kyat birmanês',
			'MMK' => 'Kyat mianmarense',
			'GEL' => 'Lari georgiano',
			'LVL' => 'Lats letão',
			'ALK' => 'Lek Albanês (1946–1965)',
			'ALL' => 'Lek albanês',
			'HNL' => 'Lempira hondurenha',
			'SLL' => 'Leone de Serra Leoa',
			'MDL' => 'Leu moldávio',
			'RON' => 'Leu romeno',
			'ROL' => 'Leu romeno (1952–2006)',
			'BGN' => 'Lev búlgaro',
			'BGO' => 'Lev búlgaro (1879–1952)',
			'BGL' => 'Lev forte búlgaro',
			'BGM' => 'Lev socialista búlgaro',
			'GBP' => 'Libra britânica',
			'CYP' => 'Libra cipriota',
			'GIP' => 'Libra de Gibraltar',
			'SHP' => 'Libra de Santa Helena',
			'EGP' => 'Libra egípcia',
			'IEP' => 'Libra irlandesa',
			'ILP' => 'Libra israelita',
			'LBP' => 'Libra libanesa',
			'MTP' => 'Libra maltesa',
			'FKP' => 'Libra malvinense',
			'SDG' => 'Libra sudanesa',
			'SDP' => 'Libra sudanesa (1957–1998)',
			'SSP' => 'Libra sul-sudanesa',
			'SYP' => 'Libra síria',
			'SZL' => 'Lilangeni suazi',
			'ITL' => 'Lira italiana',
			'MTL' => 'Lira maltesa',
			'TRY' => 'Lira turca',
			'TRL' => 'Lira turca (1922–2005)',
			'LTL' => 'Litas lituano',
			'LSL' => 'Loti do Lesoto',
			'MVP' => 'Maldivian Rupee (1947–1981)',
			'AZM' => 'Manat azerbaijano (1993–2006)',
			'AZN' => 'Manat azeri',
			'TMM' => 'Manat do Turcomenistão (1993–2009)',
			'TMT' => 'Manat turcomeno',
			'FIM' => 'Marca finlandesa',
			'DEM' => 'Marco alemão',
			'BAM' => 'Marco bósnio-herzegovino conversível',
			'MZM' => 'Metical de Moçambique (1980–2006)',
			'MZN' => 'Metical moçambicano',
			'BOV' => 'Mvdol boliviano',
			'NGN' => 'Naira nigeriana',
			'ERN' => 'Nakfa da Eritreia',
			'BTN' => 'Ngultrum butanês',
			'AON' => 'Novo cuanza angolano (1990–2000)',
			'BAN' => 'Novo dinar da Bósnia-Herzegovina (1994–1997)',
			'TWD' => 'Novo dólar taiwanês',
			'DDM' => 'Ostmark da Alemanha Oriental',
			'MRO' => 'Ouguiya mauritana',
			'MOP' => 'Pataca macaense',
			'TOP' => 'Paʻanga tonganesa',
			'ADP' => 'Peseta de Andorra',
			'ESP' => 'Peseta espanhola',
			'ESA' => 'Peseta espanhola (conta A)',
			'ESB' => 'Peseta espanhola (conta conversível)',
			'MXP' => 'Peso Prata mexicano (1861–1992)',
			'ARS' => 'Peso argentino',
			'ARM' => 'Peso argentino (1881–1970)',
			'ARP' => 'Peso argentino (1983–1985)',
			'BOP' => 'Peso boliviano',
			'CLP' => 'Peso chileno',
			'COP' => 'Peso colombiano',
			'CUP' => 'Peso cubano',
			'CUC' => 'Peso cubano conversível',
			'GWP' => 'Peso da Guiné-Bissau',
			'DOP' => 'Peso dominicano',
			'PHP' => 'Peso filipino',
			'ARL' => 'Peso lei argentino (1970–1983)',
			'MXN' => 'Peso mexicano',
			'UYU' => 'Peso uruguaio',
			'UYP' => 'Peso uruguaio (1975–1993)',
			'UYI' => 'Peso uruguaio en unidades indexadas',
			'BWP' => 'Pula botsuanesa',
			'GTQ' => 'Quetzal guatemalense',
			'ZAR' => 'Rand sul-africano',
			'ZAL' => 'Rand sul-africano (financeiro)',
			'BRL' => 'Real brasileiro',
			'QAR' => 'Rial catariano',
			'YER' => 'Rial iemenita',
			'IRR' => 'Rial iraniano',
			'OMR' => 'Rial omanense',
			'KHR' => 'Riel cambojano',
			'MYR' => 'Ringgit malaio',
			'SAR' => 'Riyal saudita',
			'BYN' => 'Rublo bielorrusso',
			'BYR' => 'Rublo bielorrusso (2000–2016)',
			'TJR' => 'Rublo do Tadjiquistão',
			'LVR' => 'Rublo letão',
			'BYB' => 'Rublo novo bielo-russo (1994–1999)',
			'RUB' => 'Rublo russo',
			'RUR' => 'Rublo russo (1991–1998)',
			'SUR' => 'Rublo soviético',
			'LKR' => 'Rupia ceilandesa',
			'INR' => 'Rupia indiana',
			'IDR' => 'Rupia indonésia',
			'MVR' => 'Rupia maldiva',
			'MUR' => 'Rupia mauriciana',
			'NPR' => 'Rupia nepalesa',
			'PKR' => 'Rupia paquistanesa',
			'SCR' => 'Rupia seichelense',
			'ILR' => 'Sheqel antigo israelita',
			'ILS' => 'Sheqel novo israelita',
			'PEN' => 'Sol peruano',
			'PES' => 'Sol peruano (1863–1965)',
			'KGS' => 'Som quirguiz',
			'UZS' => 'Som uzbeque',
			'TJS' => 'Somoni tadjique',
			'ECS' => 'Sucre equatoriano',
			'GNS' => 'Syli da Guiné',
			'BDT' => 'Taka bengalesa',
			'WST' => 'Tala samoano',
			'LTT' => 'Talonas lituano',
			'KZT' => 'Tenge cazaque',
			'SIT' => 'Tolar Bons esloveno',
			'MNT' => 'Tugrik mongol',
			'MXV' => 'Unidade Mexicana de Investimento (UDI)',
			'XEU' => 'Unidade de Moeda Europeia',
			'ECV' => 'Unidade de Valor Constante (UVC) do Equador',
			'COU' => 'Unidade de Valor Real',
			'CLF' => 'Unidades de Fomento chilenas',
			'VUV' => 'Vatu vanuatuense',
			'KRO' => 'Won da Coreia do Sul (1945–1953)',
			'KPW' => 'Won norte-coreano',
			'KRW' => 'Won sul-coreano',
			'ATS' => 'Xelim austríaco',
			'KES' => 'Xelim queniano',
			'SOS' => 'Xelim somaliano',
			'TZS' => 'Xelim tanzaniano',
			'UGX' => 'Xelim ugandense',
			'UGS' => 'Xelim ugandense (1966–1987)',
			'CNY' => 'Yuan chinês',
			'ZRN' => 'Zaire Novo zairense (1993–1998)',
			'ZRZ' => 'Zaire zairense (1971–1993)',
			'PLN' => 'Zloti polonês',
			'PLZ' => 'Zloti polonês (1950–1995)',
			'BTC' => 'Bitcoin',
			'ETH' => 'Ethereum');

if (!isset($currencies[$currency])) return "";	

return $currencies[$currency];

}

public static function csv_to_array($data,$sep = ","){

	$lines = self::csv_to_array_lines($data);

	$return = [];

	foreach($lines as $line){
		$return[] = explode($sep,$line);
	}

	return $return;
}

public static function csv_to_array_lines($data){

	return explode("\r\n",$data);

}

public static function single_file_name($path){

	$pos = strrpos($path,"/");
	
	if ($pos !==false) 	return substr($path,($pos+1));			

	return $path;

}

public static function periods_cod_to_char($period){
	$periods = [
		'd' => 'diário',
		'w' => 'semanal',
		'm'	=> 'mensal'
	];
	return $periods[$period];
}

public static function uncompress_zip($source,$target){

	$Path = storage_path("app/".$source);			
	$storage_target = storage_path("app/".$target);	
	Storage::deleteDirectory($target);
	Zipper::make($Path)->extractTo($storage_target);
	return;

}

public static function list_zip_files($file){

	$path = storage_path("app/".$file);
	$files = Zipper::make($path)->listFiles();
	Zipper::make($path)->close();		
	return $files;

}

public static function delete_file($file){
	$to_remove = storage_path("app/".$file);		
	unlink($to_remove);		
	return;
}

public static function check_precision($number){

	if (strpos($number,".")===false) return 0;
	$f_number = (float)$number;
	$after_comma = substr($f_number,strpos($f_number,".")+1);

	$return =  strlen($after_comma);

	return $return;

}

public static function check_precision_from_quote($quote){


	$close = (float)$quote->close;

	if (strpos($close,".")===false) return 0;

	$after_comma = substr($close,strpos($close,".")+1);

	$return =  strlen($after_comma);
		// if ($quote->pair = 'BTCUSD' && $quote->ref_date = '2017-12-05') dd($quote);
	return $return;

}

public static function validate_email_access($host,$email,$password){

	if (!extension_loaded('imap')){
		die('Protocolo IMAP não instalado, entre em contato com a Augesystems');
	}


	$mbox = @imap_open("{".$host.":143/novalidate-cert}INBOX", $email, $password);		
	$errors = imap_errors();
	imap_alerts();

	if ($errors && count($errors)>0){
		return false;
	}

	return true;

}

public static function verify_memory_usage($title){

	$status = file_get_contents('/proc/' . getmypid() . '/status');
	$real_memory_usage = 0;

	$matchArr = array();
	preg_match_all('~^(VmRSS|VmSwap):\s*([0-9]+).*$~im', $status, $matchArr);

	if(isset($matchArr[2][0]) && isset($matchArr[2][1]))
	{
		$real_memory_usage =  intval($matchArr[2][0]) + intval($matchArr[2][1]);		
	}
	
	Log::info("================================================================");
	Log::info($title);
	Log::info("================================================================");
	Log::info("Pid: ".getmypid());
	Log::info("memory_get_usage() =" . memory_get_usage()/1024 . "kb\n");
	Log::info("memory_get_usage(true) =" . memory_get_usage(true)/1024 . "kb\n");	
	Log::info("memory_get_peak_usage() =" . memory_get_peak_usage()/1024 . "kb\n");	
	Log::info("memory_get_peak_usage(true) =" . memory_get_peak_usage(true)/1024 . "kb\n");	
	Log::info("custom memory_get_process_usage() =" . $real_memory_usage . "kb\n");	
	

}
}

?>