<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // if (env('APP_ENV')=='production'){
        //     $schedule->call(function(){
        //         \App\Http\Controllers\Admin\LeadsController::load_leads_from_email();
        //     })->everyMinute();            
        // }
        $name = "remote_load_".env('APP_ENV');
        $schedule->call(function(){            
            \App\Http\Controllers\Admin\QuotesController::load_quotes_from_remote_server();            
        })->everyMinute()->name($name)->withoutOverlapping(2);        
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
