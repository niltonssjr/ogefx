<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isAdmin',function($user){
            return $user->level == 'admin';
        });

        Gate::define('isUser',function($user){
            return $user->level == 'user';
        });  

        Gate::define('isInactive',function($user){            

            if (!$user->ativo) return true;

            $plano_validade = $user->plano_validade;

            if (trim($plano_validade!="")){
                $hoje = date_format(date_create(),"Y-m-d H:i:s");

                if($hoje > $plano_validade){
                    return true;
                }            
            }

            return false;
        });   


        //
    }
}
