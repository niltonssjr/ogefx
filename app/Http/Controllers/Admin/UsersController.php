<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\StdClasses\StdFunctions;
use App\Http\Requests\userFormRequest;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class UsersController extends Controller
{
    public function index(){

        Stdfunctions::allow_admin();        

    	$source_users = User::get();
    	
    	$titles = [
            '*'            ,
    		'Nome',
    		'Email',
    		'Plano',
    		'Vencimento',
    		'Status',
    		'Nível',
            'Último acesso',
    		'Ações'
    	];

    	$users = [];
    	foreach($source_users as $single_user){
    		$detail = [    		
                'id' => $single_user->id,
    			'name' => $single_user->name,
    			'email' => $single_user->email,
    			'plan_name' => self::plan_name($single_user->plano_nome),
    			'plan_expiration_date' => StdFunctions::fmt_datetime_dmy($single_user->plano_validade),
    			'status' => ($single_user->ativo)? 'Ativo' : 'Inativo',
    			'level' => ($single_user->level=='admin')? 'Administrador' : 'Usuário',
                'last_login' => $single_user->last_login? date_format(date_create($single_user->last_login),"d/m/Y H:i") : "-"
    		];
    		$users[] = $detail;
    	}

    	return view ('admin.usersList',compact('titles','users'));

    }

    public function insert_show(){

        Stdfunctions::allow_admin();

    	$user_form = (object)[];
    	$user_form->id =0;
    	$user_form->name = "";
    	$user_form->email = "";
    	$user_form->nivel = "user";
    	$user_form->status = "ativo";
    	$user_form->plano_nome = "";
    	$user_form->plano_validade = "";

    	return view ('admin.usersInsert',compact('user_form'));

    }

    public function insert_process(userFormRequest $request){

    	Stdfunctions::allow_admin();

    	$plan_expiration_date = StdFunctions::fmt_dmy_ymd($request->user_plano_validade);
    	
    	$temp_pw = explode("@",$request->user_email)[0];

    	$new_user = User::create([
    			'name' => $request->user_name,
    			'email' => $request->user_email,
    			'password' => bcrypt($temp_pw),
    			'plano_nome' => $request->user_plano_nome,
    			'plano_validade' => ($plan_expiration_date!="")? $plan_expiration_date: null,
    			'level' => $request->user_nivel,
    			'ativo' => ($request->user_status =='ativo')
    		]);

    	if (!$new_user){
    		return redirect()->back()->with(['error'=>'Erro no cadastramento do usuário']);
    	}

        Mail::to($request->user_email)->send(new WelcomeMail($new_user));

    	return redirect()->route('users.list');

    }

    public static function insert_from_lead($nome, $email, $trial_days){

        $user = User::where('email',$email)->first();

        if ($user){
            Log::info("O lead " . $email . " já estava cadastrado no sistema.");            
            return;
        }

        $plan_expiration_date = date_format(date_add(date_create(),date_interval_create_from_date_string($trial_days . " days")),"Y-m-d H:i:s");

        $temp_pw = explode("@",$email)[0];

        $new_user = User::create([
                'name' => $nome,
                'email' => $email,
                'password' => bcrypt($temp_pw),
                'plano_nome' => 'Trial',
                'plano_validade' => ($plan_expiration_date!="")? $plan_expiration_date: null,
                'level' => 'user',
                'ativo' => true
            ]);

        if (!$new_user){
            Log::error("O usuário não pode ser cadastrado: " . $nome . " - " . $email);
            return;
        }

        Mail::to($email)->send(new WelcomeMail($new_user));

        Log::info("Novo lead cadastrado: " . $nome . " - " . $email);

        return;
    }

    public function update_show($id){

        Stdfunctions::allow_admin();

    	$user = User::find($id);

    	if (!$user){
    		return redirect()->back()->with(['error'=>'Usuário não encontrado']);
    	}

    	$user_form = (object)[];
    	$user_form->id =$user->id;
    	$user_form->name = $user->name;
    	$user_form->email = $user->email;
    	$user_form->nivel = $user->level;
    	$user_form->status = ($user->ativo)? "ativo":"inativo";
    	$user_form->plano_nome = $user->plano_nome;
    	$user_form->plano_validade = StdFunctions::fmt_datetime_dmy($user->plano_validade);
        
    	return view ('admin.usersUpdate',compact('user_form'));
    }

    public function update_process(userFormRequest $request){
        
        Stdfunctions::allow_admin();

        $plan_expiration_date = StdFunctions::fmt_dmy_ymd($request->user_plano_validade);
        
        $temp_pw = explode("@",$request->user_email)[0];

        $update_user = User::find($request->user_id);

        if (!$update_user) return redirect()-back()->with(['error'=>'Usuário não encontrado']);

        $update_user->name = $request->user_name;
        $update_user->email = $request->user_email;
        $update_user->plano_nome = $request->user_plano_nome;
        $update_user->plano_validade =  ($plan_expiration_date!="")? $plan_expiration_date: null;
        $update_user->level = $request->user_nivel;
        $update_user->ativo = ($request->user_status =='ativo');

        $update_user->save();

        return redirect()->route('users.list');

    }    

    public function delete_process($id){

        Stdfunctions::allow_admin();

        $user = User::find($id);
        $name = $user->name;
        $user->delete();
        return redirect()->route('users.list')->with(['success'=>"Usuário {$name} removido"]);

    }

    public function inactivate_process($id){

        Stdfunctions::allow_admin();

        $user = User::find($id);
        $user->ativo = false;
        $name = $user->name;
        $user->save();
        return redirect()->route('users.list')->with(['success'=>"Usuário {$name} desativado."]);

    }

    public function reactivate_process($id){

        Stdfunctions::allow_admin();

        $user = User::find($id);
        $user->ativo = true;
        $name = $user->name;
        $user->save();
        return redirect()->route('users.list')->with(['success'=>"Usuário {$name} reativado."]);

    }    

    public function profile_show(){

        $user = Auth()->User();
        
        $user_form = (object)[
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
        ];

        return view ('admin.myprofile',compact('user_form'));

    }

    public function profile_update(Request $request){

        $current_user = Auth()->user();
        $myId = $current_user->id;

        $validator_rules = [
            'user_name' => 'required',
            'user_email' => 'required|unique:users,email,'.$myId
        ];

        $validator_messages = [
            'user_name.required' => 'Informe o seu nome',
            'user_email.required' => 'Informe seu e-mail',
            'user_email.unique' => 'Este e-mail está em uso por outro usuário'
        ];

        $request->validate($validator_rules,$validator_messages);

        $current_user->name = $request->user_name;
        $current_user->email = $request->user_email;
        $current_user->save();

        $user_form = (object)[
            'id' => $current_user->id,
            'name' => $current_user->name,
            'email' => $current_user->email,
        ];

        return redirect()->route('myprofile.show')->with(['success'=>'Perfil atualizado']);        

    }

    public function password_show(){

        return view ('admin.myaccess');

    }

    public function password_update(Request $request){

        $validator_rules = [
            'pw_password' => 'required|confirmed'
        ];

        $validator_messages = [
            'pw_passowrd.required' => 'Informe a nova senha',
            'pw_password.confirmed' => 'A confirmação de senha precisa ser igual à senha informada'
        ];

        $request->validate($validator_rules,$validator_messages);

        $user=auth()->user();
        $user->password = bcrypt($request->pw_password);
        $user->save();

        return redirect()->route('myaccess.show')->with(['success'=>'Senha atualizada']);

    }

    private static function plan_name($plan){

        $plans = [
            '1' => '1 mês',
            '3' => '3 meses',
            '6' => '6 meses',
            '9' => '9 meses',
            '12' => '12 meses'
        ];

        return (isset($plans[$plan]))? $plans[$plan] : $plan;

    }

}


