<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Portfolio;
use App\Models\Quote;
use App\StdClasses\StdFunctions;
use DB;
use Log;

class PortfolioController extends Controller
{
    public static function index(){

        Stdfunctions::allow_active();
        
    	$portfolio_list = Auth()->user()->portfolio()->orderBy('created_at','desc')->get();    	

    	$max_headers = 0;

		$lines = [];

    	foreach($portfolio_list as $port_line){

    		$portfolio = Quote::return_analysis(
    				$port_line->period,
    				$port_line->pair,
    				$port_line->start_ref_date,
    				$port_line->end_ref_date
    			);

    		//the amount of column headers according to the periods of all lines
    		if (count($portfolio->periods)> $max_headers) $max_headers = count($portfolio->periods);

    		$new_line = [
    			$port_line->id,
    			StdFunctions::fmt_datetime_dmyhis($port_line->created_at),
    			$port_line->pair,
    			UCFirst(StdFunctions::periods_cod_to_char($port_line->period)),
    			StdFunctions::fmt_datetime_dmy($port_line->start_ref_date),
    			StdFunctions::fmt_datetime_dmy($port_line->end_ref_date),
    			$portfolio->first_quote,
    			$portfolio->last_quote,
    			$portfolio->gain
    		];

    		foreach($portfolio->periods as $period_gain){
    			$new_line[] = $period_gain;
    		}

    		$lines[] = $new_line;
    		
    	}

    	$titles = ['*','Análise','Par','Período','Data inicial','Data Final','Abertura','Fechamento','Ganho'];
    	for($x=1;$x<=$max_headers;$x++) $titles[] = $x;

        $total_headers = $max_headers + 9;            

    	//include de remove button
    	foreach($lines as &$line){
    		$line[0] = "<button class='btn remove_portfolio_button' data-id= '" . $line[0] . "' onclick='remove_portfolio(this)'><i class='glyphicon glyphicon-trash'></i></button>";
            if (count($line) < $total_headers){
                for ($x=count($line);$x<$total_headers;$x++) $line[] = "";
            }
    	}
    	
    	return view ('admin.portfolio.portfolio',compact('titles','lines'));

    }

    public function create(Request $request){

    	$max_port = 2000;

    	$port = Auth()->user()->portfolio()->get();
    	if (count($port)>=$max_port) Auth()->user()->portfolio()->where('created_at',$port->min('created_at'))->delete();

        $pairs_to_add = [];

        if (preg_match("/^[A-Z]{6}$/",$request->pair)){
            $currency_1 = substr($request->pair,0,3);
            $currency_2 = substr($request->pair,3,3);
            $pairs = Quote::where('pair','like','%'.$currency_1.'%')
                            ->orWhere('pair','like','%'.$currency_2.'%')
                            ->select('pair')
                            ->distinct()
                            ->get();
            foreach ($pairs as $pair){
                $pairs_to_add[] = $pair->pair;
            }
        } else {
            $pairs_to_add[] = $request->pair;
        }

        foreach($pairs_to_add as $pair){
            Portfolio::create( [
                'user_id' => Auth()->user()->id,
                'start_ref_date' => $request->start_date,
                'end_ref_date' => $request->end_date,
                'pair' => $pair,
                'period' => $request->period,
                'created_at' => date_format(date_create(),"Y-m-d H:i:s")
            ]);            
        }


    	return json_encode(['success'=> 'Portfolio cadastrado']);
    }

    public function delete(Request $request){

    	$portfolio = Portfolio::find($request->id);
    
    	$portfolio->delete();

    	return json_encode(['success'=> 'Portfolio excluído']);
    }

    public function delete_all(Request $request){

        $user = Auth()->user();

        $portfolio = Portfolio::where('user_id',$user->id);
    
        $portfolio->delete();

        return redirect()->route('portfolio');
    }    
}
