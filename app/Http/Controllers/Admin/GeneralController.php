<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\StdClasses\StdFunctions;

class GeneralController extends Controller
{
    public function index(){
    	StdFunctions::allow_active();
    	return view('admin.dashboard');
    }
    public function test_email(){

        $user = auth()->user();
        return view('admin.emails.welcome',compact('user'));
    }    
}
