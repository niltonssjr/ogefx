<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Models\Quote;
use App\Models\Currency;
use App\StdClasses\StdFunctions;
use Lava;
use DB;
use App\Models\SystemConfig;
use Illuminate\Support\Facades\Cache;
use App\Repositories\SystemConfigRepository as System_Config;
use App\Repositories\PairCurrentInfoRepository as Pair_Current_Info;
use App\Repositories\QuotesRepository as QuotesRep;
use Response;

set_time_limit(0);
class QuotesController extends Controller
{

	public function Index($parm_period = "d"){

		$quotes = Quote::stats($parm_period);

		$titles = ['Referência','Moedas','Abertura','Mínimo','Máximo','Fechamento','Volume'];
		$period = StdFunctions::periods_cod_to_char($parm_period);

		$lines = self::format_quotes_list($quotes);


		return view ("admin.quotes.quotesList",compact('titles','lines','pair','period'));

	}

	public function quotesDaily(){
		$parm_period = "d";
		$quotes = Quote::stats($parm_period);

		$titles = ['Referência','Moedas','Abertura','Mínimo','Máximo','Fechamento','Volume'];
		$period = StdFunctions::periods_cod_to_char($parm_period);

		$lines = self::format_quotes_list($quotes);

		return view ("admin.quotes.quotesList",compact('titles','lines','pair','period'));

	}

	public function quotesWeekly(){

		$parm_period = "w";
		$quotes = Quote::stats($parm_period);

		$titles = ['Referência','Moedas','Abertura','Mínimo','Máximo','Fechamento','Volume'];
		$period = StdFunctions::periods_cod_to_char($parm_period);

		$lines = self::format_quotes_list($quotes);

		return view ("admin.quotes.quotesList",compact('titles','lines','pair','period'));

	}

	public function quotesMonthly(){

		$parm_period = "m";
		$quotes = Quote::stats($parm_period);

		$titles = ['Referência','Moedas','Abertura','Mínimo','Máximo','Fechamento','Volume'];
		$period = StdFunctions::periods_cod_to_char($parm_period);

		$lines = self::format_quotes_list($quotes);

		return view ("admin.quotes.quotesList",compact('titles','lines','pair','period'));

	}

	private function format_quotes_list($quotes){

		if (!$quotes) return [];

		$fmt_quotes = [];

		foreach($quotes as $quote)    	{
			$fmt_quotes[] = (object)[
				'ref_date' => StdFunctions::fmt_datetime_dmy($quote->ref_date),
				'pair' => $quote->pair,
				'open' => $quote->open,
				'minimum' => $quote->minimum,
				'maximum' => $quote->maximum,
				'close' => $quote->close,
				'volume' => $quote->volume
			];
		}

		return $fmt_quotes;

	}

	public static function calc_ret_afast_rv($last_quote){        

		$ret = (object)[
			'retorno' => (object) [
				'd_3' => 0,
				'd_2' => 0,
				'd_1' => 0,
				'd0' => 0,
				'd1' => 0,
				'd2' => 0,
				'd3' => 0,
			],
			'retorno_quotes' => (object) [
				'd_3' => 0,
				'd_2' => 0,
				'd_1' => 0,
				'd0' => 0,
				'd1' => 0,
				'd2' => 0,
				'd3' => 0,
			],            
			'relacao_volume' => (object)[
				'd_3' => 0,
				'd_2' => 0,
				'd_1' => 0,
				'd0' => 0,
				'd1' => 0,
				'd2' => 0,
				'd3' => 0,
			],
			'relacao_volume_quotes' => (object) [
				'd_3' => 0,
				'd_2' => 0,
				'd_1' => 0,
				'd0' => 0,
				'd1' => 0,
				'd2' => 0,
				'd3' => 0,
			],            
		];

		$ret->last_relacao_volume = 0;
		$ret->last_retorno = 0;
		$ret->last_distancia_media = 0;
		$ret->last_diferenca_media =0;

		if (!$last_quote) return $ret;

		$precision = Quote::get_pair_precision($last_quote->pair);

		$ret->retorno->d_3 = StdFunctions::float_to_percentage_color($last_quote->retorno_md150 - 3 * ($last_quote->retorno_sdev150));
		$ret->retorno->d_2 = StdFunctions::float_to_percentage_color($last_quote->retorno_md150 - 2 * ($last_quote->retorno_sdev150));
		$ret->retorno->d_1 = StdFunctions::float_to_percentage_color($last_quote->retorno_md150 - 1 * ($last_quote->retorno_sdev150));
		$ret->retorno->d0 = StdFunctions::float_to_percentage_color($last_quote->retorno_md150);
		$ret->retorno->d1 = StdFunctions::float_to_percentage_color($last_quote->retorno_md150 + 1 * ($last_quote->retorno_sdev150));
		$ret->retorno->d2 = StdFunctions::float_to_percentage_color($last_quote->retorno_md150 + 2 * ($last_quote->retorno_sdev150));
		$ret->retorno->d3 = StdFunctions::float_to_percentage_color($last_quote->retorno_md150 + 3 * ($last_quote->retorno_sdev150));

		$ret->relacao_volume->d_3 = StdFunctions::float_to_percentage_color($last_quote->relacao_volume_md150 - 3 * ($last_quote->relacao_volume_sdev150));
		$ret->relacao_volume->d_2 = StdFunctions::float_to_percentage_color($last_quote->relacao_volume_md150 - 2 * ($last_quote->relacao_volume_sdev150));
		$ret->relacao_volume->d_1 = StdFunctions::float_to_percentage_color($last_quote->relacao_volume_md150 - 1 * ($last_quote->relacao_volume_sdev150));
		$ret->relacao_volume->d0 = StdFunctions::float_to_percentage_color($last_quote->relacao_volume_md150);
		$ret->relacao_volume->d1 = StdFunctions::float_to_percentage_color($last_quote->relacao_volume_md150 + 1 * ($last_quote->relacao_volume_sdev150));
		$ret->relacao_volume->d2 = StdFunctions::float_to_percentage_color($last_quote->relacao_volume_md150 + 2 * ($last_quote->relacao_volume_sdev150));
		$ret->relacao_volume->d3 = StdFunctions::float_to_percentage_color($last_quote->relacao_volume_md150 + 3 * ($last_quote->relacao_volume_sdev150));

		$ret->retorno_quotes->d_3 = Quote::format_quote_value((1+ (($last_quote->retorno_md150 + (-3 * $last_quote->retorno_sdev150)))/100) * $last_quote->close,$precision);
		$ret->retorno_quotes->d_2 = Quote::format_quote_value((1+ (($last_quote->retorno_md150 + (-2 * $last_quote->retorno_sdev150)))/100) * $last_quote->close,$precision);
		$ret->retorno_quotes->d_1 = Quote::format_quote_value((1+ (($last_quote->retorno_md150 + (-1 * $last_quote->retorno_sdev150)))/100) * $last_quote->close,$precision);
		$ret->retorno_quotes->d0 = Quote::format_quote_value((1+ (($last_quote->retorno_md150 + (0 * $last_quote->retorno_sdev150)))/100) * $last_quote->close,$precision);
		$ret->retorno_quotes->d1 = Quote::format_quote_value((1+ (($last_quote->retorno_md150 + (1 * $last_quote->retorno_sdev150)))/100) * $last_quote->close,$precision);
		$ret->retorno_quotes->d2 = Quote::format_quote_value((1+ (($last_quote->retorno_md150 + (2 * $last_quote->retorno_sdev150)))/100) * $last_quote->close,$precision);
		$ret->retorno_quotes->d3 = Quote::format_quote_value((1+ (($last_quote->retorno_md150 + (3 * $last_quote->retorno_sdev150)))/100) * $last_quote->close,$precision);

		$ret->relacao_volume_quotes->d_3 = Quote::format_quote_value((1+ (($last_quote->relacao_volume_md150 + (-3 * $last_quote->relacao_volume_sdev150)))/100) * $last_quote->close,$precision);
		$ret->relacao_volume_quotes->d_2 = Quote::format_quote_value((1+ (($last_quote->relacao_volume_md150 + (-2 * $last_quote->relacao_volume_sdev150)))/100) * $last_quote->close,$precision);
		$ret->relacao_volume_quotes->d_1 = Quote::format_quote_value((1+ (($last_quote->relacao_volume_md150 + (-1 * $last_quote->relacao_volume_sdev150)))/100) * $last_quote->close,$precision);
		$ret->relacao_volume_quotes->d0 = Quote::format_quote_value((1+ (($last_quote->relacao_volume_md150 + (0 * $last_quote->relacao_volume_sdev150)))/100) * $last_quote->close,$precision);
		$ret->relacao_volume_quotes->d1 = Quote::format_quote_value((1+ (($last_quote->relacao_volume_md150 + (1 * $last_quote->relacao_volume_sdev150)))/100) * $last_quote->close,$precision);
		$ret->relacao_volume_quotes->d2 = Quote::format_quote_value((1+ (($last_quote->relacao_volume_md150 + (2 * $last_quote->relacao_volume_sdev150)))/100) * $last_quote->close,$precision);
		$ret->relacao_volume_quotes->d3 = Quote::format_quote_value((1+ (($last_quote->relacao_volume_md150 + (3 * $last_quote->relacao_volume_sdev150)))/100) * $last_quote->close,$precision);

		$ret->last_relacao_volume = StdFunctions::float_to_percentage($last_quote->relacao_volume);
		$ret->last_relacao_volume_marked = ($last_quote->relacao_volume >= 0.2);
		$ret->last_retorno = StdFunctions::float_to_percentage($last_quote->retorno);
		$ret->last_distancia_media = StdFunctions::float_to_percentage($last_quote->distancia_media);
		$fator_precision = pow(10,$precision);
		$ret->last_diferenca_media = round($last_quote->diferenca_media * $fator_precision);

		return $ret;

	}

	private function format_indicators_list($quotes){

		if (!$quotes) return [];

		$fmt_quotes = [];

		foreach($quotes as $quote)      {
			$fmt_quotes[] = (object)[
				'ref_date' => StdFunctions::fmt_datetime_dmy($quote->ref_date),
				'pair' => $quote->pair,
				'open' => $quote->open,
				'minimum' => $quote->minimum,
				'maximum' => $quote->maximum,
				'close' => $quote->close,
				'volume' => $quote->volume,
				'volume_milimiza' => $quote->volume_milimiza,
				'ganho' => $quote->ganho,
				'perda' => $quote->perda,
				'mg' => $quote->mg,
				'mp' => $quote->mp,
				'close_md5' => $quote->close_md5,
				'close_md_12' => $quote->close_md12,
				'rs_inv' => $quote->rs_inv,
				'rsi_14_inv' => $quote->rsi_14_inv,
				'rsi_normal' => $quote->rsi_normal,
				'rs_normal' => $quote->rs_normal,
				'pt' => $quote->pt,
				'usd' => $quote->usd,
				'dsd' => $quote->dsd,
				'u' => $quote->u,
				'd' => $quote->d,
				'rvi' => $quote->rvi,
				'mms_ptcci_20' => $quote->mms_ptcci_20,
				'true_range' => $quote->true_range,
				'atr_d1' => StdFunctions::float_to_percentage($quote->atr_d1),
				'mean_devation' => $quote->mean_devation,
				'ciclo_cci_20' => $quote->ciclo_cci_20,
				'mm21' => $quote->mm21,
				'ln' => StdFunctions::float_to_percentage($quote->ln),
				'desv_p' => StdFunctions::float_to_percentage($quote->desv_p),
				'f_afast' => StdFunctions::float_to_percentage($quote->f_afast),
				'mvh' => StdFunctions::float_to_percentage($quote->mvh),
				'emv' => $quote->emv,
				'media_14_emv' => $quote->media_14_emv,
				'media_exp_5_rsi' => $quote->media_exp_5_rsi,
				'media_exp_12_rsi' => $quote->media_exp_12_rsi,
				'media_exp_5_rvi' => $quote->media_exp_5_rvi,
				'media_exp_12_rvi' => $quote->media_exp_12_rvi,                
				'razao_gp_inv' => $quote->razao_gp_inv,
				'razao_pg' => $quote->razao_pg,
				'razao_rsi_rvi' => $quote->razao_rsi_rvi,
				'razao_rvi_rsi_inv' => $quote->razao_rvi_rsi_inv,
				'retorno' => StdFunctions::float_to_percentage($quote->retorno),
				'rv' => StdFunctions::float_to_percentage($quote->rv),
				'afast_ret' => StdFunctions::float_to_percentage($quote->afast_ret),
				'retorno_md150' => StdFunctions::float_to_percentage($quote->retorno_md150),
				'rv_md150' => StdFunctions::float_to_percentage($quote->rv_md150),
				'retorno_sdev150' => StdFunctions::float_to_percentage($quote->retorno_sdev150),
				'rv_sdev150' => StdFunctions::float_to_percentage($quote->rv_sdev150),
				'close_md_exp_6' => $quote->close_md_exp_6,
				'close_md_exp_12' => $quote->close_md_exp_12,
				'close_md_exp_24' => $quote->close_md_exp_24,
				'distancia_media' => StdFunctions::float_to_percentage($quote->distancia_media),
				'relacao_volume' => StdFunctions::float_to_percentage($quote->relacao_volume),
				'relacao_volume_md150' => StdFunctions::float_to_percentage($quote->relacao_volume_md150),
				'relacao_volume_sdev150' => StdFunctions::float_to_percentage($quote->relacao_volume_sdev150),
				'diferencia_media' => $quote->diferenca_media,
				'ln5_padrao' => $quote->ln5_padrao,
				'ln5_normalizado' => $quote->ln5_normalizado,
				'm_5_vol' => $quote->m_vol_5,
				'm_20_vol' => $quote->m_vol_20,
				'last_rsi_bs_ref_date' => $quote->last_rsi_bs_ref_date,
				'last_rsi_bs_rsi_normal' => $quote->last_rsi_bs_rsi_normal,
				'last_rsi_bs_close' => $quote->last_rsi_bs_close,
				'last_rvi_bs_ref_date' => $quote->last_rvi_bs_ref_date,
				'last_rvi_bs_rvi' => $quote->last_rvi_bs_rvi,
				'last_rvi_bs_close' => $quote->last_rvi_bs_close,				
			];
		}

		return $fmt_quotes;        

	}

	public function showLoad(){

        //check if the user is an administrator
		Stdfunctions::allow_admin();

        //check the files present on compress directory
		$fileData = Quote::files_storaged();

		return view ('admin.quotes.quotesLoad',compact('fileData'));
	}

	public function cleanQuotesDatabase(){

		DB::statement('delete from quotes');

		$files = Storage::allFiles('quotes_uncompressed');

		Storage::delete($files);

		$files = Storage::allFiles('quotes_compressed');

		Storage::delete($files);        

		return json_encode(["success"=>'Base de cotações reiniciada']);

	}
	public function loadSingleFile(Request $request){

        //check if the user in an administrator
		Stdfunctions::allow_admin();


        //check if the files was selected
		if (!$request->hasFile('quotesFile')){
			return redirect()->back()->with(['error'=>'Selecione o arquivo a ser importado']);
		}

        //check if the file is a zip file
		$ext = strToLower($request->quotesFile->extension());
		if ($ext!="zip" ){
			return redirect()->back()->with(['error'=>'Apenas arquivos com extensão ZIP são permitidos']);   
		}

        //take the original filename
		$targetName = $request->quotesFile->getClientOriginalName();


        //store the file
		$path = $request->quotesFile->storeAs('quotes_compressed',$targetName);

        //retrieve the list of files present on the zip file
		$file_list = StdFunctions::list_zip_files($path);

        //verify if the files are valid
        // foreach($file_list as $single_file){

        //     $valid = true;
        //     $single_filename = (strpos($single_file,"/")!==false)? substr($single_file,strpos($single_file,"/")+1) : $single_file;

        //     if (strpos($single_filename,".")!==false) $valid = false;            

        //     if (!$valid){
        //         StdFunctions::delete_file($path);                
        //         return redirect()->back()->with(['error'=>"Arquivo $single_file presente em $targetName não é válido para importação."]);
        //     }
        // }   

        //format the path
		$targetPath = "quotes_uncompressed/".$targetName."/";
		$targetPath = str_replace(".zip","",$targetPath);        

        //uncompress file
		StdFunctions::uncompress_zip($path,$targetPath);

    //block auto-import process
		$loading_raw_files = SystemConfig::firstOrCreate(['key'=>'loading_quotes']);
		$loading_raw_files->value="yes";
		$loading_raw_files->save();

    //import the files
		$return = Quote::import_quotes_file($targetPath);

		$loading_raw_files->value="no";
		$loading_raw_files->save();

		return redirect()->route('quotes.showload')->with(['success'=>"Arquivo $targetName carregado com sucesso."]);


	}

	public function stats(){
		Quote::stats();
	}

	public function unzip(){

		StdFunctions::uncompress_zip("quotes_compressed/PlanilhasD3.zip","quotes_uncompressed");
		StdFunctions::uncompress_zip("quotes_compressed/PlanilhasM3.zip","quotes_uncompressed");
		StdFunctions::uncompress_zip("quotes_compressed/PlanilhasW3.zip","quotes_uncompressed");

		$this->loadSingleFile();

	}

	public function history(Request $request){
		Stdfunctions::allow_admin();        
		$validate_rules = [
			'pair' => 'not_in:sel',
			'period' => 'not_in:sel'
		];

		$validate_messages = [
			'pair.not_in' => 'Informe o par de moedas para pesquisa',
			'period.not_in' => 'Informe o tipo de cotação'
		];

		$request->validate($validate_rules,$validate_messages);

		$periods = [
			'd' => 'Diário',
			'w' => 'Semanal',
			'm' => 'Mensal',
		];
		$pairs = Quote::currencies();
		$titles = ['Referência','Moedas','Abertura','Mínimo','Máximo','Fechamento','Volume'];        

		$default_pair = (count($pairs)>0) ? $pairs[0]->pair : null;
		$selected_pair = ($request->pair)? $request->pair:$default_pair;
		$selected_period = ($request->period)? $periods[$request->period]:$periods["d"];

		$research_pair = $selected_pair;
		$research_period = ($request->period)? $request->period:"d";

		$start_date = ($request->start_date)? StdFunctions::fmt_dmy_ymd($request->start_date):null;
		$end_date = ($request->end_date)? StdFunctions::fmt_dmy_ymd($request->end_date):null;

		$quotes = Quote::filter_quotes($research_period,$research_pair,$start_date,$end_date)->get();        
		$lines = self::format_quotes_list($quotes);

		$start_date = $quotes->min('ref_date');
		$end_date = $quotes->max('ref_date');

		$default_values = (object)[
			'pair' => ($request->pair)? $request->pair:$selected_pair,
			'period' => ($request->period)? $periods[$request->period]:$selected_period,
			'start_date' => ($request->start_date)? $request->start_date : StdFunctions::fmt_datetime_dmy($start_date),
			'end_date' => ($request->end_date)? $request->end_date : StdFunctions::fmt_datetime_dmy($end_date),
		];


		$history_chart = Lava::dataTable();
		$history_chart->addDateColumn('Data');
		$history_chart->addNumberColumn('Cotação');

		foreach($quotes as $quote){
			$history_chart->addRow([$quote->ref_date,$quote->close]);
		}

		Lava::LineChart('hist_quotes',$history_chart,['height'=>500,'hAxis'=>["gridlines"=>["count"=>20]]]);

		return view ('admin.quotes.quotesHistory',compact('selected_period','selected_pair','periods','pairs','titles','lines','default_values','history_chart'));

	}

	public function indicators(Request $request){

		Stdfunctions::allow_admin();        

		$validate_rules = [
			'pair' => 'not_in:sel',
			'period' => 'not_in:sel'
		];

		$validate_messages = [
			'pair.not_in' => 'Informe o par de moedas para pesquisa',
			'period.not_in' => 'Informe o tipo de cotação'
		];

		$request->validate($validate_rules,$validate_messages);

		$periods = [
			'd' => 'Diário',
			'w' => 'Semanal',
			'm' => 'Mensal',
		];

		$pairs = Quote::currencies();

		$default_pair = (count($pairs)>0) ? $pairs[0]->pair : null;
		$selected_pair = ($request->pair)? $request->pair:$default_pair;
		$selected_period = ($request->period)? $periods[$request->period]:$periods["d"];

		$research_pair = $selected_pair;
		$research_period = ($request->period)? $request->period:"d";

		$start_date = ($request->start_date)? StdFunctions::fmt_dmy_ymd($request->start_date):null;
		$end_date = ($request->end_date)? StdFunctions::fmt_dmy_ymd($request->end_date):null;

		$quotes = Quote::filter_quotes($research_period,$research_pair,$start_date,$end_date)->get();  

		$start_date = $quotes->min('ref_date');
		$end_date = $quotes->max('ref_date');

		$ln_series = Quote::ln_series($research_period,$research_pair,$start_date,$end_date);
		$ln_normalizado = $ln_series->normalizado;
		$ln_padrao = $ln_series->padrao;

		foreach($quotes as &$quote)      {
			$quote->ln5_padrao = (isset($ln_padrao[$quote->ref_date])) ? $ln_padrao[$quote->ref_date] : 0;
			$quote->ln5_normalizado = (isset($ln_normalizado[$quote->ref_date])) ? $ln_normalizado[$quote->ref_date] : 0;
		}

		$lines = self::format_indicators_list($quotes);



		$default_values = (object)[
			'pair' => ($request->pair)? $request->pair:$selected_pair,
			'period' => ($request->period)? $periods[$request->period]:$selected_period,
			'start_date' => ($request->start_date)? $request->start_date : StdFunctions::fmt_datetime_dmy($start_date),
			'end_date' => ($request->end_date)? $request->end_date : StdFunctions::fmt_datetime_dmy($end_date),
		];

		$titles = [
			'Referência',
			'Moedas',
			'Abertura',
			'Mínimo',
			'Máximo',
			'Fechamento',
			'Volume',
			'Volume Milimiza',
			'Ganho',
			'Perda',
			'MG',
			'MP',
			'Fechamento MD5',
			'Fechamento MD12',
			'RS invertido',
			'RSI-14 Inv',
			'RSI Normal',
			'RS Normal',
			'PT',
			'USD',
			'DSD',
			'U',
			'D',
			'RVI',
			'MMS PTCCI 20',
			'True Range',
			'ATR-D1',
			'Mean Devation',
			'20 Ciclo CCI',
			'MM21',
			'LN',
			'Desv.P',
			'F.Afast',
			'mvh',
			'EMV',
			'Média 14 EMV',
			'Média Exp 5 RSI',
			'Média Exp 12 RSI',
			'Média Exp 5 RVI',
			'Média Exp 12 RVI',            
			'Razão G/P Inv',
			'Razão P/G',
			'Razão RSI/RVI',
			'Razão RVI/RSI',
			'Retorno',
			'RV',
			'Afast-ret',
			'Média Retorno 150',
			'Média RV 150',
			'Desvio Retorno 150',
			'Desvio RV 150',
			'Fechamento MD6',
			'Fechamento MD12',
			'Fechamento MD24',
			'Distância da média',
			'Relação volume',
			'Relação volume - MD150',
			'Relação volume - Desvio',
			'Diferença da média',
			'ln5_padrao',
			'ln5_normalizado',
			'Media Volat 5',
			'Média Volat 20',
			'Last BS RSI - data',
			'Last BS RSI - RSI NORMAL',
			'Last BS RSI - Fechamento',
			'Last BS RVI - data',
			'Last BS RVI - RVI',
			'Last BS RVI - Fechamento',			
		];

        // $history_chart = Lava::dataTable();
        // $history_chart->addDateColumn('Data');
        // $history_chart->addNumberColumn('Cotação');


        // $dt_forca_pot = Lava::dataTable();
        // $dt_forca_pot->addDateColumn('Data');
        // // $dt_forca_pot->AddStringColumn('Data');
        // $dt_forca_pot->addNumberColumn('RSI normal');
        // $dt_forca_pot->addNumberColumn('Média EXP 5 RSI');
        // $dt_forca_pot->addNumberColumn('Média EXP 12 RSI');

        // $dt_volat = Lava::dataTable();
        // $dt_volat->addDateColumn('Data');
        // $dt_volat->addNumberColumn('RVI');
        // $dt_volat->addNumberColumn('40');
        // $dt_volat->addNumberColumn('50');
        // $dt_volat->addNumberColumn('60');

        // $dt_rvi = Lava::dataTable();
        // $dt_rvi->addDateColumn('Data');
        // $dt_rvi->addNumberColumn('RVI');
        // $dt_rvi->addNumberColumn('1');

        // foreach($quotes as $quote){
        //     $history_chart->addRow([$quote->ref_date,$quote->close]);
        //     $dt_forca_pot->addRow([$quote->ref_date,$quote->rsi_normal,$quote->media_exp_5_rsi,$quote->media_exp_12_rsi]);
        //     $dt_volat->addRow([$quote->ref_date,$quote->rvi,40,50,60]);
        //     $dt_rvi->addRow([$quote->ref_date,$quote->razao_rsi_rvi,1]);
        // }

        // Lava::LineChart('hist_quotes',$history_chart,['height'=>500,'hAxis'=>["gridlines"=>["count"=>20]]]);        
        // Lava::LineChart('chart_forca',$dt_forca_pot,['height'=>500,'hAxis'=>["gridlines"=>["count"=>20]]]);
        // Lava::LineChart('chart_volat',$dt_volat,['height'=>500,'hAxis'=>["gridlines"=>["count"=>20]]]);
        // Lava::LineChart('chart_rvi',$dt_rvi,['height'=>500,'hAxis'=>["gridlines"=>["count"=>20]]]);

		return view ('admin.indicators.indicators',compact('selected_period','selected_pair','periods','pairs','titles','lines','default_values'));        
	}

	private function volatility($pair,$start_date = null,$end_date = null){        

		$col_volat = collect([]);

		$vol = (object)[

			'last_d_quote' => 0,
			'last_d_quote_date' => "",
			'vol_d' => 0,
			'vol_d_p' => "0%",
			'up_d' => 0,
			'down_d' => 0,

			'rvi_d_quote_date' => "",
			'rvi_d_quote' => 0,
			'rvi_d_ind' => 0,
			'rvi_d_status' => "",
			'rvi_d_last_ind' => 0,

			'rsi_d_ref_date' => "",
			'rsi_d_quote' => 0,            
			'rsi_d_status' => "",
			'rsi_d_ind' => 0,
			'rsi_d_last_ind' => 0,

			'last_w_quote' => 0,
			'last_w_quote_date' => "",
			'vol_w' => 0,
			'vol_w_p' => "0%",
			'up_w' => 0,
			'down_w' => 0,

			'rvi_w_quote_date' => "",
			'rvi_w_quote' => 0,
			'rvi_w_ind' => 0,
			'rvi_w_status' => "",
			'rvi_w_last_ind' => 0,

			'rsi_w_ref_date' => "",
			'rsi_w_quote' => 0,            
			'rsi_w_status' => "",
			'rsi_w_ind' => 0,
			'rsi_w_last_ind' => 0,

			'last_m_quote' => 0,
			'last_m_quote_date' => "",
			'vol_m' => 0,
			'vol_m_p' => "0%",
			'up_m' => 0,
			'down_m' => 0,

			'rvi_m_quote_date' => "",
			'rvi_m_quote' => 0,
			'rvi_m_ind' => 0,
			'rvi_m_status' => "",
			'rvi_m_last_ind' => 0,

			'rsi_m_ref_date' => "",
			'rsi_m_quote' => 0,            
			'rsi_m_status' => "",
			'rsi_m_ind' => 0,
			'rsi_m_last_ind' => 0,

		];        

		// $quotes = Quote::filter_quotes('d',$pair,$start_date,$end_date)->get();    
		// $quotes_m = Quote::filter_quotes('m',$pair,$start_date,$end_date)->get();
		// $quotes_w = Quote::filter_quotes('w',$pair,$start_date,$end_date)->get();

		// foreach($quotes as $quote){
		// 	$volat = $quote->atr_d1;
		// 	$col_volat->push($volat);
		// }        

		// $last_d_quote = null;
		// $last_m_quote = null;
		// $last_w_quote = null;

		// if ($quotes && count($quotes)>0) $last_d_quote = $quotes[0];
		// if ($quotes_m && count($quotes_m)>0) $last_m_quote = $quotes_m[0];
		// if ($quotes_w && count($quotes_w)>0) $last_w_quote = $quotes_w[0];

		$last_d_quote = Quote::quote_by_ref_date("d",$pair,$end_date);
		$last_m_quote = Quote::quote_by_ref_date("m",$pair,$end_date);
		$last_w_quote = Quote::quote_by_ref_date("w",$pair,$end_date);

		if ($last_d_quote){       

			$precision = Quote::get_pair_precision($last_d_quote->pair);

			$vol->last_d_quote = Quote::quote_to_points($last_d_quote->close,$pair); 
			$vol->last_d_quote_date = StdFunctions::fmt_datetime_dmy($last_d_quote->ref_date);                        
			// $vol->vol_d = $col_volat->slice(0,1)[0];   
			$vol->vol_d = $last_d_quote->atr_d1;   
			$vol->vol_d_p = round($vol->vol_d * 100, 2)  . "%";
			$vol->up_d = Quote::format_quote_value($last_d_quote->close * (1+$vol->vol_d),$precision);
			$vol->down_d = Quote::format_quote_value($last_d_quote->close *(1-$vol->vol_d),$precision);    

			// $rvi = self::last_rvi_buy_sell($quotes);
			$rvi = (object)[
				'ref_date' => $last_d_quote->last_rvi_bs_ref_date,
				'rvi' => $last_d_quote->last_rvi_bs_rvi,
				'status' => '',
				'close' => $last_d_quote->last_rvi_bs_close
			];			

			$vol->rvi_d_quote_date = StdFunctions::fmt_datetime_dmy($rvi->ref_date);            
			$vol->rvi_d_quote = Quote::format_quote_value($rvi->close,$precision);
			$vol->rvi_d_status = self::status_rsi($rvi->rvi);
			$vol->rvi_d_ind = round($rvi->rvi);
			$vol->rvi_d_last_ind = round($last_d_quote->rvi);

			// $rsi = self::last_rsi_buy_sell($quotes);
			$rsi = (object)[
				'ref_date' => $last_d_quote->last_rsi_bs_ref_date,
				'rsi' => $last_d_quote->last_rsi_bs_rsi_normal,
				'status' => '',
				'close' => $last_d_quote->last_rsi_bs_close,
			];			

			$vol->rsi_d_ref_date = StdFunctions::fmt_datetime_dmy($rsi->ref_date);
			$vol->rsi_d_quote = Quote::format_quote_value($rsi->close,$precision);
			$vol->rsi_d_status = self::status_rsi($rsi->rsi);
			$vol->rsi_d_ind = round($rsi->rsi);
			$vol->rsi_d_last_ind = round($last_d_quote->rsi_normal);
		}

		if ($last_w_quote){
			$precision = Quote::get_pair_precision($last_w_quote->pair);
			$vol->last_w_quote = Quote::quote_to_points($last_w_quote->close,$pair);
			$vol->last_w_quote_date = StdFunctions::fmt_datetime_dmy($last_w_quote->ref_date);                        
			// $vol->vol_w = $col_volat->slice(0,5)->avg(); 
			$vol->vol_w = $last_w_quote->m_vol_5;   
			$vol->vol_w_p = round($vol->vol_w * 100, 2)  . "%";
			$vol->up_w = Quote::format_quote_value($last_w_quote->close * (1+$vol->vol_w),$precision);
			$vol->down_w = Quote::format_quote_value($last_w_quote->close *(1-$vol->vol_w),$precision);   

			// $rvi = self::last_rvi_buy_sell($quotes);
			$rvi = (object)[
				'ref_date' => $last_w_quote->last_rvi_bs_ref_date,
				'rvi' => $last_w_quote->last_rvi_bs_rvi,
				'status' => '',
				'close' => $last_w_quote->last_rvi_bs_close
			];	

			$vol->rvi_w_quote_date = StdFunctions::fmt_datetime_dmy($rvi->ref_date);            
			$vol->rvi_w_quote = Quote::format_quote_value($rvi->close,$precision);
			$vol->rvi_w_status = self::status_rsi($rvi->rvi);
			$vol->rvi_w_ind = round($rvi->rvi);
			$vol->rvi_w_last_ind = round($last_w_quote->rvi);


			// $rsi = self::last_rsi_buy_sell($quotes);
			$rsi = (object)[
				'ref_date' => $last_w_quote->last_rsi_bs_ref_date,
				'rsi' => $last_w_quote->last_rsi_bs_rsi_normal,
				'status' => '',
				'close' => $last_w_quote->last_rsi_bs_close,
			];	

			$vol->rsi_w_ref_date = StdFunctions::fmt_datetime_dmy($rsi->ref_date);
			$vol->rsi_w_quote = Quote::format_quote_value($rsi->close,$precision);
			$vol->rsi_w_status = self::status_rsi($rsi->rsi);   
			$vol->rsi_w_ind = round($rsi->rsi);    
			$vol->rsi_w_last_ind = round($last_w_quote->rsi_normal);     

		}

		if ($last_m_quote){
			$precision = Quote::get_pair_precision($last_m_quote->pair);
            // dd($last_m_quote->close);
			$vol->last_m_quote = Quote::quote_to_points($last_m_quote->close,$pair);
			$vol->last_m_quote_date = StdFunctions::fmt_datetime_dmy($last_m_quote->ref_date);                        
			// $vol->vol_m = $col_volat->slice(0,20)->avg();   
			$vol->vol_m = $last_w_quote->m_vol_20;   
			$vol->vol_m_p = round($vol->vol_m * 100, 2)  . "%"; 
			$vol->up_m = Quote::format_quote_value($last_m_quote->close * (1+$vol->vol_m),$precision);
			$vol->down_m = Quote::format_quote_value($last_m_quote->close *(1-$vol->vol_m),$precision);         

			// $rvi = self::last_rvi_buy_sell($quotes);
			$rvi = (object)[
				'ref_date' => $last_m_quote->last_rvi_bs_ref_date,
				'rvi' => $last_m_quote->last_rvi_bs_rvi,
				'status' => '',
				'close' => $last_m_quote->last_rvi_bs_close
			];

			$vol->rvi_m_quote_date = StdFunctions::fmt_datetime_dmy($rvi->ref_date);            
			$vol->rvi_m_quote = Quote::format_quote_value($rvi->close,$precision);
			$vol->rvi_m_status = self::status_rsi($rvi->rvi);
			$vol->rvi_m_ind = round($rvi->rvi);
			$vol->rvi_m_last_ind = round($last_m_quote->rvi);

			// $rsi = self::last_rsi_buy_sell($quotes);
			$rsi = (object)[
				'ref_date' => $last_m_quote->last_rsi_bs_ref_date,
				'rsi' => $last_m_quote->last_rsi_bs_rsi_normal,
				'status' => '',
				'close' => $last_m_quote->last_rsi_bs_close,
			];	

			$vol->rsi_m_ref_date = StdFunctions::fmt_datetime_dmy($rsi->ref_date);  
			$vol->rsi_m_quote = Quote::format_quote_value($rsi->close,$precision);
			$vol->rsi_m_status = self::status_rsi($rsi->rsi);  
			$vol->rsi_m_ind = round($rsi->rsi); 
			$vol->rsi_m_last_ind = round($last_m_quote->rsi_normal);

		}                       

		return $vol;
	}

	private static function last_rsi_buy_sell(&$quotes){

		$buy_sell = (object)[
			'ref_date' => '',
			'rsi' => 0,
			'status' => '',
			'close' => 0
		];

		foreach($quotes as $quote){
			if ($quote->rsi_normal < 29.99 || $quote->rsi_normal > 69.99){
				$buy_sell->ref_date = $quote->ref_date;
				$buy_sell->rsi = $quote->rsi_normal;
				$buy_sell->close = $quote->close;              
				break;
			}
		}

		return $buy_sell;

	}

	private static function last_rvi_buy_sell(&$quotes){

		$buy_sell = (object)[
			'ref_date' => '',
			'rvi' => 0,
			'status' => '',
			'close' => 0
		];

		foreach($quotes as $quote){
			if ($quote->rvi < 29.99 || $quote->rvi > 69.99){
				$buy_sell->ref_date = $quote->ref_date;
				$buy_sell->rvi = $quote->rvi;
				$buy_sell->close = $quote->close;               
				break;
			}
		}

		return $buy_sell;

	}

    // private static function status_rvi($rvi){
    //     if ($rvi>50) return "<i class='glyphicon glyphicon-circle-arrow-up' style='color:green;font-size:110%'></i>";
    //     if ($rvi<=50) return "<i class='glyphicon glyphicon-circle-arrow-down' style='color:red;font-size:110%''></i>";
    //     return "<i class='glyphicon glyphicon-record' style='color:orange;font-size:110%''></i>";
    // }

	private static function status_rsi($rsi){

		if ($rsi>69.99) return "<i class='glyphicon glyphicon-circle-arrow-down' style='color:red;font-size:110%'></i>";

		if ($rsi==0) return "<i class='glyphicon glyphicon-record' style='color:orange;font-size:110%''></i>";

		if ($rsi<29.99) return "<i class='glyphicon glyphicon-circle-arrow-up' style='color:green;font-size:110%''></i>";

		return "<i class='glyphicon glyphicon-record' style='color:orange;font-size:110%''></i>";
	}    

	public function analysis(Request $request){

		// Log::info("Entrei na rotina: " . date_format(date_create(),"H:i:s.u"));
		Stdfunctions::allow_active();



		// Log::info("Gravei o acesso: " . date_format(date_create(),"H:i:s.u"));
		$validate_rules = [
			'pair' => 'not_in:sel',
			'period' => 'not_in:sel'
		];

		$validate_messages = [
			'pair.not_in' => 'Informe o par de moedas para pesquisa',
			'period.not_in' => 'Informe o tipo de cotação'
		];

		$request->validate($validate_rules,$validate_messages);

		$periods = [
			'd' => 'Diário',
			'w' => 'Semanal',
			'm' => 'Mensal',
		];

		$pairs = Quote::currencies();
		// Log::info("Busquei os pares: " . date_format(date_create(),"H:i:s.u"));

		$default_pair = (count($pairs)>0) ? $pairs[0]->pair : null;
		$selected_pair = ($request->pair)? $request->pair:$default_pair;
		$selected_period = ($request->period)? $periods[$request->period]:$periods["d"];

		$research_pair = $selected_pair;
		$research_period = ($request->period)? $request->period:"d";

		$start_date = ($request->start_date)? StdFunctions::fmt_dmy_ymd($request->start_date):null;
		$end_date = ($request->end_date)? StdFunctions::fmt_dmy_ymd($request->end_date):null;

		$quotes = Quote::filter_quotes($research_period,$research_pair,$start_date,$end_date)->get();  
		// Log::info("Trouxe todas as cotações: " . date_format(date_create(),"H:i:s.u"));      

		$volat_data = self::volatility($selected_pair,$start_date,$end_date);

		// Log::info("Calculei a volatilidade: " . date_format(date_create(),"H:i:s.u"));      

		$rv_quote = (count($quotes)>0) ? $quotes[0] : null;
		$ret_rv = self::calc_ret_afast_rv($rv_quote);
		// Log::info("Calculei a ref_afast_rv: " . date_format(date_create(),"H:i:s.u"));      
		$ft_semaforo = ($rv_quote)? Quote::check_signals($rv_quote) : "";

        // $return_analysis = self::return_analysis($quotes);
		$return_analysis = Quote::return_analysis($research_period,$research_pair,$start_date,$end_date,12);
		// Log::info("Calculei a análise de retornos: " . date_format(date_create(),"H:i:s.u"));      

		$forca_tendencia_atual = self::forca_tendencia_atual();

		// Log::info("Calculei a força tendência atual: " . date_format(date_create(),"H:i:s.u"));      

		// $semaforos_todas_as_cotacoes = json_decode(self::semaforos_todas_as_cotacoes());

		// Log::info("Calculei o semáforo de todas as cotações: " . date_format(date_create(),"H:i:s.u"));      

		$start_date = $quotes->min('ref_date');
		$end_date = $quotes->max('ref_date');

		$serie_de_retorno = QuotesRep::serie_de_retorno_padrao($quotes);		
		// Log::info("Calculei as séries LN: " . date_format(date_create(),"H:i:s.u"));      
		$retornos_normalizado = $serie_de_retorno->normalizado;
		$retornos_padrao = $serie_de_retorno->padrao;
		$retornos_periodo_sigla = $serie_de_retorno->periodo_sigla;
		$retornos_periodo_titulo = $serie_de_retorno->periodo_titulo;
		$retornos_desvio_padrao = $serie_de_retorno->desvio_padrao;
		$retornos_desvio_normalizado = $serie_de_retorno->desvio_normalizado;
		$retornos_atual_padrao = $serie_de_retorno->atual_padrao;
		$retornos_atual_normalizado = $serie_de_retorno->atual_normalizado;

		$pair_current_info = new Pair_Current_Info;
		$retorno_pair_current_info = $pair_current_info->retrieve_from_cache($research_pair);	
		$tab_direcoes_tendencia = [
			'0' => "glyphicon glyphicon-record neutral_percentage",
			'1' => "glyphicon glyphicon-circle-arrow-down negative_percentage",
			'2' => "glyphicon glyphicon-circle-arrow-up positive_percentage",
			"" => "glyphicon glyphicon-record neutral_percentage",
		];

		$indicadores_cruzamento_media = (object)[
			'cruzm15' => $tab_direcoes_tendencia[$retorno_pair_current_info->ind_15_cruzamento_media],
			'cruzm30' => $tab_direcoes_tendencia[$retorno_pair_current_info->ind_30_cruzamento_media],
			'cruzm60' => $tab_direcoes_tendencia[$retorno_pair_current_info->ind_60_cruzamento_media],
			'cruzm240' => $tab_direcoes_tendencia[$retorno_pair_current_info->ind_240_cruzamento_media],
			'cruzm1440' => $tab_direcoes_tendencia[$retorno_pair_current_info->ind_1440_cruzamento_media]
		];

		$default_values = (object)[
			'pair' => ($request->pair)? $request->pair:$selected_pair,
			'period' => ($request->period)? $periods[$request->period]:$selected_period,
			'start_date' => ($request->start_date)? $request->start_date : StdFunctions::fmt_datetime_dmy($start_date),
			'end_date' => ($request->end_date)? $request->end_date : StdFunctions::fmt_datetime_dmy($end_date),
		];

        // $signals = Quote::check_signals($quote);

		$hc_hist_quotes=[];
		$hc_hist_quotes_media_5=[];
		$hc_hist_quotes_media_12=[];

		$hc_forca_rsi_normal =[];
		$hc_forca_media_5 =[];
		$hc_forca_media_12 =[];

		$hc_volat_rvi=[];

		$hc_volat_media_rvi=[];

		$hc_volat_40=[];
		$hc_volat_50=[];

		$hc_rsi_rvi=[];

		$hc_index_volat_avg_afast_ret = [];        

		$hc_indice_conv = [];

		$hc_eq_media_volat = [];

		$hc_retornos_series = [];

		$hc_rv = [];
		$hc_fator = [];

		foreach($quotes as $quote){

			$hc_hist_quotes[] = [strtotime($quote->ref_date)*1000, (float)$quote->close];
			$hc_hist_quotes_media_5[] = [strtotime($quote->ref_date)*1000,(float)$quote->close_md5];
			$hc_hist_quotes_media_12[] = [strtotime($quote->ref_date)*1000,(float)$quote->close_md12];    

			$hc_forca_rsi_normal[] = [strtotime($quote->ref_date)*1000,round((float)$quote->rsi_normal,2)];
			$hc_forca_media_5[] = [strtotime($quote->ref_date)*1000,round((float)$quote->media_exp_5_rsi,2)];
			$hc_forca_media_12[] = [strtotime($quote->ref_date)*1000,round((float)$quote->media_exp_12_rsi,2)];    

			$hc_volat_rvi[] = [strtotime($quote->ref_date)*1000,round((float)$quote->rvi,2)];

			$hc_volat_media_rvi[] = [strtotime($quote->ref_date)*1000,round((float)$quote->media_exp_5_rvi,2)];

			$hc_rsi_rvi[] = [strtotime($quote->ref_date)*1000,round((float)$quote->razao_rsi_rvi,2)];     

			$afast_ret = 0.5;
			if ($quote->close > 0 && $quote->close !=$quote->open) $afast_ret = $quote->f_afast - (($quote->close - $quote->open) / $quote->open);            

			$hc_index_volat_avg_afast_ret[] = [strtotime($quote->ref_date)*1000,round(((float)$afast_ret)*100,2)];     

			$hc_indice_conv[] = [strtotime($quote->ref_date)*1000,round((float)$quote->ciclo_cci_20,2)];     

			$hc_eq_media_volat[] = [strtotime($quote->ref_date)*1000,(float)$quote->media_14_emv]; 

			$hc_rv[] = [strtotime($quote->ref_date)*1000,round((float)$quote->relacao_volume * 100,2)];
			$hc_fator[] = [strtotime($quote->ref_date)*1000,round((float)$quote->distancia_media * 100,2)];

		}   

		$hc_retornos_normalizado = [];

		foreach($retornos_normalizado as $key=>$value){
			$hc_retornos_normalizado[] = [strtotime($key)*1000,(float)$value];
		}

		$hc_retornos_padrao = [];

		foreach($retornos_padrao as $key=>$value){
			$hc_retornos_padrao[] = [strtotime($key)*1000,(float)$value];
		}

		// Log::info("Antes da força das moedas: " . date_format(date_create(),"H:i:s.u"));      
		$forca_das_moedas = json_decode(self::forca_das_moedas());
		// Log::info("Calculei a força das moedas: " . date_format(date_create(),"H:i:s.u"));      

		$j_hc_hist_quotes = json_encode($hc_hist_quotes);
		$j_hc_hist_quotes_media_5 = json_encode($hc_hist_quotes_media_5);
		$j_hc_hist_quotes_media_12 = json_encode($hc_hist_quotes_media_12);

		$j_hc_forca_rsi_normal = json_encode($hc_forca_rsi_normal);
		$j_hc_forca_media_5 = json_encode($hc_forca_media_5);
		$j_hc_forca_media_12 = json_encode($hc_forca_media_12);        

		$j_hc_volat_rvi = json_encode($hc_volat_rvi);

		$j_hc_volat_media_rvi = json_encode($hc_volat_media_rvi);

		$j_hc_rsi_rvi = json_encode($hc_rsi_rvi);

		$j_hc_index_volat_avg_afast_ret = json_encode($hc_index_volat_avg_afast_ret);

		$j_hc_indice_conv = json_encode($hc_indice_conv);

		$j_hc_eq_media_volat = json_encode($hc_eq_media_volat);

		$j_hc_retornos_normalizado = json_encode($hc_retornos_normalizado);
		$j_hc_retornos_padrao = json_encode($hc_retornos_padrao);

		$j_hc_rv = json_encode($hc_rv);
		$j_hc_fator = json_encode($hc_fator);		

        

		return view ('admin.indicators.analysis',
			compact(
				'selected_period',
				'selected_pair',
				'periods',
				'pairs',
				'default_values',
				'indicadores_cruzamento_media',
				'j_hc_hist_quotes',
				'j_hc_hist_quotes_media_5',
				'j_hc_hist_quotes_media_12',
				'j_hc_forca_rsi_normal',
				'j_hc_forca_media_5',
				'j_hc_forca_media_12',
				'j_hc_volat_rvi',
				'j_hc_volat_media_rvi',
				'j_hc_rsi_rvi',
				'j_hc_index_volat_avg_afast_ret',
				'j_hc_indice_conv',
				'j_hc_eq_media_volat',
				'j_hc_retornos_normalizado',
				'j_hc_retornos_padrao',
				'j_hc_rv',
				'j_hc_fator',				
				'retornos_periodo_sigla',
				'retornos_periodo_titulo',
				'retornos_desvio_padrao',
				'retornos_desvio_normalizado',
				'retornos_atual_padrao',
				'retornos_atual_normalizado',
				'volat_data',
				'return_analysis',
				'ret_rv',
				'ft_semaforo',
				'forca_tendencia_atual',
				// 'semaforos_todas_as_cotacoes',
				'forca_das_moedas'			
			));        
	}

	public function simulator(Request $request){

        // return StdFunctions::next_business_day($request->ref_date);

		$sim = (object)[
			'ref_date' => '',
			'action' => '',
			'target_quote' => 0,
			'points' => 0,
			'rvi' => 0,
			'rsi' => 0
		];

		$parm_r6 = 14;
		$parm_r7 = 2/($parm_r6+1);

		$start_date = ($request->start_date)? StdFunctions::fmt_dmy_ymd($request->start_date):null;
		$end_date = ($request->end_date)? StdFunctions::fmt_dmy_ymd($request->end_date):null;

        //verify volatility data

		$vol = self::volatility($request->pair,$start_date,$end_date);

		$sim->vol = $vol;

        //classify daily volatility and monthly volatility
		$vol_a = [];
		$vol_a[] = round($vol->vol_d,6);
		$vol_a[] = round($vol->vol_m,6);
		sort($vol_a);

        //choose a random volatility in this band
		$vol_factor = (mt_rand(round($vol_a[0]*1000000),round($vol_a[1]*1000000)))/1000000;

		$sim->vol_factor = $vol_factor;

		$factor_mult = 1;

		if ($request->period =="d"){
			$last_quote_date_raw = $vol->last_d_quote_date;
			if ($vol->rsi_d_ind > 69.99) $factor_mult = -1;
		} else {
			if ($request->period=='w'){
				$last_quote_date_raw = $vol->last_w_quote_date;
				if ($vol->rsi_w_ind > 69.99) $factor_mult = -1;
			} else {
				$last_quote_date_raw = $vol->last_m_quote_date;
				if ($vol->rsi_m_ind > 69.99) $factor_mult = -1;
			}
		}

		if (trim($last_quote_date_raw)=="") return json_encode($sim);

		$last_quote_date = date_create_from_format("d/m/Y",$last_quote_date_raw);
		$next_quote_date = $last_quote_date;

		$last_quote_value = $request->quote;

		$considered_factor = $vol_factor * $factor_mult;

		$last_quotes = Quote::filter_quotes($request->period,$request->pair,$start_date,$end_date)->orderBy('ref_date','desc')->take(14)->get();


		$collect_ganho = collect([]);
		$collect_perda = collect([]);
		$collect_closes_10 = collect([]);

		foreach($last_quotes as $last_quotes_item){

			$collect_ganho->prepend(Quote::quote_to_points($last_quotes_item->ganho,$request->pair));            
			$collect_perda->prepend(Quote::quote_to_points($last_quotes_item->perda,$request->pair));

			$collect_closes_10->prepend(Quote::quote_to_points($last_quotes_item->close,$request->pair));
			if (!isset($last_quote_u)) $last_quote_u = Quote::quote_to_points($last_quotes_item->u,$request->pair);  
			if (!isset($last_quote_d)) $last_quote_d = Quote::quote_to_points($last_quotes_item->d,$request->pair);        
		}

		$ind = 0;
		$rvi = 0;

		while($ind<5000){

			$next_quote_date = StdFunctions::next_business_date($next_quote_date,$request->period);


			$next_quote_value = (1+$considered_factor) * $last_quote_value;
			$collect_closes_10->prepend(($next_quote_value));            

			$ganho = ($next_quote_value > $last_quote_value)? ($next_quote_value - $last_quote_value):0;
			$perda = ($next_quote_value < $last_quote_value)? ($last_quote_value - $next_quote_value):0;

			$collect_ganho->prepend($ganho);
			$collect_perda->prepend($perda);

			$collect_ganho = $collect_ganho->slice(0,14);
			$collect_perda = $collect_perda->slice(0,14);
			$collect_closes_10 = $collect_closes_10->slice(0,10);

			$mg = $collect_ganho->avg();
			$mp = $collect_perda->avg();

			$rs_normal = ($mp > 0)? ($mg / $mp): 0;
			$rsi_normal = (100 - (100 / ($rs_normal + 1)));

			$usd = 0;
			if ($ganho > 0) $usd = Quote::result_precision(Quote::standard_devation($collect_closes_10->all()));            

			$dsd =0;
			if ($perda > 0) $dsd = Quote::result_precision(Quote::standard_devation($collect_closes_10->all()));           

			$last_u = $last_quote_u;                
			$u = Quote::result_precision((($usd - $last_u) * $parm_r7) + $last_u);
			$last_quote_u = $u;

			$last_d = $last_quote_d;
			$d = Quote::result_precision((($dsd - $last_d) * $parm_r7) + $last_d);
			$last_quote_d = $d;

			$rvi = 0;
			if ($d >0) $rvi = Quote::result_precision(100 * ($u/($u + $d)));

            //factor_mult = -1 indicates that actually it's on "sell" and 1 indicates "buy"
			if (($factor_mult == -1 && $rsi_normal < 29.99) ||
				($factor_mult==1 && $rsi_normal > 69.99)) break;

				$last_quote_value = $next_quote_value;

			$ind+=1;            

		}        

		$sim->ref_date = date_format($next_quote_date,"d/m/Y");
		$sim->target_quote = Quote::round_quote($next_quote_value,$request->pair);
		$sim->points = Quote::points_format($sim->target_quote - $request->quote,$request->pair);       
		$sim->rsi_normal = round($rsi_normal);
		$sim->rvi = round($rvi);
		$sim->action = self::status_rsi($rsi_normal);
		$sim->mg = $mg;
		$sim->mp = $mp;

		return json_encode($sim);

	}

	public static function forca_tendencia_atual(){

		$last_d_load = Quote::last_quote_date('d');
		$last_w_load = Quote::last_quote_date('w');
		$last_m_load = Quote::last_quote_date('m');

		$strong_d_quotes = Quote::strong_pairs_on_last_load('d',$last_d_load);
		$strong_w_quotes = Quote::strong_pairs_on_last_load('w',$last_w_load);
		$strong_m_quotes = Quote::strong_pairs_on_last_load('m',$last_m_load);

		$quotes_d = [];
		$quotes_w = [];
		$quotes_m = [];

		foreach($strong_d_quotes as $quote){

			$line = (object)[];
			$line->pair = $quote->pair;
			$line->close = $quote->close;
			$line->t = self::status_rsi($quote->rsi_normal);
			$line->i = round($quote->rsi_normal);
			$line->sinalizador = Quote::check_signals($quote);

			$quotes_d[] = $line;
		}

		foreach($strong_w_quotes as $quote){

			$line = (object)[];
			$line->pair = $quote->pair;
			$line->close = $quote->close;
			$line->t = self::status_rsi($quote->rsi_normal);
			$line->i = round($quote->rsi_normal);
			$line->sinalizador = Quote::check_signals($quote);

			$quotes_w[] = $line;
		}

		foreach($strong_m_quotes as $quote){

			$line = (object)[];
			$line->pair = $quote->pair;
			$line->close = $quote->close;
			$line->t = self::status_rsi($quote->rsi_normal);
			$line->i = round($quote->rsi_normal);
			$line->sinalizador = Quote::check_signals($quote);

			$quotes_m[] = $line;
		}    

		$return = (object)[];

		$return->max_ref_date_d = StdFunctions::fmt_datetime_dmy($last_d_load);
		$return->max_ref_date_w = StdFunctions::fmt_datetime_dmy($last_w_load);
		$return->max_ref_date_m = StdFunctions::fmt_datetime_dmy($last_m_load);

		$return->quotes_d = $quotes_d;
		$return->quotes_w = $quotes_w;
		$return->quotes_m = $quotes_m;

		return $return;

	}

	public static function semaforos_todas_as_cotacoes(Request $request=null){

		$quotes = Quote::last_quotes();

		$quotes_d = [];
		$quotes_w = [];
		$quotes_m = [];

		foreach($quotes->last_d_quotes as $quote){

			$line = (object)[];
			$line->pair = $quote->pair;
			$line->sinalizador = Quote::check_signals($quote);
			$line->ret_rv = self::calc_ret_afast_rv($quote);
			$quotes_d[] = $line;
		}

		foreach($quotes->last_w_quotes as $quote){

			$line = (object)[];
			$line->pair = $quote->pair;
			$line->sinalizador = Quote::check_signals($quote);
			$line->ret_rv = self::calc_ret_afast_rv($quote);
			$quotes_w[] = $line;
		}

		foreach($quotes->last_m_quotes as $quote){

			$line = (object)[];
			$line->pair = $quote->pair;
			$line->sinalizador = Quote::check_signals($quote);
			$line->ret_rv = self::calc_ret_afast_rv($quote);
			$quotes_m[] = $line;
		}        


		$return = (object)[];

		$return->max_ref_date_d = StdFunctions::fmt_datetime_dmy($quotes->last_d_quote_ref_date);
		$return->max_ref_date_w = StdFunctions::fmt_datetime_dmy($quotes->last_w_quote_ref_date);
		$return->max_ref_date_m = StdFunctions::fmt_datetime_dmy($quotes->last_m_quote_ref_date);

		$return->quotes_d = $quotes_d;
		$return->quotes_w = $quotes_w;
		$return->quotes_m = $quotes_m;

		return json_encode($return);

	}

	public static function load_quotes_from_remote_server(){

		// StdFunctions::verify_memory_usage("Antes da carga");

		Log::info("Carga do servidor remoto iniciada: " . date_format(date_create(),"Y-m-d H:i:s"));
		$loading_raw_files = SystemConfig::firstOrCreate(['key'=>'loading_quotes'])->value;
		if ($loading_raw_files=='yes') return;

        //clean folders

		$files = Storage::allFiles("/compressed_last_quotes");
		Storage::delete($files);
		
		$files = Storage::allFiles("/uncompressed_last_quotes");
		Storage::delete($files);        

		$remote_server = env('REMOTE_QUOTES_SERVER');
		$remote_server = "http://35.226.27.254";
		$remote_server = "http://200.98.4.75";
		$remote_server = "http://173.225.102.170:80";

		$remote_server = System_Config::ler('servidor_dados_ip','http://173.225.102.170').
						 ':'.
						 System_Config::ler('servidor_dados_porta','80');
		
		$remote_file = $remote_server . "/last.zip";
		$storage_path = "/compressed_last_quotes/";

		Log::info("Arquivo remoto buscado:".$remote_file);
		
		$zip_file = file_get_contents($remote_file);
		Storage::put($storage_path.'last.zip',$zip_file);

		$target_folder = "/uncompressed_last_quotes/";
		StdFunctions::uncompress_zip($storage_path."last.zip",$target_folder);

		$files = Storage::allFiles($target_folder);

		$files_to_be_loaded = [];

		$files_to_be_loaded_current_info = [];

		foreach($files as $file){

			if (preg_match("/(.)(1440|10080|43200)$/",$file)){                
				$files_to_be_loaded[] = $file;				
			}         

			if (preg_match("/(.)(15|30|60|240|1440)$/",$file)){                
				$files_to_be_loaded_current_info[] = $file;				
			}     			    
		}

		Quote::import_quotes_from_remote_server($files_to_be_loaded);

		$pair_current_info = new Pair_Current_Info;
		$pair_current_info->import_current_info_remote_server($files_to_be_loaded_current_info);

		Cache::forget('forca_das_moedas');
		Log::info("Carga do servidor remoto finalizada: " . date_format(date_create(),"Y-m-d H:i:s"));
		// StdFunctions::verify_memory_usage("Depois da carga");		

	}


	public static function forca_das_moedas(){

		$user = auth()->user();
		if (($user->plano_validade && date_create($user->plano_validade) < date_create()) || $user->ativo != 1) abort(429);
		$user->last_login = date_format(date_create(),"Y-m-d H:i:s");
		$user->save();
		
		return self::calcular_forca_das_moedas();

		return Cache::rememberForever('forca_das_moedas',function(){
			return self::calcular_forca_das_moedas();
		});

	}

	// public static function calcular_forca_das_moedas(){
	public static function calcular_forca_das_moedas(){

		$moedas_principais = ['EUR','GBP','AUD','CAD','CHF','JPY','NZD','USD'];
		$periodos = ['d','w','m'];

		$forca = [];

		$limite_low = SystemConfig::firstOrCreate(['key'=>'currency_power_low'])->value;
		$limite_high = SystemConfig::firstOrCreate(['key'=>'currency_power_high'])->value;

		if ($limite_low==null) $limite_low = 20;
		if ($limite_high==null) $limite_high = 80;

		foreach($moedas_principais as $moeda_base){

			$todos_os_pares = Quote::todos_os_pares_por_moeda($moeda_base, $moedas_principais);
			// if ($moeda_base=='USD') dd($todos_os_pares);

			foreach($todos_os_pares as $par){

				foreach($periodos as $periodo){

					$ultima_cotacao = Quote::last_quote($par,$periodo);

					if (!$ultima_cotacao){
						// $forca[$moeda_base][$periodo] = [];
						continue;
					}

					$range_raw = (float)$ultima_cotacao->maximum - (float)$ultima_cotacao->minimum;

					if ($range_raw ==0){
						$percentual_raw = 0;
					} else {
						$percentual_raw = ((float)$ultima_cotacao->close - (float)$ultima_cotacao->minimum) / ((float)$ultima_cotacao->maximum - (float)$ultima_cotacao->minimum);	
					}

					$retorno = (object)[
						'par' => $par,
						'moeda' => $moeda_base,
						'periodo' => $periodo,
						'percentual_raw' => $percentual_raw,						
						'range_raw' => Quote::points_format($range_raw,$par)
					];

					//$retorno->percentual = StdFunctions::float_to_percentage($retorno->percentual_raw);
					$retorno->percentual = Quote::convert_percent_to_xref($retorno->percentual_raw);
					$retorno->percentual_indicator = Quote::forca_geral_moeda_indicador($retorno->percentual);

					$forca[$moeda_base][$periodo][$par] = $retorno;
				}

			}
		}

		$forca_geral = [];

		Log::info(print_r($forca_geral, true));

		foreach($forca as $moeda_base=>$dados_por_moeda_base){
			foreach($dados_por_moeda_base as $periodo=>$dados_por_periodo){
				$soma_percentual = 0;
				$count_percentual = 0;
				foreach($dados_por_periodo as $par=>$dados_por_par){
					$moeda_pesquisada = str_replace($moeda_base,"",$par);
					if (in_array($moeda_pesquisada,$moedas_principais)){
						if (strpos($par,$moeda_base)==0){
							//$soma_percentual += $forca[$moeda_base][$periodo][$par]->percentual; //teste número
							$soma_percentual += (float)Quote::convert_percent_to_xref($forca[$moeda_base][$periodo][$par]->percentual_raw); 
						} else {
							//$soma_percentual -= $forca[$moeda_base][$periodo][$par]->percentual; // teste número
							$soma_percentual += (float)Quote::convert_percent_to_xref(1 - $forca[$moeda_base][$periodo][$par]->percentual_raw);
						}
						$count_percentual +=1;
					}
				}
				$percentual_calculado = $soma_percentual / $count_percentual;
				Log::info("Percentual calculado $par: " . $percentual_calculado);
				$percentual_indicator = 'neutral';

				/*
				avaliação com base em percentual
				if (($percentual_calculado*100) <= $limite_low){
					$percentual_indicator = 'low';
				} else {
					if (($percentual_calculado * 100) >= $limite_high){
						$percentual_indicator = 'high';
					}
				}				
				*/

				$forca_geral[$periodo][$moeda_base] = (object)[
					'percentual_raw' => $percentual_calculado,
					//'percentual' => StdFunctions::float_to_percentage($percentual_calculado),
					'percentual' => number_format($percentual_calculado,2),
					'percentual_indicator' => Quote::forca_geral_moeda_indicador($percentual_calculado),
					'moeda_base' => $moeda_base,
					'periodo' => $periodo
				];
			}
		}
		
		foreach($forca as $moeda_base => $dados_moeda_base){
			foreach($dados_moeda_base as $periodo => $dados_periodo){
				foreach($dados_periodo as $par => $dados_par){
					$moeda_primaria = substr($par, 0, 3);
					$moeda_secundaria = substr($par, 3, 3);
					$buy = $forca_geral[$periodo][$moeda_primaria]->percentual;
					$sell = $forca_geral[$periodo][$moeda_secundaria]->percentual;
					$forca[$moeda_base][$periodo][$par]->buy = $buy;
					$forca[$moeda_base][$periodo][$par]->sell = $sell;
					$forca[$moeda_base][$periodo][$par]->diff = number_format($buy - $sell, 2);
					$indicators = Quote::forca_geral_buy_sell_indicador($buy, $sell);
					$forca[$moeda_base][$periodo][$par]->buy_indicator = $indicators->buy_class;
					$forca[$moeda_base][$periodo][$par]->sell_indicator = $indicators->sell_class;
				}
			}
		}

		$commodities = [];
		$commodities_pesquisados = ['GOLD','SILVER'];

		foreach($commodities_pesquisados as $comm){					

			foreach($periodos as $periodo){

				$ultima_cotacao = Quote::last_quote($comm,$periodo);

				if (!$ultima_cotacao){
					$commodities[$comm][$periodo] = [];
					continue;
				}

				if ($ultima_cotacao->maximum == $ultima_cotacao->mininum){
					$percentual_raw =0;
				} else {
					$percentual_raw = ((float)$ultima_cotacao->close - (float)$ultima_cotacao->minimum) / ((float)$ultima_cotacao->maximum - (float)$ultima_cotacao->minimum);
				}

				$range_raw = (float)$ultima_cotacao->maximum - (float)$ultima_cotacao->minimum;

				$percentual_indicator = 'neutral';
				if (($percentual_raw*100) <= $limite_low){
					$percentual_indicator = 'low';
				} else {
					if (($percentual_raw * 100) >= $limite_high){
						$percentual_indicator = 'high';
					}
				}

				$retorno = (object)[						
					'commodity' => $comm,
					'periodo' => $periodo,
					'percentual_raw' => $percentual_raw,
					'percentual_indicator' => $percentual_indicator,	
					'range_raw' => Quote::points_format($range_raw,$par)
				];

				$retorno->percentual = StdFunctions::float_to_percentage($retorno->percentual_raw);

				$commodities[$periodo][$comm] = $retorno;
			}

			
		}

		$periodos_tratados = [
			'd' => 'Diário',
			'w' => 'Semanal',
			'm' => 'Mensal'
		];

		$ultima_atualizacao = Quote::max('created_at');
		if ($ultima_atualizacao==null) $ultima_atualizacao = "2019-05-14 11:00:00";		

		$retorno = (object)[
			'forca_por_par' => $forca,
			'forca_geral' => $forca_geral,
			'commodities' => $commodities,
			'moedas_tratadas' => array_keys($forca),
			'periodos_tratados' => $periodos_tratados,
			'ultima_atualizacao' => $ultima_atualizacao
		];

		return json_encode($retorno);

	}	

	public function semaforo_cruzamento_media($pair){

		$pair_current_info = new Pair_Current_Info;
		return response(json_encode($pair_current_info->retrieve_from_cache($pair)),200);

	}

	public function flow_money_csv($moeda, $periodo, $qtd_periodos = 12, $start_date = null, $end_date = null){		
		
		//para cada par, trazer o flow_money
		//montar no array, a data de referencia e o valor do par, usando a data de referencia como indice
		//ao terminar de montar todos os pares, fazer a soma dos valores
		//fazer o dwonload

		$pares = Quote::principais_cruzamentos_moeda($moeda);
		sort($pares);

		$standard_line = ['_data' => ""];
		foreach($pares as $par) $standard_line[$par] = 0;

		$flow_money = [];

		if ($start_date=="null") $start_date = null;
		if ($end_date=="null") $end_date = null;

		$min_date = "2020-05-01";
		$max_date = "1985-01-01";

		foreach($pares as $par){

			$quotes = Quote::filter_quotes($periodo,$par,$start_date,$end_date)->get(); 

			$quotes_min_ref_date = date_format(date_create($quotes->min('ref_date')),"Y-m-d");
			$quotes_max_ref_date = date_format(date_create($quotes->max('ref_date')),"Y-m-d");

			if ($quotes_min_ref_date < $min_date) $min_date = $quotes_min_ref_date;
			if ($quotes_max_ref_date > $max_date) $max_date = $quotes_max_ref_date;

			$serie_de_retorno = QuotesRep::serie_de_retorno_padrao($quotes, $qtd_periodos);				
			$retornos_padrao = $serie_de_retorno->padrao;
			foreach($retornos_padrao as $data => $valor){

				if (!isset($flow_money[$data])) $flow_money[$data] = $standard_line;

				$flow_money[$data]['_data'] = $data;
				$flow_money[$data][$par] = (float)$valor;
				//if (!isset($flow_money[$data]['ztotal'])) $flow_money[$data]['ztotal'] = 0;
				//$flow_money[$data]['ztotal']+= (float)$valor;
			}
		}

		sort($flow_money);

		$file_name = "flow_money_" . 
						$moeda . "_" . 
						$periodo . "_de_" . 
						$min_date . "_ate_" . $max_date . 
						"_" . $qtd_periodos . "_periodos_" . 
						"_extracao_" . date_format(date_create(),"Ymd Hi") . ".csv";


		$headers = [
			'Content-Type' => "text/csv",
			"Content-Disposition" => "attachment; filename=" . $file_name,
			"Pragma" => "no-cache",
			"Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
			"Expires" => "0"
		];

		$cabecalho_csv = $pares;
		array_unshift($cabecalho_csv,"Data");	
		//$cabecalho_csv[] = "Total";

		$callback = function() use ($cabecalho_csv, $flow_money){
			$file = fopen("php://output","w");

			fputcsv($file, $cabecalho_csv);

			foreach($flow_money as $flow_money_line){
				fputcsv($file, $flow_money_line);
			}

			fclose($file);
		};

		return Response::stream($callback, 200, $headers);

	}
}
