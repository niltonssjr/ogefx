<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SystemConfig;
use Illuminate\Support\Facades\Log;

class LeadsController extends Controller
{
	public static function load_leads_from_email(){

		$config = (object)[];

		$config->email_server = SystemConfig::firstOrCreate(['key'=>'email_server'])->value;
		$config->email_account = SystemConfig::firstOrCreate(['key'=>'email_account'])->value;
		$config->email_password = SystemConfig::firstOrCreate(['key'=>'email_password'])->value;
		$config->trial_days = SystemConfig::firstOrCreate(['key'=>'trial_days'])->value;

		// $filtro_email = "crossingtwolines@ogefx.com.br";		
		$filtro_email = "notificacoes@automacaodevendas.com.br";
		$partes = explode('@',$filtro_email);
		$filtro_mailbox = $partes[0];
		$filtro_host = $partes[1]; 

		if (!extension_loaded('imap')){
			Log::error("Protocolo IMAP não instalado, entre em contato com a Augesystems");
			return;
		}

		$mbox = imap_open("{".$config->email_server.":143/novalidate-cert}INBOX", $config->email_account, $config->email_password);
		$errors = imap_errors();
		$alerts = imap_alerts();

		if ($errors && count($errors)>0){
			Log::error("Não foi possível se conectar ao e-mail. Autenticação falhou");
			Log::error($errors[0]);
			return;
		}

		if ($alerts && count($alerts)>0){
			Log::error("Não foi possível se conectar ao e-mail. Autenticação falhou");
			Log::error($alerts[0]);
			return;
		}		

		Log::info("Emails na caixa: " . imap_num_msg($mbox));

		if (imap_num_msg($mbox)==0) return;		

		$emails_selecionados = [];

		for($m = 1; $m <= imap_num_msg($mbox); $m++){

			$header = imap_headerinfo($mbox, $m);	

			if (!isset($header->subject)) continue;

			$subject = $header->subject;
			if ($header->from[0]->mailbox==$filtro_mailbox &&
				$header->from[0]->host==$filtro_host &&
				$header->Unseen == "U" &&
				strpos($subject,'Novo lead cadastrado!')>0){


				$structure = imap_fetchstructure($mbox, $m);			
				
				$body = imap_fetchbody($mbox,$m,1);

				if($structure->encoding == 3) {
					$body = imap_base64($body);
				} else if($structure->encoding == 1) {
					$body = imap_8bit($body);
				} else {
					$body = imap_qprint($body);
				}				

				$email = self::buscar_email($body);				
				$nome = self::buscar_nome($body);				
				$nome = str_replace("=","",$nome);
				//Log::info("***** Um novo lead será cadastrado! Nome=>(".$nome.") Email=>(".$email.") em hexa:(".bin2hex($email).")");				
				\App\Http\Controllers\Admin\UsersController::insert_from_lead($nome,$email, $config->trial_days);
				
				// $emails_selecionados[]=[
				// 	'date' => $header->date,
				// 	'subject' => $header->subject,
				// 	'Unseen' => $header->Unseen,			
				// 	'chamado'=>$this->retorna_variavel("chamado é",$body),
				// 	'nome' => $this->retorna_variavel("Nome:",$body),
				// 	'email' => $this->retorna_variavel("Email:",$body)			
				// ];
			}

		}

		imap_errors();
		imap_close($mbox);

		return;

	}

	private static function buscar_email($local){

		// Log::info("Etapa 0: ".$local);
		$local = str_replace("=","",$local);
		$local = str_replace(hex2bin('0d0a'),"",$local);
		// Log::info("Etapa 1: ".$local);
		$posicao = strpos($local,"E-mail: ");
		if ($posicao==0) return "Não tem email";
		$body = substr($local,$posicao+8);
		// Log::info("Etapa 2: ".$body);
		$posicao_quebra = strpos($body,"<br");
		if ($posicao_quebra>0) $body=substr($body,0,$posicao_quebra);
		// Log::info("Etapa 3: ".$body);
		$posicao_quebra = strpos($body,"Nome:");
		if ($posicao_quebra>0) $body=substr($body,0,$posicao_quebra);
		// Log::info("Etapa 4: ".$body);
		$posicao_quebra = strpos($body,"<mailto");
		if ($posicao_quebra>0) $body=substr($body,0,$posicao_quebra);		
		// Log::info("Etapa 5: ".$body);
		$posicao_quebra = strpos($body," ");
		if ($posicao_quebra>0) $body=substr($body,0,$posicao_quebra);
		// Log::info("Etapa 6: ".$body);
		$posicao_quebra = strpos($body,hex2bin('0d0a'));
		if ($posicao_quebra>0) $body=substr($body,0,$posicao_quebra);		
		// Log::info("Etapa 7: ".$body);
		return $body;

	}

	private static function buscar_nome($local){

		// Log::info("Etapa 0: ".$local);
		$local = str_replace("=","",$local);
		$local = str_replace(hex2bin('0d0a'),"",$local);
		// Log::info("Etapa 1: ".$local);
		$posicao = strpos($local,"Nome: ");
		if ($posicao==0) return "Não tem email";
		$body = substr($local,$posicao+6);
		// Log::info("Etapa 2: ".$body);
		$posicao_quebra = strpos($body,"<br");
		if ($posicao_quebra>0) $body=substr($body,0,$posicao_quebra);
		// Log::info("Etapa 4: ".$body);
		$posicao_quebra = strpos($body,"<mailto");
		if ($posicao_quebra>0) $body=substr($body,0,$posicao_quebra);		
		// Log::info("Etapa 5: ".$body);
		$posicao_quebra = strpos($body," ");
		if ($posicao_quebra>0) $body=substr($body,0,$posicao_quebra);
		// Log::info("Etapa 6: ".$body);
		$posicao_quebra = strpos($body,hex2bin('0d0a'));
		if ($posicao_quebra>0) $body=substr($body,0,$posicao_quebra);		
		// Log::info("Etapa 7: ".$body);
		return $body;

	}
	private static function retorna_variavel($procura,$local){

		$matches = [];
		preg_match("/".$procura."(.*)(\s|<br)/",$local,$matches);
		if (count($matches)==0) return "";

		return trim($matches[1]);

	}	
}
