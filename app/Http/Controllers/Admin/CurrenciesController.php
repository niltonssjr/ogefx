<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\Quote;
use App\StdClasses\StdFunctions;
use DB;

class CurrenciesController extends Controller
{
    public function index(){

        Stdfunctions::allow_admin();        
        StdFunctions::allow_active();
        
        // $root_currencies = Currency::all();
    	$currencies = Currency::orderBy('name')->get();

    	return view ('admin.currencies',compact('currencies'));

    }

    public function destroy(Request $request){

        

        DB::beginTransaction();
        
        $currency = Currency::find($request->id);
        $quotes = Quote::where('pair',$currency->name);
        
        $quotes->delete();
        $currency->delete();

        DB::commit();

        return redirect()->route('currencies');

    }

    public function store (Request $request){

        $this->validate($request,
                            [
                                'currency_name' => 'required|unique:currencies,name'
                            ],
                            [
                                'currency_name.required' => 'Informe o nome do ativo',
                                'currency_name.unique' => 'O ativo já está cadastrado'
                            ]
                        );

        $currency = Currency::create(['name'=>trim(strtoupper($request->currency_name)),'description'=>null]);

        return redirect()->route('currencies');

    }
}
