<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\StdClasses\StdFunctions;
use App\Models\SystemConfig;
use Illuminate\Support\Arr;
use App\User;
use App\Repositories\SystemConfigRepository;
use Illuminate\Support\Facades\Cache;

class SystemSettingsController extends Controller
{

	protected $systemconfig;

	public function __construct(SystemConfigRepository $systemconfig){

		$this->systemconfig = $systemconfig;

	}
	public function index(){

		StdFunctions::allow_admin();

		$config = (object)[
		];

		$config->email_server = $this->systemconfig->ler('email_server');
		$config->email_account = $this->systemconfig->ler('email_account');
		$config->email_password = $this->systemconfig->ler('email_password');
		$config->trial_days = $this->systemconfig->ler('trial_days',10);
		$config->currency_power_low = $this->systemconfig->ler('currency_power_low',20);
		$config->currency_power_high = $this->systemconfig->ler('currency_power_high',80);
		$config->trial_users = User::where('plano_nome','Trial')->count();
		$config->servidor_dados_ip = $this->systemconfig->ler('servidor_dados_ip');
		$config->servidor_dados_porta = $this->systemconfig->ler('servidor_dados_porta','80');

		$forca_percent_pontos_padrao = [3, 10, 25, 40, 50, 60, 75, 90, 97];

		for ($x = 1; $x < 10; $x++){
			$varname = 'forca_percent_pontos_' . $x;
			$config->{$varname} = $this->systemconfig->ler($varname, $forca_percent_pontos_padrao[$x - 1]);
		}

		$config->forca_buy_sell_high_buy = $this->systemconfig->ler('forca_buy_sell_high_buy',6);
		$config->forca_buy_sell_high_sell = $this->systemconfig->ler('forca_buy_sell_high_sell',2);
		$config->forca_buy_sell_low_buy = $this->systemconfig->ler('forca_buy_sell_low_buy',2);
		$config->forca_buy_sell_low_sell = $this->systemconfig->ler('forca_buy_sell_low_sell',6);

		$config->forca_pontos_indicador_orange = $this->systemconfig->ler('forca_pontos_indicador_orange',2);
		$config->forca_pontos_indicador_yellow = $this->systemconfig->ler('forca_pontos_indicador_yellow',5);
		$config->forca_pontos_indicador_green = $this->systemconfig->ler('forca_pontos_indicador_green',7);
		$config->forca_pontos_indicador_red = 0;		

		return view('admin.systemsettings',compact('config'));
	}

	public function store(Request $request){

		$this->validate($request,
			[
				'email_server' => 'required',
				'email_account' => 'required',
				'email_password' => 'required',
				'trial_days' => 'required|gt:0',
				'currency_power_low' => 'required|lt:currency_power_high',
				'currency_power_high' => 'required',
				'servidor_dados_ip' => 'required',
				'servidor_dados_porta' => 'required'
			],
			[
				'email_server.required' => 'Informe o servidor de e-mail que você utiliza',
				'email_account.required' => 'Informe a sua conta de e-mail',
				'email_password.required' => 'Informe a senha de sua caixa de e-mails',
				'trial_days.required' => 'Informe quantos dias quer que o novo lead utilize o sistema',
				'trial_days.gt' => 'Informe quantos dias quer que o novo lead utilize o sistema',
				'currency_power_low.required' => 'Informe o limite inferior da força das moedas',
				'currency_power_low.lt' => 'O limite inferior deve ser menor do que o superior',
				'currency_power_high.required' => 'Informe o limite superior da força das moedas',      
				'servidor_dados_ip.required' => 'Informe o ip do servidor de dados',
				'servidor_dados_porta.required' => 'Informe a porta do servidor de dados',
			]);


		if (env('APP_ENV')=='production'){
			$validate_access = StdFunctions::validate_email_access($request->email_server,$request->email_account,$request->email_password);

			if (!$validate_access){
				return redirect()->back()->withErrors(['Não foi possível se conectar ao e-mail']);
			} 
		}



		$config = (object)[];

		$config->email_server = $this->systemconfig->gravar('email_server',$request->email_server);
		$config->email_account = $this->systemconfig->gravar('email_account',$request->email_account);
		$config->email_password = $this->systemconfig->gravar('email_passowrd',$request->email_password);
		$config->trial_days = $this->systemconfig->gravar('trial_days',$request->trial_days);
		$config->currency_power_low = $this->systemconfig->gravar('currency_power_low',$request->currency_power_low);
		$config->currency_power_high = $this->systemconfig->gravar('currency_power_high',$request->currency_power_high);
		$config->servidor_dados_ip = $this->systemconfig->gravar('servidor_dados_ip',$request->servidor_dados_ip);
		$config->servidor_dados_porta = $this->systemconfig->gravar('servidor_dados_porta',$request->servidor_dados_porta);

		$config->trial_users = User::where('plano_nome','Trial')->count();	

		$forca_percent_pontos = [];
		for ($x = 1; $x < 10; $x++){
			$varname = 'forca_percent_pontos_' . $x;
			$forca_percent_pontos[] = $request->{$varname};
		}

		sort($forca_percent_pontos);	

		for ($x = 1; $x < 10; $x++){
			$varname = 'forca_percent_pontos_' . $x;
			$config->{$varname} = $this->systemconfig->gravar($varname, $forca_percent_pontos[$x-1]);
		}			

		$config->forca_buy_sell_high_buy = $this->systemconfig->gravar('forca_buy_sell_high_buy',$request->forca_buy_sell_high_buy);
		$config->forca_buy_sell_high_sell = $this->systemconfig->gravar('forca_buy_sell_high_sell',$request->forca_buy_sell_high_sell);
		$config->forca_buy_sell_low_buy = $this->systemconfig->gravar('forca_buy_sell_low_buy',$request->forca_buy_sell_low_buy);
		$config->forca_buy_sell_low_sell = $this->systemconfig->gravar('forca_buy_sell_low_sell',$request->forca_buy_sell_low_sell);


		$forca_pontos_indicadores = [
			$request->forca_pontos_indicador_orange,
			$request->forca_pontos_indicador_yellow,
			$request->forca_pontos_indicador_green
		];

		sort($forca_pontos_indicadores);

		$config->forca_pontos_indicador_orange = $this->systemconfig->gravar('forca_pontos_indicador_orange',$forca_pontos_indicadores[0]);
		$config->forca_pontos_indicador_yellow = $this->systemconfig->gravar('forca_pontos_indicador_yellow',$forca_pontos_indicadores[1]);
		$config->forca_pontos_indicador_green = $this->systemconfig->gravar('forca_pontos_indicador_green',$forca_pontos_indicadores[2]);
		$config->forca_pontos_indicador_red = 0;

		Session()->put('success',"Configurações atualizadas");

		Cache::forget('forca_percent_pontos');
		Cache::forget('forca_buy_sell');
		Cache::forget("forca_pontos_indicador");

		return view('admin.systemsettings')->with(compact('config'));

	}
}
