<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class userFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        

        return [
            'user_name' => 'required',
            'user_email' => 'required|unique:users,email,'.$this->user_id,
            'user_plano_nome' => 'required_if:user_nivel,user|not_in:sel',
            'user_plano_validade' => 'required_if:user_nivel,user'
        ];
    }

    public function messages(){

        return[
            'user_name.required' => 'Informe o nome do cliente',
            'user_email.required' => 'Informe o e-mail do cliente',
            'user_email.unique' => 'E-mail já cadastrado',
            'user_plano_nome.required_if' => 'Informe o plano do cliente',
            'user_plano_nome.not_in' => 'Informe o plano do cliente',
            'user_plano_validade.required_if' => 'Informe a data de validade do plano'
        ];
    }
}
