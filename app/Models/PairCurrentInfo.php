<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PairCurrentInfo extends Model
{
	protected $fillable = [
		'pair',
		'ref_date',
		'ind_15_cruzamento_media',
		'ind_30_cruzamento_media',
		'ind_60_cruzamento_media',
		'ind_240_cruzamento_media',
		'ind_1440_cruzamento_media'
	];
}
