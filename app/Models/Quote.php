<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Collection;
use App\StdClasses\CSV_import;
use App\StdClasses\StdFunctions;
use App\Models\Currency;
use DB;
use Log;
use Illuminate\Support\Facades\Cache;
use App\Repositories\SystemConfigRepository as Config;

class Quote extends Model
{
	protected $fillable = [
		'period',
		'pair',
		'ref_date',
		'open',
		'close',
		'minimum',
		'maximum',
		'volume',
		'volume_milimiza',
		'ganho',
		'perda',
		'mg',
		'mp',
		'rs_inv',
		'rsi_14_inv',
		'rsi_normal',
		'rs_normal',
		'pt',
		'usd',
		'dsd',
		'u',
		'd',
		'rvi',
		'mms_ptcci_20',
		'true_range',
		'atr_d1',
		'mean_devation',
		'ciclo_cci_20',
		'mm21',
		'ln',
		'desv_p',
		'f_afast',
		'mvh',
		'emv',
		'media_14_emv',
		'media_exp_5_rsi',
		'media_exp_12_rsi',
		'razao_gp_inv',
		'razao_pg',
		'razao_rsi_rvi',
		'razao_rvi_rsi_inv',
		'created_at',
		'calc_date',
		'close_md5',
		'close_md12',
		'media_exp_5_rvi',
		'media_exp_12_rvi',
		'rv',
		'retorno',
		'close_md_exp_6',
		'close_md_exp_12',
		'close_md_exp_24',
		'afast_ret',
		'retorno_md150',
		'rv_md150',
		'retorno_sdev150',
		'rv_sdev150',
		'num_decimals',
		'distancia_media',
		'relacao_volume',
		'relacao_volume_md150',
		'relacao_volume_sdev150',
		'diferenca_media',		
		'last_rsi_bs_ref_date',
		'last_rsi_bs_rsi_normal',
		'last_rsi_bs_close',
		'last_rvi_bs_ref_date',
		'last_rvi_bs_rvi',
		'last_rvi_bs_close',
		'm_vol_5',
		'm_vol_20'		
	];

	public static function last_quote($pair,$period){

		$last_quote_date = self::where('pair',$pair)->where('period',$period)->max('ref_date');    	

		if(!$last_quote_date) return null;

		$quote = self::where('pair',$pair)->where('period',$period)->where('ref_date',$last_quote_date)->first();

		return $quote;

	}

	public static function quote_by_ref_date($period,$pair,$ref_date){

		$quote = self::where('pair',$pair)
					->where('period',$period)
					->when($ref_date,function($query, $ref_date){
						return $query->where('ref_date',"<=",$ref_date);
					})
					->orderBy('ref_date','desc')->first();

		return $quote;

	}

	public static function last_quote_date($period){

		$last_quote_date = self::where('period',$period)->max('ref_date');     

		if(!$last_quote_date) return null;        

		return $last_quote_date;

	}

	public static function strong_pairs_on_last_load($period,$last_quote_date){

		$quotes = self::where('period',$period)->where('ref_date',$last_quote_date)->where(function($query){
			$query->where('rsi_normal',"<",29.99)
			->orwhere('rsi_normal',">",69.99);
		})->get();

		return $quotes;

	}

	public static function filter_quotes($period = 'd',$pair=null,$minimum_date = null, $maximum_date = null,$maximum_amount_of_quotes = 99999){

        // $maximum_amount_of_quotes = 99999999;

		if (!$minimum_date && !$maximum_date && $maximum_amount_of_quotes==99999){
			switch($period){
				case 'd':
				$maximum_amount_of_quotes = 300;
				break;
				case 'w':
				$maximum_amount_of_quotes = 300;
				break;
				case 'm':
				$maximum_amount_of_quotes = 170;
				break;                                        
			}
		}

		$quotes = self::where('period',$period)
		->when($pair,function($query,$pair){
			return $query->where('pair',$pair);
		})
		->when($minimum_date,function($query,$minimum_date){
			return $query->where('ref_date','>=',$minimum_date);
		})
		->when($maximum_date,function($query,$maximum_date){
			return $query->where('ref_date','<=',$maximum_date);
		})->orderBy('ref_date','desc')
		->take($maximum_amount_of_quotes);

		return $quotes;

	}

	public static function import_quotes_file($folder){

		$files = Storage::allFiles($folder);

		foreach($files as $file){
			self::import_quotes_single_file_with_indicators($file);
		}

	}

	public static function files_storaged(){

		$files = Storage::files("quotes_compressed");
		$fileData = [];
		foreach($files as $file){
			$fileData[] = (object)[
				'fileName' => substr($file,(stripos($file,"/")+1)),
				'fileSize' => ceil(Storage::size($file)/1024) . " kb",
				'fileDate' => date("d/m/Y H:i:s",Storage::lastModified($file))
			];
		}
		return $fileData;
	}

	public static function import_quotes_from_remote_server($paths){

		$automatic_load = true;

		$periods = [
			'1440' => 'd',
			'43200' => 'm',
			'10080' => 'w'
		];                

		$valid_periods = array_keys($periods);

		$valid_currencies = Currency::pluck('name')->toArray();

		$array_exclusoes = [];

		$records_to_include_total = [];


		foreach($paths as $path){
			
			$pos = strrpos($path,"/");

			if ($pos !==false){
				$file =  substr($path,($pos+1));
			} else {
				$file = $path;
			}

			$pair = str_replace($valid_periods,"",$file);
			$suffix = str_replace($pair,"",$file);

			if (!in_array($suffix,$valid_periods)) continue;    

			$validation_pair = $pair;      			

			if (!in_array($validation_pair,$valid_currencies)) continue;			

			$period = $periods[$suffix];

			$file = Storage::get($path);

			$quotes = StdFunctions::csv_to_array($file);

			$ref_date = str_replace(".","-",$quotes[0][0]);        


			$last_quote_date = null;

			$lines_created = 0;

			$quote = reset($quotes);

			$records = [];
			$last_date = null;			

			$ref_date = str_replace(".","-",$quote[0]);

			$open = $quote[1];
			$minimum = $quote[3];
			$maximum = $quote[2];
			$close = $quote[4];
			$volume = $quote[5];

			$new_line = (object)[];
			$new_line->id = 0;
			$new_line->period = $period;
			$new_line->pair = $pair;
			$new_line->ref_date = $ref_date;
			$new_line->open = $open;
			$new_line->minimum = $minimum;
			$new_line->maximum = $maximum;
			$new_line->close = $close;
			$new_line->volume = $volume;
			$new_line->volume_milimiza = 0;
			$new_line->ganho = 0;
			$new_line->perda = 0;
			$new_line->mg = 0;
			$new_line->mp = 0;
			$new_line->rs_inv = 0;
			$new_line->rsi_14_inv = 0;
			$new_line->rs_normal = 0;
			$new_line->rsi_normal = 0;
			$new_line->pt = 0;
			$new_line->usd = 0;            
			$new_line->dsd = 0;                                 
			$new_line->u = 0;
			$new_line->d = 0;
			$new_line->rvi = 0;            
			$new_line->mms_ptcci_20 = 0;
			$new_line->true_range = 0;            
			$new_line->atr_d1 = 0;
			$new_line->mean_devation = 0;            
			$new_line->ciclo_cci_20 = 0;
			$new_line->mm21 = 0;
			$new_line->ln = 0;
			$new_line->desv_p = 0;
			$new_line->f_afast = 0;            
			$new_line->mvh = 0;
			$new_line->emv = 0;
			$new_line->media_14_emv = 0;            
			$new_line->media_exp_5_rsi = 0;            
			$new_line->media_exp_12_rsi = 0;
			$new_line->razao_gp_inv = 0;
			$new_line->razao_pg = 0;
			$new_line->razao_rsi_rvi = 0;
			$new_line->razao_rvi_rsi_inv =0;
			$new_line->calc_date = null;

			$records[] = $new_line;
			$last_date = $new_line->ref_date;

			if (count($records)==0) continue;    

			$new_records = collect($records);			
			
			$last_quotes_calculated = Cache::remember($pair."-".$period."-".$last_date,1440,function() use ($pair, $period, $last_date){
				return Quote::where('pair',$pair)
				->where('period',$period)
				->where('ref_date','<',$last_date)
				->where('calc_date','!=',null)
				->orderBy('ref_date','desc')
				->select('open','close','maximum','minimum','ref_date','volume','pair','calc_date')
				->take(150)
				->get();          				
			});
    

			$quotes_to_calc = $new_records->concat($last_quotes_calculated);     
			$quotes_to_calc_sorted = $quotes_to_calc->sortBy('ref_date');   

			unset($new_records);
			unset($last_quotes_calculated);
			unset($quotes_to_calc);                        

			$records_to_include = self::calculate_indicators($quotes_to_calc_sorted);
			
			$records_to_include_total[] = $records_to_include[0];

		}

		if (count($records_to_include_total)>0){			

			foreach($records_to_include_total as $rec_to_inc){
				
				$upd_quote = Quote::updateOrCreate(
								[	
									'pair'=>$rec_to_inc['pair'],
									'period' => $rec_to_inc['period'],
									'ref_date' => $rec_to_inc['ref_date']
								],
								$rec_to_inc
							);
			}

		}
		// DB::table('quotes')->insert($records_to_include_total);

		Cache::forget('forca_das_moedas');

	}


	public static function import_quotes_single_file_with_indicators($path, $automatic_load=false){       

		$pos = strrpos($path,"/");

		if ($pos !==false){
			$file =  substr($path,($pos+1));
		} else {
			$file = $path;
		}

		$periods = [
			'1440' => 'd',
			'43200' => 'm',
			'10080' => 'w'
		];                

		$valid_periods = array_keys($periods);

		$pair = str_replace($valid_periods,"",$file);
		$suffix = str_replace($pair,"",$file);   


		if (!in_array($suffix,$valid_periods)) return;    

		$validation_pair = $pair;      

		$valid_currency = Currency::whereRaw("UPPER(TRIM(name)) = '".strtoupper(trim($validation_pair))."'")->first();


		if (!$valid_currency) return;    

		$period = $periods[$suffix];

		if (preg_match("/^#(.)*(K|M)[0-9]{1}$/",$pair) && !$automatic_load){        
			$commodity = substr($pair,0,strlen($pair)-2);            
			self::remove_commodity($commodity,$period);            
		}



		$file = Storage::get($path);

		$quotes = StdFunctions::csv_to_array($file);

		rsort($quotes);

		if ($automatic_load){
			$ref_date = str_replace(".","-",$quotes[0][0]);        
			self::where("pair",$pair)->where("period",$period)->where("ref_date","=",$ref_date)->delete();
		}



		$last_quote_date = null;

		$lines_created = 0;

		$quote = reset($quotes);

		$records = [];
		$last_date = null;

		while($quote){      

			$ref_date = str_replace(".","-",$quote[0]);

			if ($last_quote_date !=null && $ref_date <= $last_quote_date){            
				break;
			}

			if (!isset($quote[1])) break;

			$open = $quote[1];
			$minimum = $quote[3];
			$maximum = $quote[2];
			$close = $quote[4];
			$volume = $quote[5];

			$new_line = (object)[];
			$new_line->id = 0;
			$new_line->period = $period;
			$new_line->pair = $pair;
			$new_line->ref_date = $ref_date;
			$new_line->open = $open;
			$new_line->minimum = $minimum;
			$new_line->maximum = $maximum;
			$new_line->close = $close;
			$new_line->volume = $volume;
			$new_line->volume_milimiza = 0;
			$new_line->ganho = 0;
			$new_line->perda = 0;
			$new_line->mg = 0;
			$new_line->mp = 0;
			$new_line->rs_inv = 0;
			$new_line->rsi_14_inv = 0;
			$new_line->rs_normal = 0;
			$new_line->rsi_normal = 0;
			$new_line->pt = 0;
			$new_line->usd = 0;            
			$new_line->dsd = 0;                                 
			$new_line->u = 0;
			$new_line->d = 0;
			$new_line->rvi = 0;            
			$new_line->mms_ptcci_20 = 0;
			$new_line->true_range = 0;            
			$new_line->atr_d1 = 0;
			$new_line->mean_devation = 0;            
			$new_line->ciclo_cci_20 = 0;
			$new_line->mm21 = 0;
			$new_line->ln = 0;
			$new_line->desv_p = 0;
			$new_line->f_afast = 0;            
			$new_line->mvh = 0;
			$new_line->emv = 0;
			$new_line->media_14_emv = 0;            
			$new_line->media_exp_5_rsi = 0;            
			$new_line->media_exp_12_rsi = 0;
			$new_line->razao_gp_inv = 0;
			$new_line->razao_pg = 0;
			$new_line->razao_rsi_rvi = 0;
			$new_line->razao_rvi_rsi_inv =0;
			$new_line->calc_date = null;

			$records[] = $new_line;
			$last_date = $new_line->ref_date;
			$quote = next($quotes);

		}

		if (count($records)==0) return;    

		$new_records = collect($records);

		if ($automatic_load){
        //em caso de carga automática, busca os últimos registros já calculados
			$last_quotes_calculated = Quote::where('pair',$pair)
			->where('period',$period)
			->where('calc_date','!=',null)
			->orderBy('ref_date','desc')			
			->take(150)
			->get();          
		} else {
        //caso contrário, força um objeto vazio
			$last_quotes_calculated = Quote::where('pair',"xptoy")
			->get();                  
		}

		$quotes_to_calc = $new_records->concat($last_quotes_calculated);     
		$quotes_to_calc_sorted = $quotes_to_calc->sortBy('ref_date');   

		unset($new_records);
		unset($last_quotes_calculated);
		unset($quotes_to_calc);                        

		$records_to_include = self::calculate_indicators($quotes_to_calc_sorted);

		$chunks = array_chunk($records_to_include,200);

		if (!$automatic_load){
        //se não for carga automática, apaga o que tem no banco para o par / período
			Quote::where('pair',$pair)
			->where('period',$period)        
			->delete();  
		}

		foreach($chunks as $chunk){
			DB::table('quotes')->insert($chunk);    
		}      

		Cache::forget('forca_das_moedas');

	}

	private static function calculate_indicators($quotes_to_calc_sorted){

		$parm_r6 = 14;
		$parm_r7 = 2/($parm_r6+1);

		$parm_ah7 = 5;
		$parm_ah6 = 2/($parm_ah7+1);
		$parm_ai7 = 12;
		$parm_ai6 = 2/($parm_ai7+1);  

		$loop_index = 0;

		$collect_closes = collect([]);
		$collect_gains = collect([]);
		$collect_loses = collect([]);
		$collect_pt = collect([]);
		$collect_ln = collect([]);
		$collect_desv_p = collect([]);
		$collect_emv = collect([]);
		$collect_quotes = collect([]);
		$collect_retorno = collect([]);
		$collect_afast_ret = collect([]);
		$collect_rv = collect([]);
		$collect_relacao_volume = collect([]);    
		$collect_volat = collect([]);

		$buy_sell_rvi = (object)[
			'ref_date' => "",
			'rvi' => "",
			'close' => "",
		];		

		$buy_sell_rsi = (object)[
			'ref_date' => "",
			'rsi_normal' => "",
			'close' => "",
		];	

		$records_to_include = [];

		foreach($quotes_to_calc_sorted as $quote){

			$quote->num_decimals = StdFunctions::check_precision_from_quote($quote);

			$gain = 0;
			$lose = 0;

			if (isset($last_quote)){ if ($last_quote->close < $quote->close){ $gain = self::result_precision($quote->close - $last_quote->close);}}
			if (isset($last_quote)) { if ($last_quote->close > $quote->close){ $lose = self::result_precision($last_quote->close - $quote->close);}}

			$collect_gains->prepend($gain);
			$collect_gains = $collect_gains->slice(0,14);
			$collect_loses->prepend($lose);
			$collect_loses = $collect_loses->slice(0,14);
			$collect_closes->prepend($quote->close);
			$collect_closes = $collect_closes->slice(0,21);
			$collect_closes_10 = $collect_closes->slice(0,10);

			$volume_milimiza = (substr($quote->pair,3)=="JPY")? self::result_precision($quote->volume / 1000):self::result_precision($quote->volume / 100000);
			$pt = self::result_precision(($quote->maximum + $quote->minimum + $quote->close) / 3);
			$collect_pt->prepend($pt);
			$collect_pt = $collect_pt->slice(0,20);
			$ln = 0;            
			if (isset($last_quote)) $ln = self::result_precision(log($quote->close / $last_quote->close));
			$collect_ln->prepend($ln);
			$collect_ln = $collect_ln->slice(0,21);
			$desv_p = self::result_precision(self::standard_devation($collect_ln->all()) * sqrt(252));
			$collect_desv_p->prepend($desv_p);
			$collect_desv_p = $collect_desv_p->slice(0,21);
			$last_quote_minimum = (isset($last_quote))? $last_quote->minimum : 0;
			$last_quote_maximum = (isset($last_quote))? $last_quote->maximum : 0;
			$emv = 0;
			if ((($quote->maximum - $quote->minimum))!=0)
				$emv = self::result_precision(((($quote->minimum + $quote->maximum)/2) - (($last_quote_minimum + $last_quote_maximum)/2))/($volume_milimiza/($quote->maximum - $quote->minimum)));
			$collect_emv->prepend($emv);
			$collect_emv = $collect_emv->slice(0,14);  


			$quote->volume_milimiza = $volume_milimiza;
			$quote->ganho = $gain;
			$quote->perda = $lose;
			$quote->mg = self::result_precision($collect_gains->avg());

			if ($quote->mg == 0 && isset($last_quote)){
				if ($last_quote->mg != 0) $quote->mg = $last_quote->mg;
			}

			$quote->mp = self::result_precision($collect_loses->avg());

			if ($quote->mp == 0 && isset($last_quote)){
				if ($last_quote->mp != 0) $quote->mp = $last_quote->mp;
			}

			$quote->rs_inv = ($quote->mg > 0)? self::result_precision(($quote->mp / $quote->mg)) : 0;
			$quote->rsi_14_inv = self::result_precision(100 - (100 / ($quote->rs_inv + 1)));
			$quote->rs_normal = ($quote->mp > 0)? self::result_precision(($quote->mg / $quote->mp)) : 0;
			$quote->rsi_normal = self::result_precision(100 - (100 / ($quote->rs_normal + 1)));
			$quote->pt = $pt;
			$quote->usd =0;
			if ($gain > 0) $quote->usd = self::result_precision(self::standard_devation($collect_closes_10->all()));
			$quote->dsd =0;
			if ($lose > 0) $quote->dsd = self::result_precision(self::standard_devation($collect_closes_10->all()));
			$last_u = 0;
			if (isset($last_quote)) $last_u = $last_quote->u;
			$quote->u = self::result_precision((($quote->usd - $last_u)*$parm_r7)+$last_u);
			$last_d = 0;
			if (isset($last_quote)) $last_d = $last_quote->d;
			$quote->d = self::result_precision((($quote->dsd - $last_d) * $parm_r7) + $last_d);
			$quote->rvi = 0;
			if ($quote->d >0) $quote->rvi = self::result_precision(100 * ($quote->u/($quote->u + $quote->d)));
			$quote->mms_ptcci_20 = self::result_precision($collect_pt->avg());
			$quote->true_range = $quote->maximum - $quote->minimum;
			$last_quote_true_range = (isset($last_quote))? $last_quote->true_range : 0;
			$quote->atr_d1 = self::result_precision((($quote->true_range + $last_quote_true_range)/2)/$quote->open);
			$quote->mean_devation = self::result_precision(self::mean_devation($collect_pt,$quote->mms_ptcci_20));
			if ($quote->mean_devation==0) $quote->mean_devation = 1;
			$quote->ciclo_cci_20 = self::result_precision(($quote->pt - $quote->mms_ptcci_20)/ (0.015 * $quote->mean_devation));
			$quote->mm21 = self::result_precision($collect_closes->avg());
			$quote->ln = $ln;
			$quote->desv_p = $desv_p;
			$quote->f_afast =0;
			if ($quote->desv_p !=0) $quote->f_afast = self::result_precision((($quote->close / $quote->mm21)-1)/$quote->desv_p);
			$quote->mvh = self::result_precision($collect_desv_p->avg());
			$quote->emv = $emv;
			$quote->media_14_emv = self::result_precision($collect_emv->avg());

			$last_media_exp_5_rsi = (isset($last_quote))? $last_quote->media_exp_5_rsi : 0;
			$quote->media_exp_5_rsi = self::result_precision((($quote->rsi_14_inv - $last_media_exp_5_rsi) * $parm_ah6) + $last_media_exp_5_rsi);

			$last_media_exp_12_rsi = (isset($last_quote))? $last_quote->media_exp_12_rsi : 0;
			$quote->media_exp_12_rsi = self::result_precision((($quote->rsi_14_inv - $last_media_exp_12_rsi) * $parm_ai6) + $last_media_exp_12_rsi); 

			$last_media_exp_5_rvi = (isset($last_quote))? $last_quote->media_exp_5_rvi : 0;
			$quote->media_exp_5_rvi = self::result_precision((($quote->rvi - $last_media_exp_5_rvi) * $parm_ah6) + $last_media_exp_5_rvi);

			$last_media_exp_12_rvi = (isset($last_quote))? $last_quote->media_exp_12_rvi : 0;
			$quote->media_exp_12_rvi = self::result_precision((($quote->rvi - $last_media_exp_12_rvi) * $parm_ai6) + $last_media_exp_12_rvi); 

			$last_close_md5 = (isset($last_quote))? $last_quote->close_md5 : $quote->close;
			$quote->close_md5 = self::result_precision((($quote->close - $last_close_md5) * $parm_ah6) + $last_close_md5);

			$last_close_md12 = (isset($last_quote))? $last_quote->close_md12 : $quote->close;
			$quote->close_md12 = self::result_precision((($quote->close - $last_close_md12) * $parm_ai6) + $last_close_md12);

			$quote->razao_gp_inv = ($quote->mp >0)? self::result_precision($quote->mg / $quote->mp) : 0;
			$quote->razao_pg = ($quote->mg > 0)? self::result_precision($quote->mp / $quote->mg) : 0;
			$quote->razao_rsi_rvi = 0;
			if ($quote->rsi_14_inv!=0) $quote->razao_rsi_rvi = self::result_precision((($quote->rsi_normal/$quote->rsi_14_inv)>3)? 3 :($quote->rsi_normal/$quote->rsi_14_inv));
			$quote->razao_rvi_rsi_inv =0;
			if ($quote->rsi_14_inv!=0) $quote->razao_rvi_rsi_inv = self::result_precision($quote->rvi / $quote->rsi_14_inv);   

			$quote->retorno = ($quote->open !=0) ? ($quote->close - $quote->open) / $quote->open : 0;
			$quote->afast_ret = $quote->f_afast - $quote->retorno;
			$quote->rv = ($quote->f_afast - $quote->afast_ret) * 16;

			$collect_retorno->prepend($quote->retorno);
			$collect_retorno = $collect_retorno->slice(0,150);

			$collect_afast_ret->prepend($quote->afast_ret);
			$collect_afast_ret = $collect_afast_ret->slice(0,150);

			$collect_rv->prepend($quote->rv);
			$collect_rv = $collect_rv->slice(0,150);

			$quote->retorno_md150 = self::result_precision($collect_retorno->avg());
			$quote->rv_md150 = self::result_precision($collect_rv->avg());
			$quote->retorno_sdev150 = self::result_precision(self::standard_devation($collect_retorno->all()));
			$quote->rv_sdev150 = self::result_precision(self::standard_devation($collect_rv->all()));

			$last_close_md_exp_6 = (isset($last_quote))? $last_quote->close_md_exp_6 : $quote->close;
			$quote->close_md_exp_6 = self::result_precision((($quote->close - $last_close_md_exp_6) * (2/(1+6))) + $last_close_md_exp_6);                

			$last_close_md_exp_12 = (isset($last_quote))? $last_quote->close_md_exp_12 : $quote->close;
			$quote->close_md_exp_12 = self::result_precision((($quote->close - $last_close_md_exp_12) * (2/(1+12))) + $last_close_md_exp_12);    

			$last_close_md_exp_24 = (isset($last_quote))? $last_quote->close_md_exp_24 : $quote->close;
			$quote->close_md_exp_24 = self::result_precision((($quote->close - $last_close_md_exp_24) * (2/(1+24))) + $last_close_md_exp_24);

			$quote->distancia_media = self::result_precision(($quote->close / $quote->mm21) - 1);
			$mvh = ($quote->mvh>0)? $quote->mvh:1;
			$quote->relacao_volume = pow(pow(($quote->distancia_media / $mvh),2),0.5);
			$collect_relacao_volume->prepend($quote->relacao_volume);
			$collect_relacao_volume = $collect_relacao_volume->slice(0,150);
			$quote->relacao_volume_md150 = self::result_precision($collect_relacao_volume->avg());
			$quote->relacao_volume_sdev150 = self::result_precision(self::standard_devation($collect_relacao_volume->all()));
			$quote->diferenca_media = self::result_precision($quote->close - $quote->mm21);

			$collect_volat->prepend($quote->atr_d1);
			$quote->m_vol_5 = $collect_volat->slice(0,5)->avg();
			$quote->m_vol_20 = $collect_volat->slice(0,20)->avg();

			//verifica se o RVI está dentro dos intervalos para cálculo da volatilidade, do contrário copia o anterior
			if ($quote->rvi < 29.99 || $quote->rvi > 69.99){
				$buy_sell_rvi->ref_date = $quote->ref_date;
				$buy_sell_rvi->rvi = $quote->rvi;
				$buy_sell_rvi->close = $quote->close;               				
			}			

			$quote->last_rvi_bs_ref_date = $buy_sell_rvi->ref_date;
			$quote->last_rvi_bs_rvi = $buy_sell_rvi->rvi;
			$quote->last_rvi_bs_close = $buy_sell_rvi->close;		

			//verifica se o RSI_normal está dentro dos intervalos para cálculo da volatilidade, do contrário copia o anterior
			if ($quote->rsi_normal < 29.99 || $quote->rsi_normal > 69.99){
				$buy_sell_rsi->ref_date = $quote->ref_date;
				$buy_sell_rsi->rsi_normal = $quote->rsi_normal;
				$buy_sell_rsi->close = $quote->close;               				
			}			

			$quote->last_rsi_bs_ref_date = $buy_sell_rsi->ref_date;
			$quote->last_rsi_bs_rsi_normal = $buy_sell_rsi->rsi_normal;
			$quote->last_rsi_bs_close = $buy_sell_rsi->close;


			if ($quote->calc_date == null){     
				$quote->calc_date = date_create();
				$quote->created_at = date_create();
				$records_to_include[] = (array)$quote; 
			}           

			$last_quote = $quote;
		}    
		return $records_to_include;
	}

	private static function remove_commodity($commodity,$period){
		$sql = "delete from quotes where pair like '{$commodity}%' and period = '{$period}'";
		DB::statement($sql);
		return;
	}    

	public static function result_precision($value){
		return round($value,6);
	}

	public static function standard_devation($array){

		if (count($array)<2) return 0;

        //calculate the average
		$avg = array_sum($array)/count($array);

        //sum the absolute distance between numbers and average raised to the power of 2
		$distance_sum = 0;

		foreach($array as $a){
			$distance_sum += pow(abs($a - $avg),2);
		}

        //divide by the numer of items
		$distance_sum_avg = $distance_sum / count($array);

        //return sqrt
		return sqrt($distance_sum_avg);

	}

	public static function currencies(){

		$sql = "select distinct pair from quotes order by pair";
		$records = DB::select($sql);

		return $records;

	}

	private static function mean_devation($array_pt,$pt_avg){

		if (count($array_pt)<14) return $pt_avg;

		$soma = 0;

		for ($x=0;$x<count($array_pt);$x++){
			$soma+=ABS($pt_avg - $array_pt[$x]);
		}

		return $soma/20;
        // $res = (ABS($pt_avg-$array_pt[7])+
        //         (ABS($pt_avg-$array_pt[8])+
        //         (ABS($pt_avg-$array_pt[9])+
        //         (ABS($pt_avg-$array_pt[10])+
        //         (ABS($pt_avg-$array_pt[10])+
        //         (ABS($pt_avg-$array_pt[11])+
        //         (ABS($pt_avg-$array_pt[12])+
        //         (ABS($pt_avg-$array_pt[13])+
        //         ABS($pt_avg-$array_pt[12])+
        //         ABS($pt_avg-$array_pt[11])+
        //         ABS($pt_avg-$array_pt[10])+
        //         ABS($pt_avg-$array_pt[9])+
        //         ABS($pt_avg-$array_pt[8])+
        //         ABS($pt_avg-$array_pt[7])+
        //         ABS($pt_avg-$array_pt[6])+
        //         ABS($pt_avg-$array_pt[5])+
        //         ABS($pt_avg-$array_pt[4])+
        //         ABS($pt_avg-$array_pt[3])+
        //         ABS($pt_avg-$array_pt[2])+
        //         ABS($pt_avg-$array_pt[1])+
        //         ABS($pt_avg-$array_pt[0]))
        //         /20)))))));

		return $res;
	}

	public static function stats($period = "d"){

		$currencies = self::currencies();

		$stats = [];

		foreach($currencies as $currency){
			$last_quote = self::last_quote($currency->pair,$period);
			if ($last_quote) $stats[] = self::last_quote($currency->pair,$period);
		}

		return $stats;

	}

	public static function return_analysis($period='d',$pair,$start_date = null,$end_date=null,$max_rec = 12){

		$an = (object)[
			'first_ref_date' => null,
			'last_ref_date' => null,
			'first_ref_date_raw' => null,
			'last_ref_date_raw' => null,
			'pair' => $pair,
			'period' => $period,
			'first_quote' => 0,
			'last_quote' => 0,
			'gain' => 0,
			'periods' => [],
			'summed' => [],
			'quotes' => [],
			'ref_date' => []
		];

		$an_quotes = self::filter_quotes($period,$pair,$start_date,$end_date,$max_rec)->get();        

		if (count($an_quotes)==0) return $an;

		$first_date = $an_quotes->min('ref_date');        
		$last_date = $an_quotes->max('ref_date');

		$last_quote = self::quote_to_points($an_quotes[0]->close,$pair);
		$first_quote = self::quote_to_points($an_quotes[count($an_quotes)-1]->open,$pair);
		$gain = StdFunctions::float_to_percentage_color(($last_quote / $first_quote) -1);

		$an = (object)[
			'first_ref_date' => date_format(date_create($first_date),'d/m/Y'),
			'last_ref_date' => date_format(date_create($last_date),'d/m/Y'),
			'first_ref_date_raw' => $first_date,
			'last_ref_date_raw' => $last_date,
			'pair' => $pair,
			'period' => $period,
			'first_quote' => $first_quote,
			'last_quote' => $last_quote,
			'gain' => $gain ,
			'periods' => [],
			'summed' => [],
			'quotes' => [],
			'ref_date' => []
		];

		$previous_quote = 0;
		$ind = 0;

		foreach($an_quotes as $quote){

			$close_value = self::quote_to_points($quote->close,$pair);
			$open_value = self::quote_to_points($quote->open,$pair);


			if ($ind>0){
				$dif = StdFunctions::float_to_percentage_color(($previous_close / $open_value)-1);
				$summed = StdFunctions::float_to_percentage_color(($last_quote / $open_value)-1);
				array_push($an->periods,$dif);
				array_push($an->summed,$summed);
			}


			array_push($an->quotes,$close_value);
			array_push($an->ref_date,StdFunctions::fmt_datetime_dmy($quote->ref_date));

			$previous_close = $close_value;
			$previous_open = $open_value;
			$ind+=1;
		}
		array_pop($an->quotes);
		array_pop($an->ref_date);

		return $an;

	}

	public static function quote_to_points($quote,$pair){

		return round($quote,5);

        // $new_quote = 0;

        // if (strpos($pair,'JPY')!==false){
        //     $new_quote = round($quote,2);
        // } else {
        //     $new_quote = (float)$quote * 100000;
        // }

        // return $new_quote;

	}   

	public static function quote_to_points_range($quote,$pair){

		$new_quote = 0;

		if (strpos($pair,'JPY')!==false){
			$new_quote = round((float)$quote * 1000,2);
		} else {
			$new_quote = round((float)$quote * 10000,0);
		}

		return $new_quote;

	}   
	public static function check_quote_precision($quote){

		$precisions = [];
		$precisions[] = StdFunctions::check_precision($quote->open);
		$precisions[] = StdFunctions::check_precision($quote->close);
		$precisions[] = StdFunctions::check_precision($quote->minimum);
		$precisions[] = StdFunctions::check_precision($quote->maximum);

		arsort($precisions);

		return $precisions[0];

	}

	public static function points_format($quote,$pair){


		$factors = [
			'BTCUSD' => 100,
			'BCHUSD' => 100,
			'ETHUSD' => 100,
			'LTCUSD' => 100,
			'#Coffee' => 100,
			'#Corn' => 100,
			'#Sugar' => 100,
			'#Wheat' => 100,
			'GOLD' => 100,
			'SILVER' => 1000,
			'JPY' => 1000,
			'XRPUSD' => 10000
		];


		if (strpos($pair,'JPY')!==false){
			$pair = 'JPY';
		} else {
			if (preg_match("/^[#]{1}[A-Za-z0-9_]{1,8}[_]{1}[A-Za-z]{1}[0-9]{1}$/",$pair)){
				$pair = substr($pair,0,strpos($pair,"_"));
			}
		}

		$factor = (isset($factors[$pair]))? $factors[$pair] : 100000;
        // return $quote;
		return round($quote * $factor);

	} 

	public static function round_quote($quote,$pair){

		$new_quote = 0;

		if (strpos($pair,'JPY')!==false){
			$new_quote = round($quote,2);
		} else {
			$new_quote = round($quote,5);
		}

		return $new_quote;

	}

	public static function format_quote_value($value,$precision){
		return number_format($value,$precision,".","");
	}

	public static function check_signals($quote){

		$status = [
			'up' => "<i class='glyphicon glyphicon-circle-arrow-up' style='color:green;font-size:100%'></i>",
			'neutral' => "<i class='glyphicon glyphicon-circle' style='color:orange;font-size:100%'></i>",
			'down' => "<i class='glyphicon glyphicon-circle-arrow-down' style='color:red;font-size:100%'></i>"
		];

		$return = "";

		if ($quote->close_md_exp_6 > $quote->close_md_exp_12){
			$return.= $status['up'];
		} else {
			$return.= $status['down'];
		}

		if ($quote->close_md_exp_6 > $quote->close_md_exp_24){
			$return.= $status['up'];
		} else {
			$return.= $status['down'];
		}

		if ($quote->close_md_exp_12 > $quote->close_md_exp_24){
			$return.= $status['up'];
		} else {
			$return.= $status['down'];
		}   

		return $return;     

	}

	public static function get_pair_precision($pair){

		return self::where('pair',$pair)->max('num_decimals');

	}

	public static function last_quotes(){

		$ret = (object)[
			'last_d_quotes' => [],
			'last_w_quotes' => [],
			'last_m_quotes' => [],
			'last_d_quote_ref_date' => "",
			'last_w_quote_ref_date' => "",
			'last_m_quote_ref_date' => "",
		];

		$ret->last_d_quote_ref_date = self::last_quote_date("d");
		$ret->last_w_quote_ref_date = self::last_quote_date("w");
		$ret->last_m_quote_ref_date = self::last_quote_date("m");

		$ret->last_d_quotes = self::filter_quotes("d",null,$ret->last_d_quote_ref_date,$ret->last_d_quote_ref_date)->orderBy('pair')->get();
		$ret->last_w_quotes = self::filter_quotes("w",null,$ret->last_w_quote_ref_date,$ret->last_w_quote_ref_date)->orderBy('pair')->get();
		$ret->last_m_quotes = self::filter_quotes("m",null,$ret->last_m_quote_ref_date,$ret->last_m_quote_ref_date)->orderBy('pair')->get();

		return $ret;

	}

	public static function ln_series($period,$pair,$minimum_date,$maximum_date){

		$desvio = (object)[
			'padrao' => (object) [
				'd_3' => 0,
				'd_2' => 0,
				'd_1' => 0,
				'd0' => 0,
				'd1' => 0,
				'd2' => 0,
				'd3' => 0,
			],
			'normalizado' => (object) [
				'd_3' => 0,
				'd_2' => 0,
				'd_1' => 0,
				'd0' => 0,
				'd1' => 0,
				'd2' => 0,
				'd3' => 0,
			]
		];

		$periodos = 12;
		$prefixo_sigla = "CR".$periodos;
		$a_texto_periodo_red = ['d'=>"D",'w'=>'S','m'=>'M'];
		$a_texto_periodo_ext = ['d'=>'DIAS','w'=>'SEMANAS','m'=>'MESES' ];
		$periodo_sigla = $prefixo_sigla . $a_texto_periodo_red[$period];
		$periodo_titulo = $periodos . " " . $a_texto_periodo_ext[$period];

		$quotes = self::filter_quotes($period,$pair,null,null,500)->get();

		$quotes = $quotes->sortBy('ref_date');
		$quotes = $quotes->values()->all();

		$col_ln_padrao = collect([]);
		$a_wk_ln_padrao = [];
		$max_sum_ln = -99999;
		$min_sum_ln = 99999;

		foreach($quotes as $quote){
			$col_ln_padrao->prepend($quote->ln);
			$col_ln_padrao = $col_ln_padrao->slice(0,$periodos);
			if ($quote->ref_date >= $minimum_date && $quote->ref_date <= $maximum_date){
				$sum = $col_ln_padrao->sum();
				$a_wk_ln_padrao[$quote->ref_date] = $sum;
				if ($sum>$max_sum_ln) $max_sum_ln = $sum;
				if ($sum<$min_sum_ln) $min_sum_ln = $sum;
			}
		}

		$a_ln_padrao = [];
		$a_ln_normalizado = [];
		$ultimo_ln_padrao = 0;
		$ultimo_ln_normalizado = 0;

		foreach($a_wk_ln_padrao as $ref_date=>$wk_ln){

			if (($max_sum_ln - $min_sum_ln)==0){
				$normalizado = 0;
			} else {
				$normalizado = ($wk_ln - $min_sum_ln) / ($max_sum_ln - $min_sum_ln);    
			}        

			$a_ln_padrao[$ref_date] = (float)StdFunctions::float_to_percentage_no_signal_en($wk_ln);
			$ultimo_ln_padrao = $a_ln_padrao[$ref_date];

			$a_ln_normalizado[$ref_date] = (float)StdFunctions::float_to_percentage_no_signal_en($normalizado);
			$ultimo_ln_normalizado = $a_ln_normalizado[$ref_date];

		}

		$col_ln_padrao = collect($a_ln_padrao);
		$col_ln_normalizado = collect($a_ln_normalizado);
		$media_ln_padrao = $col_ln_padrao->avg();
		$media_ln_normalizado = $col_ln_normalizado->avg();

		$stddev_padrao = self::standard_devation($a_ln_padrao);
		$stddev_normalizado = self::standard_devation($a_ln_normalizado);

		$desvio->padrao->d0 = StdFunctions::percentage_color($media_ln_padrao);
		$desvio->padrao->d_3 = StdFunctions::percentage_color($media_ln_padrao - (3 * $stddev_padrao));
		$desvio->padrao->d_2 = StdFunctions::percentage_color($media_ln_padrao - (2 * $stddev_padrao));
		$desvio->padrao->d_1 = StdFunctions::percentage_color($media_ln_padrao - (1 * $stddev_padrao));
		$desvio->padrao->d1 = StdFunctions::percentage_color($media_ln_padrao + (1 * $stddev_padrao));
		$desvio->padrao->d2 = StdFunctions::percentage_color($media_ln_padrao + (2 * $stddev_padrao));
		$desvio->padrao->d3 = StdFunctions::percentage_color($media_ln_padrao + (3 * $stddev_padrao));

		$desvio->normalizado->d0 = StdFunctions::percentage_color($media_ln_normalizado);
		$desvio->normalizado->d_3 = StdFunctions::percentage_color($media_ln_normalizado - (3 * $stddev_normalizado));
		$desvio->normalizado->d_2 = StdFunctions::percentage_color($media_ln_normalizado - (2 * $stddev_normalizado));
		$desvio->normalizado->d_1 = StdFunctions::percentage_color($media_ln_normalizado - (1 * $stddev_normalizado));
		$desvio->normalizado->d1 = StdFunctions::percentage_color($media_ln_normalizado + (1 * $stddev_normalizado));
		$desvio->normalizado->d2 = StdFunctions::percentage_color($media_ln_normalizado + (2 * $stddev_normalizado));
		$desvio->normalizado->d3 = StdFunctions::percentage_color($media_ln_normalizado + (3 * $stddev_normalizado));

		return (object)[
			'normalizado' => $a_ln_normalizado,
			'padrao' => $a_ln_padrao,
			'periodo_sigla' => $periodo_sigla,
			'periodo_titulo' => $periodo_titulo,
			'desvio_padrao' => $desvio->padrao,
			'desvio_normalizado' => $desvio->normalizado,
			'atual_padrao' => StdFunctions::percentage($ultimo_ln_padrao),
			'atual_normalizado' => StdFunctions::percentage($ultimo_ln_normalizado)
		];

	}

	public static function todos_os_pares_por_moeda($moeda, $moedas_incluidas = []){

		$pares = Currency::where('name','like','%'.$moeda."%")		
		->select('name')
		->distinct()
		->orderBy('name')
		->get();

		$todos_os_pares = [];

		foreach($pares as $par){

			if ($moedas_incluidas == []) {
				$todos_os_pares[] = $par->name;	
			} else {
				$moeda_primaria = substr($par->name, 0, 3);
				$moeda_secundaria = substr($par->name, 3, 3);
				if (in_array($moeda_primaria, $moedas_incluidas) && in_array($moeda_secundaria, $moedas_incluidas)){
					$todos_os_pares[] = $par->name;		
				}
			}
			
		}

		return $todos_os_pares;

	}

	public static function principais_moedas_em_ordem(){

		return ['EUR','GBP','AUD','NZD','USD','CAD','CHF','JPY'];

	}

	public static function principais_cruzamentos_moeda($moeda){

		$principais_pares = self::principais_moedas_em_ordem();

		$ind_moeda_base = array_search($moeda, $principais_pares);

		$pares = [];

		for ($ind = 0; $ind < count($principais_pares); $ind++){
			if ($ind < $ind_moeda_base){
				$pares[] = $principais_pares[$ind].$moeda;
			} else {
				if ($ind > $ind_moeda_base){
					$pares[] = $moeda.$principais_pares[$ind];
				}
			}
		}

		return $pares;

	}

	public static function convert_percent_to_xref($percentual){

		$percentual = $percentual * 100;

		$tabela = self::forca_percent_pontos_xref();

		foreach($tabela as $xref){
			if ($percentual > $xref['base']) return $xref['indice'];
		}

		return 0;

	}

	public static function forca_percent_pontos_xref(){

		return Cache::rememberForever('forca_percent_pontos', function(){

			$percent_pontos = [];
			$forca_percent_pontos_padrao = [3, 10, 25, 40, 50, 60, 75, 90, 97];

			for ($ind_percent = 9; $ind_percent >= 1; $ind_percent--){
				$var_name = "forca_percent_pontos_" . $ind_percent;
				$percent_pontos[] = [
					'indice' => $ind_percent,
					'base' => Config::ler($var_name, $forca_percent_pontos_padrao[$ind_percent - 1])
				];
			}

			return $percent_pontos;

		});

	}

	public static function forca_geral_moeda_indicador($forca){

		$forca_pontos_indicador = self::forca_pontos_indicador_xref();

		if ($forca >= $forca_pontos_indicador->green) return 'high';
		if ($forca >= $forca_pontos_indicador->yellow) return 'yellow';
		if ($forca >= $forca_pontos_indicador->orange) return 'orange';

		return 'low';
		
	}

	public static function forca_pontos_indicador_xref(){

		return Cache::rememberForever('forca_pontos_indicador', function(){

			return (object)[
				'orange' => Config::ler('forca_pontos_indicador_orange', 2),
				'yellow' => Config::ler('forca_pontos_indicador_yellow', 5),
				'green' => Config::ler('forca_pontos_indicador_green', 7)
			];

		});

	}

	public static function forca_geral_buy_sell_indicador($buy, $sell){

		$buy_class = "neutral";
		$sell_class = "neutral";

		$forca_buy_sell_xref = self::forca_buy_sell_xref();

		if ($buy >= $forca_buy_sell_xref->high_buy && $sell <= $forca_buy_sell_xref->high_sell) $buy_class='high';
		if ($buy <= $forca_buy_sell_xref->low_buy && $sell >= $forca_buy_sell_xref->low_sell) $sell_class='low';		

		return (object)[
			'buy_class' => $buy_class,
			'sell_class' => $sell_class
		];

	}

	public static function forca_buy_sell_xref(){

		return Cache::rememberForever('forca_buy_sell', function(){

			return (object)[
				'high_buy' => Config::ler('forca_buy_sell_high_buy',6),
				'high_sell' => Config::ler('forca_buy_sell_high_sell',2),
				'low_buy' => Config::ler('forca_buy_sell_low_buy',2),
				'low_sell' => Config::ler('forca_buy_sell_low_sell',6)
			];

		});

	}



}
