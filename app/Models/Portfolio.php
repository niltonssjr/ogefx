<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $fillable = [
    	'user_id',
    	'start_ref_date',
    	'end_ref_date',
    	'pair',
    	'period',
    	'created_at'
    ];
}
