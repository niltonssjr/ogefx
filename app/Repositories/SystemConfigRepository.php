<?php 

namespace App\Repositories;
use App\Models\SystemConfig;

class SystemConfigRepository{

	public static function ler($key, $padrao = null){


		$config = SystemConfig::where('key',$key)->first();

		if (!$config) return $padrao;

		return $config->value;

	}

	public static function gravar($key,$value){


		$config = SystemConfig::firstOrCreate(['key'=>$key]);

		$config->value = $value;

		$config->save();

		return $config->value;

	}

}

?>