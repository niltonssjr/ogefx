<?php 

namespace App\Repositories;
use App\Models\Currency;

class CurrenciesRepository{

	public static function valid_currencies(){

		return Currency::pluck('name')->toArray();

	}

}

?>