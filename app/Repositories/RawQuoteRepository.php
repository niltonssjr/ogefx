<?php 

namespace App\Repositories;
use App\StdClasses\StdFunctions;

class RawQuoteRepository{

	public $ref_date;
	public $open;
	public $minimum;
	public $maximum;
	public $close;
	public $volume;
	public $pair;
	public $period;
	public $num_period;
	public $cruzamento_media;

	public function __construct($pair, $num_period, $file = null){

		if (!$file) return;

		$lines = StdFunctions::csv_to_array_lines($file);
		$line = $lines[0];

		$this->pair = $pair;
		$this->num_period = $num_period;

		$this->load_from_raw_line($line);

		return;

	}

	private function load_from_raw_line($line){

		$quote = explode(",",$line);

		$this->load_attributes_from_array($quote);

		return;

	}

	private function load_attributes_from_array($quote){

		$this->ref_date = str_replace(".","-",$quote[0]);
		$this->open = $quote[1];
		$this->minimum = $quote[3];
		$this->maximum = $quote[2];
		$this->close = $quote[4];
		$this->volume = $quote[5];
		$this->cruzamento_media = isset($quote[12]) ? $quote[12] : 0;

		return;

	}

}

?>