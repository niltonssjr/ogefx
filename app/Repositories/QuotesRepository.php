<?php 

namespace App\Repositories;
use Log;
use App\StdClasses\StdFunctions;
use App\Models\Quote;

class QuotesRepository{

	public static function fileinfo($filename){

		$valid_periods = ['43200','10080','1440','240','60','30','15','5','1'];

		$retorno = (object)[];
		$retorno->pair = str_replace($valid_periods,"",$filename);
		$retorno->num_period = str_replace($retorno->pair,"",$filename);

		return $retorno;

	}

	public static function serie_de_retorno_padrao($quotes, $qtd_periodos = 12){

		$desvio = (object)[
			'padrao' => (object) [
				'd_3' => 0,
				'd_2' => 0,
				'd_1' => 0,
				'd0' => 0,
				'd1' => 0,
				'd2' => 0,
				'd3' => 0,
			],
			'normalizado' => (object) [
				'd_3' => 0,
				'd_2' => 0,
				'd_1' => 0,
				'd0' => 0,
				'd1' => 0,
				'd2' => 0,
				'd3' => 0,
			]
		];



		$quotes = $quotes->sortBy('ref_date')->all();
		$period = $quotes[0]->period;
		$quotes_sorted = [];
		foreach($quotes as $quote) $quotes_sorted[] = $quote;

		$periodos = $qtd_periodos;
		$prefixo_sigla = "CR".$periodos;
		$a_texto_periodo_red = ['d'=>"D",'w'=>'S','m'=>'M'];
		$a_texto_periodo_ext = ['d'=>'DIAS','w'=>'SEMANAS','m'=>'MESES' ];
		$periodo_sigla = $prefixo_sigla . $a_texto_periodo_red[$period];
		$periodo_titulo = $periodos . " " . $a_texto_periodo_ext[$period];

		$retornos = [];
		$base_index = 0;
		$maior_retorno = -9999;
		$menor_retorno = 9999;

		foreach($quotes_sorted as $key => $quote){

			if ($key > ($periodos - 1)) $base_index = $key - ($qtd_periodos - 1);

			$base_quote = $quotes_sorted[$base_index];
			$retorno = ($quote->close / $base_quote->open) - 1;
			$retorno = (float)StdFunctions::float_to_percentage_no_signal_en($retorno);

			if ($retorno > $maior_retorno) $maior_retorno = $retorno;
			if ($retorno < $menor_retorno) $menor_retorno = $retorno;

			$retornos[$quote->ref_date] = $retorno;

		}

		$retornos_normalizados = [];
		$ultimo_retorno_padrao = 0;
		$ultimo_retorno_normalizado = 0;

		foreach($retornos as $ref_date=>$retorno){
			$retorno_normalizado = ($retorno - $menor_retorno) / ($maior_retorno - $menor_retorno);
			$retornos_normalizados[$ref_date] = (float)StdFunctions::float_to_percentage_no_signal_en($retorno_normalizado);

			$ultimo_retorno_padrao = $retorno;
			$ultimo_retorno_normalizado = $retorno_normalizado;
		}

		$col_retornos_padrao = collect($retornos);
		$col_retornos_normalizado = collect($retornos_normalizados);
		$media_retornos_padrao = $col_retornos_padrao->avg();
		$media_retornos_normalizado = $col_retornos_normalizado->avg();

		$stddev_padrao = Quote::standard_devation($retornos);
		$stddev_normalizado = Quote::standard_devation($retornos_normalizados);

		$desvio->padrao->d0 = StdFunctions::percentage_color($media_retornos_padrao);
		$desvio->padrao->d_3 = StdFunctions::percentage_color($media_retornos_padrao - (3 * $stddev_padrao));
		$desvio->padrao->d_2 = StdFunctions::percentage_color($media_retornos_padrao - (2 * $stddev_padrao));
		$desvio->padrao->d_1 = StdFunctions::percentage_color($media_retornos_padrao - (1 * $stddev_padrao));
		$desvio->padrao->d1 = StdFunctions::percentage_color($media_retornos_padrao + (1 * $stddev_padrao));
		$desvio->padrao->d2 = StdFunctions::percentage_color($media_retornos_padrao + (2 * $stddev_padrao));
		$desvio->padrao->d3 = StdFunctions::percentage_color($media_retornos_padrao + (3 * $stddev_padrao));

		$desvio->normalizado->d0 = StdFunctions::percentage_color($media_retornos_normalizado);
		$desvio->normalizado->d_3 = StdFunctions::percentage_color($media_retornos_normalizado - (3 * $stddev_normalizado));
		$desvio->normalizado->d_2 = StdFunctions::percentage_color($media_retornos_normalizado - (2 * $stddev_normalizado));
		$desvio->normalizado->d_1 = StdFunctions::percentage_color($media_retornos_normalizado - (1 * $stddev_normalizado));
		$desvio->normalizado->d1 = StdFunctions::percentage_color($media_retornos_normalizado + (1 * $stddev_normalizado));
		$desvio->normalizado->d2 = StdFunctions::percentage_color($media_retornos_normalizado + (2 * $stddev_normalizado));
		$desvio->normalizado->d3 = StdFunctions::percentage_color($media_retornos_normalizado + (3 * $stddev_normalizado));

		return (object)[
			'normalizado' => $retornos_normalizados,
			'padrao' => $retornos,
			'periodo_sigla' => $periodo_sigla,
			'periodo_titulo' => $periodo_titulo,
			'desvio_padrao' => $desvio->padrao,
			'desvio_normalizado' => $desvio->normalizado,
			'atual_padrao' => StdFunctions::percentage($ultimo_retorno_padrao),
			'atual_normalizado' => StdFunctions::percentage($ultimo_retorno_normalizado)
		];		

		return $retornos;

	}
	
}

?>