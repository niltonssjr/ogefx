<?php 

namespace App\Repositories;

use App\Models\PairCurrentInfo;
use App\Repositories\CurrenciesRepository as Currency;
use App\Repositories\RawQuoteRepository as RawQuote;
use App\Repositories\QuotesRepository as Quotes;
use App\StdClasses\StdFunctions;
use Storage;
use Cache;
use Log;

class PairCurrentInfoRepository{

	protected $valid_periods = ['15','30','60','240','1440'];
	protected $valid_pairs;

	public function __construct(){

		$this->valid_pairs = Currency::valid_currencies();
	}

	public function retrieve($pair){

		return PairCurrentInfo::firstOrCreate(['pair'=>$pair]);
		

	}

	public function retrieve_from_cache($pair){

		return  Cache::rememberForever('PairCurrentInfo_'.$pair, function() use ($pair){
			return $this->retrieve($pair);
		});

	}

	private function validate_file($fileinfo){

		return in_array($fileinfo->pair, $this->valid_pairs) && 
		in_array($fileinfo->num_period, $this->valid_periods);

	}

	public function import_current_info_remote_server($paths){

		$last_pair = "";

		foreach($paths as $path){

			$file = StdFunctions::single_file_name($path);

			$fileinfo = Quotes::fileinfo($file);

			if (!$this->validate_file($fileinfo)) continue;	

			$file = Storage::get($path);

			$raw_quote = new RawQuote($fileinfo->pair,$fileinfo->num_period,$file);			

			if ($raw_quote->pair != $last_pair){
				//na primeira vez não teremos pares. A partir da primeira quebra, salvaremos as informações do par anteriore
				if (isset($pair_being_updated)){
					$pair_being_updated->save();
					Cache::forget('PairCurrentInfo_'.$pair_being_updated->pair);
				}
				//na quebra do par, busca o novo par a ser atualizado com informações correntes
				$pair_being_updated = $this->retrieve($raw_quote->pair);
				$last_pair = $raw_quote->pair;
			}

			$pair_being_updated->{"ind_" . $raw_quote->num_period . "_cruzamento_media"} = $raw_quote->cruzamento_media;

		}

		if (isset($pair_being_updated)){
			$pair_being_updated->save();
			Cache::forget('PairCurrentInfo_'.$pair_being_updated->pair);
		}

		return;		

	}	

}

?>